package com.quan.xap;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.quan.xap.activity.chat.ChatActivity;
import com.quan.xap.api.ApiClient;
import com.quan.xap.constants.Constants;
import com.quan.xap.interfaces.SetListViewListener;
import com.quan.xap.models.ClassChatList;
import com.quan.xap.models.InfoChat;
import com.quan.xap.models.InfoItem;
import com.quan.xap.models.ResultGetChatList;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.DataChatListValues;
import com.quan.xap.utility.MyDatabaseHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MyChatService extends Service {

    int intintervalTime = 5000;
    String strLastDate = "0";

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
//            Toast.makeText(getApplicationContext(), "getChat", Toast.LENGTH_LONG).show();
            getChat(strLastDate);
            handler.postDelayed(runnable, intintervalTime);
        }
    };

    public MyChatService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        setDataChatListValuesList();
        handler.removeCallbacks(runnable);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        getDataChatListValuesList();
        getChat(strLastDate);
        handler.postDelayed(runnable, 2000);


        return super.onStartCommand(intent, flags, startId);
    }


    private void getChat(final String last_date){

        Map<String, Object> params = new HashMap<String, Object>();
        if(Common.userProfile!=null && Common.userProfile.getId()!=null && !Common.userProfile.getId().isEmpty()) {
            params.put("user_id", Common.userProfile.getId()); //to_user_id
            params.put("last_date", "");

            ApiClient.getApiClient().get_chat(params, new Callback<ResultGetChatList>() {
                @Override
                public void success(ResultGetChatList resultGetItems, Response response) {
                    if(resultGetItems.getStat().equals(Common.success)) {
                        String currentdate = "";

                        if (resultGetItems.getDataList().size() != 0) {
                            currentdate = resultGetItems.getDataList().get(resultGetItems.getDataList().size() - 1).getTimestamps();
                        }
                        if (currentdate.compareTo(last_date) > 0) {
                            if(!last_date.equals("0")){
                                InfoChat infoChat = resultGetItems.getDataList().get(resultGetItems.getDataList().size() - 1);
                                if(Common.userProfile != null && !infoChat.getFrom_user_id().equals(Common.userProfile.getId())) {
                                    showNotification(infoChat.getFrom_user_name());
                                }
                            }
                            strLastDate = currentdate;
                            Common.chatListSort.clear();
                            List<InfoChat> chatListFrom = new ArrayList<InfoChat>();
                            List<InfoChat> chatListTo = new ArrayList<InfoChat>();
                            for (InfoChat item : resultGetItems.getDataList()) {
                                if (Common.userProfile != null) {
                                    if (item.getFrom_user_id().equals(Common.userProfile.getId())) {
//                                || item.getTo_user_id().equals(Common.userProfile.getId())) {
                                        chatListFrom.add(item);
                                    } else if (item.getTo_user_id().equals(Common.userProfile.getId())) {
                                        chatListTo.add(item);
                                    }
                                }
                            }

                            setChatListFrom(chatListFrom);
                            setChatListTo(chatListTo);

                            getItemInfo_SortById();

//                            if(iListView) {
//                                refreshDataChatListValuesList();
//                                iListView = false;
//                            }

                            applyUnreadNumberToListView();


                        } else {
                            getItemInfo_SortById();
//                            if(!iListView) {
//                                setListView();
//                                iListView = true;
//                            }
                        }
                    }else{
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.chatting_message_failed), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
                }
            });
        }
    }


    private void setChatListFrom(List<InfoChat> mData){
        if(mData==null || mData.size()==0){
            return;
        }

        Collections.sort(mData, new Comparator<InfoChat>() {
            @Override
            public int compare(InfoChat t1, InfoChat t2) {
                return Float.compare(Float.valueOf(t1.getItem_id()), Float.valueOf(t2.getItem_id()));
            }
        });

        String temp_item_id = mData.get(0).getItem_id();
        String temp_to_user_id = mData.get(0).getTo_user_id();

        ClassChatList temp_class = new ClassChatList();

        for(int i=0; i<mData.size(); i++){
            if(temp_item_id.equals(mData.get(i).getItem_id())){
                if(temp_to_user_id.equals(mData.get(i).getTo_user_id())){ // && Common.userProfile.getId().equals(mData.get(i).getFrom_user_id()))
//                    || (temp_to_user_id.equals(mData.get(i).getFrom_user_id()) && Common.userProfile.getId().equals(mData.get(i).getTo_user_id()))){
                    temp_class.my_user_id = Common.userProfile.getId();
                    temp_class.item_id = mData.get(i).getItem_id();
                    temp_class.other_user_id = mData.get(i).getTo_user_id();
                    temp_class.chatList.add(mData.get(i));
                }else{
//                    temp_class.item_id = mData.get(i).getItem_id();
                    if(temp_class.getChatList().size()!=0) {
                        Common.chatListSort.add(temp_class);
                    }
                    temp_class = new ClassChatList();
                    temp_to_user_id = mData.get(i).getTo_user_id();
                    i--;
                }
            }else{
                if(temp_class.getChatList().size()!=0) {
                    Common.chatListSort.add(temp_class);
                }
                temp_class = new ClassChatList();
                temp_item_id = mData.get(i).getItem_id();
                temp_to_user_id = mData.get(i).getTo_user_id();
                i--;
            }
        }
        if(temp_class.getChatList().size()!=0) {
            Common.chatListSort.add(temp_class);
        }
    }

    private void setChatListTo(List<InfoChat> mData){
        if(mData==null || mData.size()==0){
            return;
        }
        Collections.sort(mData, new Comparator<InfoChat>() {
            @Override
            public int compare(InfoChat t1, InfoChat t2) {
                return Float.compare(Float.valueOf(t1.getItem_id()), Float.valueOf(t2.getItem_id()));
            }
        });

        String temp_item_id = mData.get(0).getItem_id();
        String temp_from_user_id = mData.get(0).getFrom_user_id();

        ClassChatList temp_class = new ClassChatList();

        boolean isEquals = false;
        boolean isFirst = true;

        for(int i=0; i<mData.size(); i++){
            for(int k=0; k<Common.chatListSort.size(); k++){
                ClassChatList item = Common.chatListSort.get(k);
                if(mData.get(i).getItem_id().equals(item.item_id)
                        && mData.get(i).getFrom_user_id().equals(item.other_user_id)
                        && mData.get(i).getTo_user_id().equals(Common.userProfile.getId()) ){

                    Common.chatListSort.get(k).chatList.add(mData.get(i));
                    isEquals = true;
                }
            }

            if(!isEquals){
                if(isFirst){
                    temp_item_id = mData.get(i).getItem_id();
                    temp_from_user_id = mData.get(i).getFrom_user_id();
                }
                if(temp_item_id.equals(mData.get(i).getItem_id())){
                    if(temp_from_user_id.equals(mData.get(i).getFrom_user_id())){
                        temp_class.my_user_id = Common.userProfile.getId();
                        temp_class.item_id = mData.get(i).getItem_id();
                        temp_class.other_user_id = mData.get(i).getFrom_user_id();
                        temp_class.chatList.add(mData.get(i));
                    }else{
                        if(!temp_class.getItem_id().isEmpty()) {
                            Common.chatListSort.add(temp_class);
                            temp_class = new ClassChatList();
                            temp_from_user_id = mData.get(i).getTo_user_id();
                            i--;
                        }
                    }
                }else{
                    if(!temp_class.getItem_id().isEmpty()) {
                        Common.chatListSort.add(temp_class);
                        temp_class = new ClassChatList();
                        temp_item_id = mData.get(i).getItem_id();
                        i--;
                    }
                }
                isFirst = false;
            }
            isEquals = false;
        }
        if(!isFirst && !temp_class.getItem_id().isEmpty()) Common.chatListSort.add(temp_class);

        getItemInfo_SortById();

    }

    private void getItemInfo_SortById(){
        for(int k=0; k<Common.chatListSort.size(); k++) {
            final int kk =k;
//            getItemInfo(kk);
            getItemInfo_00(kk);
            Collections.sort(Common.chatListSort.get(k).chatList, new Comparator<InfoChat>() {
                @Override
                public int compare(InfoChat t1, InfoChat t2) {
                    return Float.compare(Float.valueOf(t1.getId()), Float.valueOf(t2.getId()));
//                    return Long.compare(Long.valueOf(t1.getTimestamps()), Long.valueOf(t2.getTimestamps()));
                }
            });
        }
    }

    private void applyUnreadNumberToListView(){
        int itotal_unread = 0;
        for(int k=0;k<Common.chatListSort.size();k++){
            int iUnreadK = getUnreadNumber(k);
            Common.chatListSort.get(k).setUnread_number(iUnreadK);
            itotal_unread = itotal_unread + iUnreadK;
        }
        if(itotal_unread!=0) {
//            Common.listTitleTextArrowView.setVisibility(View.VISIBLE);
            Common.listTitleTextArrowView.setText(String.valueOf(itotal_unread));
        }else{
            Common.listTitleTextArrowView.setVisibility(View.INVISIBLE);
        }
        Log.d("Conversation:", "get unread number success");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sendBroadcast(new Intent("setListView"));
            }
        }, 100);
    }

    private int getUnreadNumber(int index){
        if(Common.dataChatListValuesList.size()==0 || Common.dataChatListValuesList.size()!= Common.chatListSort.size()){
            refreshDataChatListValuesList();
        }

        int iUnread = 0;
        ClassChatList classChatList = Common.chatListSort.get(index);

        boolean isStartCount = false;
        for(DataChatListValues dataChatListValue : Common.dataChatListValuesList) {
//            isStartCount = false;
            if (dataChatListValue.item_id.equals(classChatList.getItem_id())
                    && dataChatListValue.my_user_id.equals(classChatList.getMy_user_id())
                    && dataChatListValue.other_user_id.equals(classChatList.getTo_user_id())) {
                for (int i = 0; i < classChatList.getChatList().size(); i++) {
                    if(isStartCount){
                        if(classChatList.getChatList().get(i).getFrom_user_id().equals(dataChatListValue.other_user_id)){
                            iUnread++;
                        }
                    }
                    if(dataChatListValue.last_chatting_number.equals(classChatList.getChatList().get(i).getId())){
                        isStartCount = true;
                    }
                }
                return iUnread;
            }
        }

        return iUnread;
    }


    private void setDataChatListValuesList(){
//        refreshDataChatListValuesList();
        MyDatabaseHelper mDatabaseHelper = new MyDatabaseHelper(this);
        mDatabaseHelper.setValues();
    }

    private void refreshDataChatListValuesList(){
        Common.dataChatListValuesList.clear();
        for(int k=0; k<Common.chatListSort.size(); k++){
            ClassChatList chattemp = Common.chatListSort.get(k);
            String getid = "0";
            if(chattemp.getChatList().size()>0) {
                getid = chattemp.getChatList().get(chattemp.getChatList().size() - 1).getId();
            }else {

            }
            DataChatListValues datatemp = new DataChatListValues(chattemp.getItem_id(), chattemp.getMy_user_id()
                    , chattemp.getTo_user_id(), getid);
            Common.dataChatListValuesList.add(datatemp);

        }
    }

    private void getDataChatListValuesList(){
        MyDatabaseHelper mDatabaseHelper = new MyDatabaseHelper(this);
        mDatabaseHelper.getValues();
    }

    private void getItemInfo_00(int listIndex){
        if (Common.chatListSort.size() > listIndex) {
            String list_item_id = Common.chatListSort.get(listIndex).getItem_id();
            if(Common.totalItems!=null && !Common.totalItems.isEmpty()) {
                for (InfoItem item : Common.totalItems) {
                    if (item.getId().equals(list_item_id)) {
                        Common.chatListSort.get(listIndex).setItem(item);
                    }
                }
            }
            if(Common.userItems!=null && !Common.userItems.isEmpty()) {
                for (InfoItem item : Common.userItems){
                    if (item.getId().equals(list_item_id)) {
                        Common.chatListSort.get(listIndex).setItem(item);
                    }
                }
            }
        }

    }

    private void showNotification(String other_user){
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification myNotication;
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder.setAutoCancel(true);
        builder.setContentTitle(getResources().getString(R.string.app_name));
        String strMessage = "Message come from " + other_user;
        builder.setTicker(strMessage);
        builder.setContentText(strMessage);
        builder.setSmallIcon(R.drawable.check);
        builder.setOngoing(false);
//        builder.setSubText("This is subtext...");   //API level 16

        builder.build();
        myNotication = builder.getNotification();
        manager.notify(11, myNotication);
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, myNotication);
    }

}
