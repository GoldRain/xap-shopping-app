package com.quan.xap.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by guoquan on 4/3/2017.
 */

public class InfoNotification implements Serializable{
    private static String Notification = "Notification";

    @SerializedName("id")
    private String id = "";
    @SerializedName("user_id")
    private String user_id = "";
    @SerializedName("notif_chat_messages")
    private String notif_chat_messages = "0";
    @SerializedName("notif_price_change")
    private String notif_price_change = "0";
    @SerializedName("notif_expired_listing")
    private String notif_expired_listing = "0";
    @SerializedName("notif_promotions")
    private String notif_promotions = "0";
    @SerializedName("notif_tips")
    private String notif_tips = "0";
    @SerializedName("fav_categories")
    private String fav_categories = "";

    public InfoNotification() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNotif_chat_messages() {
        return notif_chat_messages;
    }

    public void setNotif_chat_messages(String notif_chat_messages) {
        this.notif_chat_messages = notif_chat_messages;
    }

    public String getNotif_price_change() {
        return notif_price_change;
    }

    public void setNotif_price_change(String notif_price_change) {
        this.notif_price_change = notif_price_change;
    }

    public String getNotif_expired_listing() {
        return notif_expired_listing;
    }

    public void setNotif_expired_listing(String notif_expired_listing) {
        this.notif_expired_listing = notif_expired_listing;
    }

    public String getNotif_promotions() {
        return notif_promotions;
    }

    public void setNotif_promotions(String notif_promotions) {
        this.notif_promotions = notif_promotions;
    }

    public String getNotif_tips() {
        return notif_tips;
    }

    public void setNotif_tips(String notif_tips) {
        this.notif_tips = notif_tips;
    }

    public String getFav_categories() {
        return fav_categories;
    }

    public void setFav_categories(String fav_categories) {
        this.fav_categories = fav_categories;
    }
}
