package com.quan.xap.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.quan.xap.R;
import com.quan.xap.models.ClassChatList;
import com.quan.xap.models.InfoItem;
import com.quan.xap.models.InfoNotification;
import com.quan.xap.models.InfoUserProfile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Quan on 7/14/2017.
 */

public class Common {

    public static String common_url = "http://82.223.19.247/xap";
//    public static String common_url = "http://192.168.0.198:80/xap";

    public static String success = "success";
    public static String Mark = "Mark";
    public static String SIGN = "Sign in";
    public static String REGISTER = "Register";

    public static String ITEM_ID = "item_id";

    public static String[] Sort_By_String = {"distance","price low to high","price high to low","most recently published"};
    public static int[] Sort_By_Drawable = {R.drawable.ic_room_white_36dp, R.drawable.ic_vertical_align_top_white_36dp
                        ,R.drawable.ic_vertical_align_bottom_white_36dp, R.drawable.ic_date_range_white_36dp};
    public static int Sort_By_int = 0; //0: none, 1:distance, 2:price low to high, 3:price high to low, 4:most recently published

    public static String[] Category_String = {"Motors & Accessories" ,"Electronics","Sports & Leisure","Home & Garden"
                    ,"Games & Consoles","Books, Movies & Music", "Fashion & Accessories", "Baby & Child"
                    ,"Real Estate", "Appliances", "Services", "Other" };
    public static int[] Category_Drawable = {R.drawable.ic_widgets_white_36dp, R.drawable.ic_room_white_36dp, R.drawable.ic_vertical_align_top_white_36dp
            ,R.drawable.ic_vertical_align_bottom_white_36dp, R.drawable.ic_date_range_white_36dp};
    public static int Category_int = 0;

    public static String[] Currency = {"CNY", "ARS", "MXN", "COP", "EUR", "USD", "GBP", "BRL"};
    public static float[] Rate = {0.15f, 0.057f, 0.056f, 0.00034f, 1.18f, 1.0f, 1.34f, 0.32f};
    public static String[] Verify_Text = {"Nothing verified", "Partially verified", "All verified"};

    public static boolean[] isVerified = {false,false,false}; //email verify, facebook verify, google verify

    public static String[] Categories_AddItem = new String[]{"IPhone5", "Meta", "Seat Ibiza", "Nevera", "Ps3", "Samsung galaxy"
            ,"sofa", "Bicicleta", "Casco Moto", "Vestido fiesta", "Coche"};

    public static String[] Reasons = new String[]{"People or animals", "Joke", "Explicit content", "Photo doesn't match"
            , "Food or drinks", "Drugs or medicines", "Doubled item", "Forbidden product or service"
            , "Resale (tickets, etc)", "Spam" };

    public static String[] Menu_string = new String[]{"Chat", "Bulletin", "Categories", "Invite my friends", "Help"} ;

    public static long Duration = 500;

    public static String currency_usd = "$";

    public static Uri outputFileUri_User = Uri.parse("");
    public static Uri[] outputFileUri_List = {Uri.parse(""),Uri.parse(""),Uri.parse(""),Uri.parse("")};
    public static File newfile;
    public static Uri photoTempUri;

    public static InfoUserProfile userProfile = new InfoUserProfile();
    public static List<InfoItem> userItems = new ArrayList<>();
    public static ArrayList<InfoItem> totalItems = new ArrayList<>();
    public static InfoNotification userNotification = new InfoNotification();

    public static String[] favorite_categories = {"0" ,"0","0","0"
            ,"0","0", "0", "0"
            ,"0", "0", "0", "0" };

    public static ViewPager pager;
    public static int intMaxSize_tab = 0;

    public static float longitude = 0.0f;
    public static float latitude = 0.0f;

    public static float filter_distance = -1; //mile
    public static float filter_minprice = 0;
    public static float filter_maxprice = -1;
    public static int filter_date = -1;
    public static int filter_deliver = -1;
    public static int filter_accept = -1;
    public static int filter_category = -1;
    public static String filter_name = "";

    public static TextView listTitleTextArrowView;

    public static GoogleApiClient mGoogleApiClient;

    public static boolean isFilter = false;
    public static boolean isLoadedItemData = false;

    public static List<DataChatListValues> dataChatListValuesList = new ArrayList<>();

    public static List<ClassChatList> chatListSort = new ArrayList<>();

    public static float getPriceToUSD(InfoItem item){
        return Float.valueOf(item.getPrice()) * Common.Rate[Integer.valueOf(item.getCurrency())];
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static CircleImageView imgProfilePhoto;
    public static TextView txtHeader01;
    public static TextView txtHeader02;
    public static void setHeaderText(SessionManager session, Context context){
        if(txtHeader01 != null && txtHeader02 != null) {
            if (session.isLoggedIn()) {
                txtHeader01.setText(session.getUsername());
//                if(Common.userProfile.getUsername()!= null){
//                    txtHeader01.setText(Common.userProfile.getUsername());
//                }
                txtHeader02.setText(session.getEmail());
            } else {
                txtHeader01.setText(context.getResources().getString(R.string.create_account_or_sign_in));
                txtHeader02.setText(context.getResources().getString(R.string.at_xap));
            }
        }
    }

    public static File savebitmap(String filename, Bitmap bitmap) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;

        File file = new File(extStorageDirectory,filename + ".png");
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename + ".png");
            Log.e("file exist", "" + file + ",Bitmap= " + filename);
        }
        try {
            // make a new bitmap from your file
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("file", "" + file);
        return file;
    }



}
