package com.quan.xap.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SessionManager
{
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "AndroidHiveLogin";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_USER_ID = "userID";
    private static final String KEY_UPDATE = "update";
    private static final String KEY_LOCK = "lock";

    public SessionManager(Context context)
    {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn)
    {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn()
    {
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public void setUsername(String username){
        editor.putString(KEY_USERNAME, username);
        editor.commit();
    }

    public String getUsername(){
        return pref.getString(KEY_USERNAME, "no name");
    }

    public void setUserID(String userID){
        editor.putString(KEY_USER_ID, userID);
        editor.commit();
    }

    public String getUserID(){
        return pref.getString(KEY_USER_ID, "-1");
    }

    public void setLock(boolean lock){
        editor.putBoolean(KEY_LOCK, lock);
        editor.commit();
    }

    public boolean getLock(){
        return pref.getBoolean(KEY_LOCK, false);
    }

    public void setEmail(String email){
        editor.putString(KEY_EMAIL, email);
        editor.commit();
    }

    public String getEmail(){
        return pref.getString(KEY_EMAIL, "no email");
    }


    public void setUpdate(boolean isUdate)
    {
        editor.putBoolean(KEY_UPDATE, isUdate);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isUpdate()
    {
        return pref.getBoolean(KEY_UPDATE, false);
    }

}