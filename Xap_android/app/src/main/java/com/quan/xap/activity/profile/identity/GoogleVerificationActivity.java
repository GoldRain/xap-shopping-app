package com.quan.xap.activity.profile.identity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.quan.xap.R;
import com.quan.xap.api.ApiClient;
import com.quan.xap.models.ResultGetChat;
import com.quan.xap.utility.Common;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GoogleVerificationActivity extends AppCompatActivity implements View.OnClickListener
        , GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;

    ProgressDialog progressReportData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_verification);

        initialUI();
    }

    private void initialUI() {
        findViewById(R.id.imageBack).setOnClickListener(this);
        findViewById(R.id.txtButton).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtTitle)).setText("Identity Verification");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageBack:
                finish();
                break;
            case R.id.txtButton:
                if(Common.userProfile!=null && Common.userProfile.getGoogle()!=null && !Common.userProfile.getGoogle().isEmpty()){
                    verify_google();
                }else{
                    setGoogle();
                }
                break;
        }
    }

    private void verify_google(){
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getApplicationContext());
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        Map<String, Object> params = new HashMap<String, Object>();
        if(Common.userProfile!=null)  {
            params.put("user_id", Common.userProfile.getId());
            params.put("google", Common.userProfile.getGoogle());

            ApiClient.getApiClient().verify_google(params, new Callback<ResultGetChat>(){
                @Override
                public void success(ResultGetChat resultGetItems, Response response) {
                    if(resultGetItems.getStat().equals(Common.success)) {
                        progressReportData.dismiss();
                        alertDialog.setMessage(getResources().getString(R.string.google_check_message));
                        alertDialog.show();
                        Common.isVerified[2] = true;
                        finish();
                    }else{
                        Common.isVerified[2] = false;
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.verify_failed_message), Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    progressReportData.dismiss();
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
                }
            });
        }else{
            progressReportData.dismiss();
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.verify_failed_message), Toast.LENGTH_LONG).show();
        }
    }

///////////////////////google login
    boolean isGoogleLoading = true;

    private void setGoogle(){

        if(isGoogleLoading) {
            isGoogleLoading = false;

            progressReportData = new ProgressDialog(this);
            progressReportData.show();

            // Configure sign-in to request the user's ID, email address, and basic profile. ID and
            // basic profile are included in DEFAULT_SIGN_IN.
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();

            signIn();
        }
    }

    private void signIn(){
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

            verify_google();
            isGoogleLoading = true;
        }else{
            progressReportData.dismiss();
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            if(Common.userProfile!=null)  Common.userProfile.setGoogle(acct.getEmail());
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

}
