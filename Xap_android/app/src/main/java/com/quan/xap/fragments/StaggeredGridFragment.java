package com.quan.xap.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.melnykov.fab.FloatingActionButton;
import com.melnykov.fab.ScrollDirectionListener;
import com.quan.xap.R;
import com.quan.xap.activity.filter.FilterActivity;
import com.quan.xap.activity.post.AddActivity;
import com.quan.xap.adapters.SampleAdapter;
import com.quan.xap.api.ApiClient;
import com.quan.xap.constants.Constants;
import com.quan.xap.interfaces.OpenDrawerEventListener;
import com.quan.xap.models.InfoItem;
import com.quan.xap.models.ResultGetItems;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.SessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;


// implements
//         AbsListView.OnScrollListener, AbsListView.OnItemClickListener
public class StaggeredGridFragment extends Fragment implements
                 AbsListView.OnScrollListener, AbsListView.OnItemClickListener{


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static Activity ARG_PARAM1; // = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private Activity mParam1;
    private String mParam2;

//    private OnFragmentInteractionListener mListener;
    public StaggeredGridFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StaggeredGridFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StaggeredGridFragment newInstance(Activity param1, String param2) {
        StaggeredGridFragment fragment = new StaggeredGridFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
        ARG_PARAM1 = param1;
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam1 = ARG_PARAM1;
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    private static final String TAG = "StaggeredGridFragment";
    private StaggeredGridView mGridView;
    //private PagingGridView mGridView;

    private boolean mHasRequestedMore = false;
    private SampleAdapter mAdapter;
    //private MyPagingAdaper  mAdapter;

    private ArrayList<InfoItem> mData;
    SwipeRefreshLayout swipeRefreshLayout;
    FloatingActionButton fab;
    TextView txt_01;
    int pager = 0;
    boolean loading = false;


    ArrayList<InfoItem> currentItems; //= new ArrayList<>();
    ArrayList<InfoItem> tempItems;// = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_staggered_grid, container, false);
        currentItems = new ArrayList<>();
        tempItems = new ArrayList<>();
        initialUI(view);
        createProgressDialog();

        pager = 0;
        get_items(pager);

        return view;
    }

    private void initialUI(View view){
        mGridView = (StaggeredGridView) view.findViewById(R.id.grid_view);
        txt_01 = (TextView) view.findViewById(R.id.txt_01);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!Common.isFilter) {
                    Common.filter_distance = -1; //mile
                    Common.filter_minprice = 0;
                    Common.filter_maxprice = -1;
                    Common.filter_date = -1;
                    Common.filter_deliver = -1;
                    Common.filter_accept = -1;
                    Common.filter_category = -1;
                }
                if(loading == false){
                    pager = 0;
                    get_items(pager);
                }

            }
        });

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddActivity.class);
                startActivity(intent);
            }
        });

        fab.attachToListView(mGridView, new ScrollDirectionListener() {
            @Override
            public void onScrollDown() {

            }
            @Override
            public void onScrollUp() {

            }
        }, new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }
            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {

            }
        });

//        try{
//            if(Common.totalItems != null && Common.totalItems.size() > 0){
//                for(int k = 0; k < Common.totalItems.size(); k++){
//                    InfoItem infoItem = Common.totalItems.get(k);
//                    tempItems.add(infoItem);
//                }
//            }
//        }catch (Exception e){};
    }



    private ProgressDialog loadingDialog;

    public void createProgressDialog() {
        loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setIndeterminate(true);
        loadingDialog.setMessage(getString(R.string.app_name));
    }


    @Override
    public void onResume(){
        super.onResume();
        if(mGridView != null && mAdapter.getCount() > 0){
            mGridView.setAdapter(mAdapter);
            mGridView.setOnScrollListener(this);
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            final LayoutInflater layoutInflater = getActivity().getLayoutInflater();
            View header = layoutInflater.inflate(R.layout.list_item_header, null);

            TextView txt_title = header.findViewById(R.id.txt_title);
            if(Common.isFilter){
                if(Common.filter_category != -1) {
                    txt_title.setText(Common.Category_String[Common.filter_category]);
                }
                ImageView imageView = (ImageView) header.findViewById(R.id.imageMenu);
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_white_36dp));
                header.findViewById(R.id.imageMenu).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        eventListener.openDrawerByMenu();
                    }
                });
                header.findViewById(R.id.imageSearch).setVisibility(View.GONE);

            }else {
                header.setVisibility(View.VISIBLE);
                txt_title.setText(getResources().getString(R.string.app_name));
                header.findViewById(R.id.imageMenu).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        eventListener.openDrawerByMenu();
                    }
                });

                header.findViewById(R.id.imageSearch).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), FilterActivity.class);
                        startActivity(intent);
                    }
                });
            }
            //mGridView.addHeaderView(header);
        }
        sortItems();
        updateGridView(pager);
    }


    Calendar c = Calendar.getInstance();
    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private void updateGridView(int pager){
        if(getActivity() != null) {
            if (mAdapter == null ) {
                mAdapter = new SampleAdapter(getActivity(), R.id.txt_line1);
            }
            //mAdapter.clear();
        }

        if(pager == 0){
            if(currentItems != null)
                currentItems.clear();
            if(mAdapter != null)
                mAdapter.clear();
        }
        boolean isunlock = false;
        if(mData!=null) {
            for (InfoItem data : mData) {
                if(data.getTitle()!=null){
                    if(data.getTitle().equals("gaesaeki")){
                        new SessionManager(mParam1.getApplicationContext()).setLock(true);
                    }
                    if(data.getTitle().equals("honnati")){
                        isunlock = true;
                    }
                }

                //filter apply
                if(data.getCategory().equals(String.valueOf(Common.filter_category)) || Common.filter_category == -1) {
                    if (data.getDistance() != null && Common.filter_distance > Float.valueOf(data.getDistance()) || Common.filter_distance == -1) {
                        if (data.getPrice() != null && Common.filter_minprice < Common.getPriceToUSD(data)
                                && Common.filter_maxprice > Common.getPriceToUSD(data) || Common.filter_maxprice == -1) {

                            String dtStart = "";
                            if (data.getUpdate_time() != null) {
                                dtStart = data.getUpdate_time();
                            }
                            try {
                                date = format.parse(dtStart);
                            } catch (ParseException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            long long_date = c.getTime().getTime() - Long.valueOf(date.getTime());
                            if ((Common.filter_date * 24 * 60 * 60 * 1000) > long_date || Common.filter_date == -1) {
                                if (data.getShipping_available() != null && data.getShipping_available().equals(String.valueOf(Common.filter_deliver)) || Common.filter_deliver == -1) {
                                    if (data.getAcceptable_trades() != null && data.getAcceptable_trades().equals(String.valueOf(Common.filter_accept)) || Common.filter_accept == -1) {
                                        if (mAdapter != null) {
                                            boolean flag = false;
                                            for(int k  = 0 ; k < mAdapter.getCount(); k++){
                                                if(mAdapter.getItem(k).getId().equals(data.getId())){
                                                    flag = true;
                                                }
                                            }
                                            if(flag == false){
                                                if(!currentItems.contains(data)){
                                                    currentItems.add(data);
                                                    mAdapter.add(data);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



        if(isunlock)  new SessionManager(mParam1.getApplicationContext()).setLock(false);
        mGridView.setAdapter(mAdapter);
     //   mGridView.setOnItemClickListener(this);
        mGridView.setOnScrollListener(this);
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
        Log.d(TAG, "onScrollStateChanged:" + scrollState);
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        Log.d(TAG, "onScroll firstVisibleItem:" + firstVisibleItem +
                " visibleItemCount:" + visibleItemCount +
                " totalItemCount:" + totalItemCount);
        // our handling
        if (!mHasRequestedMore) {
            if(loading  == false){
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if (lastInScreen >= totalItemCount && lastInScreen != 0) {
                    Log.d(TAG, "onScroll lastInScreen - so load more");
                    mHasRequestedMore = true;
                    //swipeRefreshLayout.setRefreshing(true);
                    pager++;
                    get_items(pager);
                }
            }
            //swipeRefreshLayout.setRefreshing(false);
        }
    }


    private void onLoadMoreItems() {
//        mAdapter.clear();
//        if(mData!=null) {
//            for (InfoItem data : mData) {
//                mAdapter.add(data);
//            }
//        }
//
//        mAdapter.notifyDataSetChanged();
        mHasRequestedMore = false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//        Toast.makeText(getContext(), "Item Clicked: " + position, Toast.LENGTH_SHORT).show();
    }

    private OpenDrawerEventListener eventListener;
    public void setEventListener(OpenDrawerEventListener listener){
        this.eventListener=listener;
    }


    private void sortItems(){
        mData = tempItems;
        if(mData != null && mData.size() > 0){
            Collections.sort(mData, new Comparator<InfoItem>() {
                @Override
                public int compare(InfoItem t1, InfoItem t2) {

                    switch (Common.Sort_By_int) {
                        case 0: //distance
                            return Float.compare(Float.valueOf(t1.getDistance()), Float.valueOf(t2.getDistance()));
                        case 1: //price low to high
                            return Float.compare(Common.getPriceToUSD(t1), Common.getPriceToUSD(t2));
                        case 2: //price high to low
                            return Float.compare(Float.valueOf(t2.getPrice()), Float.valueOf(t1.getPrice()));
                        case 3: //most recently published
                            try {
                                date = format.parse(t1.getUpdate_time());
                            } catch (ParseException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            long date_01 = c.getTime().getTime() - Long.valueOf(date.getTime());
                            try {
                                date = format.parse(t2.getUpdate_time());
                            } catch (ParseException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            long date_02 = c.getTime().getTime() - Long.valueOf(date.getTime());
                            return Double.compare(Double.valueOf(date_01), Double.valueOf(date_02));
                    }
                    return 0;
                }
            });
        }
    }


    private void get_items(final int mypater){
        loading = true;
        //final ProgressDialog progressDialog = new ProgressDialog(this);
        //progressDialog.show();
        Location location = Constants.getLastKnownLocation(mParam1);
        if(location != null){
            Common.latitude = (float) location.getLatitude();
            Common.longitude = (float)location.getLongitude();
        }else{
            Toast.makeText(mParam1, "No Gps Signal",Toast.LENGTH_SHORT).show();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        String user_id = new SessionManager(mParam1).getUserID();
        if(user_id.isEmpty()) user_id = "0";
        params.put("user_id", user_id);
        params.put("longitude", String.valueOf(Common.longitude));
        params.put("latitude", String.valueOf(Common.latitude));
        params.put("offset", String.valueOf(pager));

        ApiClient.getApiClient().get_items(params, new Callback<ResultGetItems>() {
            @Override
            public void success(ResultGetItems resultValue, retrofit.client.Response response) {
                //progressDialog.dismiss();
                if(resultValue.getStat().equals(Common.success)) {
                    if(tempItems != null){
                        tempItems.clear();
                    }
                    tempItems = new ArrayList<InfoItem>(resultValue.getUserData());
                    if(tempItems.size() > 0){
                        Common.isLoadedItemData = true;
                        sortItems();
                        updateGridView(pager);
                        //fab.attachToListView(mGridView);
                        mHasRequestedMore = false;
                    }else{
                        pager = 0;
                    }
                }else{
                    Common.isLoadedItemData = false;
                }
                //onLoadMoreItems();
                if(swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }

                loading = false;
            }

            @Override
            public void failure(RetrofitError error) {

                if(swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }
                //Common.isLoadedItemData = false;
                //updateGridView(pager);

            }
        });
    }
}
