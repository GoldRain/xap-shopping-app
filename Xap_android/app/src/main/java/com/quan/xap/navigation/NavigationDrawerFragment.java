package com.quan.xap.navigation;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView;
import android.widget.Toast;

import com.quan.xap.R;
import com.quan.xap.activity.BulletinActivity;
import com.quan.xap.activity.chat.ConversationsActivity;
import com.quan.xap.activity.help.HelpActivity;
import com.quan.xap.activity.login_signup.LoginActivity;
import com.quan.xap.activity.profile.ProfileActivity;
import com.quan.xap.activity.subMainActivity;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment implements View.OnClickListener {

    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    private NavigationDrawerCallbacks mCallbacks;

//    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private ExpandableListView mDrawerListView;
    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = 0;


    ArrayList<String> groupItem = new ArrayList<String>();
    ArrayList<Object> childItem = new ArrayList<Object>();

    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail;

    SessionManager session;

    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Session manager
        session = new SessionManager(getActivity().getApplicationContext());
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mDrawerListView = (ExpandableListView) inflater.inflate(
                R.layout.drawer_drawer, container, false);
        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });

        expandableListDetail = ExpandableListDataPump.getData();
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());


        mDrawerListView.setAdapter(new ExpandableListAdapter(getActivity(), expandableListTitle, expandableListDetail));

        mDrawerListView.setOnGroupExpandListener(new OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        mDrawerListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                boolean retVal = true;
                Intent intent;
                switch (groupPosition){
                    case 0:  // chat
                        // call some activity here
                        intent = new Intent(getActivity(), ConversationsActivity.class);
                        startActivity(intent);
                        break;
                    case 1:  //bulletin
                        intent = new Intent(getActivity(), BulletinActivity.class);
                        startActivity(intent);
                        break;
//                    case 2:  //collections
//                        intent = new Intent(getActivity(), CollectionActivity.class);
//                        startActivity(intent);
//                        break;
                    case 2:  //categories
                        retVal = false;
                        break;
//                    case 4:  //new in my area
//                        // call some activity here
//                        break;
                    case 3:  //invite my friends
                        customAdmobDialog();
                        break;
                    case 4:  //help
                        intent = new Intent(getActivity(), HelpActivity.class);
                        startActivity(intent);
                        break;

                }
                if(retVal){
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
                return retVal;
            }
        });

        mDrawerListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                if (groupPosition == ExpandableListAdapter.ITEM3) {
                    Common.filter_category = childPosition;
                    //Common.isFilter = true;
                    Intent intent = new Intent(getActivity(), subMainActivity.class);
                    intent.putExtra("category", childPosition);
                    startActivity(intent);
                }
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        });

        mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);

        View header= inflater.inflate(R.layout.drawer_header, null);
        Common.imgProfilePhoto = (CircleImageView) header.findViewById(R.id.imgProfilePhoto);
        if(Common.userProfile.getProfile_image() != null){
            if(Common.userProfile!=null && !Common.userProfile.getProfile_image().isEmpty()) {
                Picasso.with(getActivity()).load(Common.common_url + Common.userProfile.getProfile_image())
                        .into(Common.imgProfilePhoto);
            }
        }


        Common.txtHeader01 = (TextView) header.findViewById(R.id.txtHeader01);
        Common.txtHeader02 = (TextView) header.findViewById(R.id.txtHeader02);
        Common.setHeaderText(session, getActivity());
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Check if user is already logged in or not
                mDrawerLayout.closeDrawer(GravityCompat.START);
                Handler mHandler = new Handler();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (session.isLoggedIn()) {
                            Intent intent = new Intent(getActivity(), ProfileActivity.class);
                            startActivity(intent);
                        }else{
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                        }
                    }
                },100);
            }
        });
        mDrawerListView.addHeaderView(header);
        return mDrawerListView;
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
    }

    private void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerListView != null) {
            mDrawerListView.setItemChecked(position, true);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
//            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
//        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }

    Dialog inviteDialog;
    private void customAdmobDialog(){
        inviteDialog=new Dialog(getContext());
        inviteDialog.setContentView(R.layout.layout_invite_dialog);
        inviteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

//        inviteDialog.setTitle(getResources().getString(R.string.app_name));

        inviteDialog.findViewById(R.id.ll_facebook).setOnClickListener(this);
        inviteDialog.findViewById(R.id.imgFacebook).setOnClickListener(this);
        inviteDialog.findViewById(R.id.txtFacebook).setOnClickListener(this);
        inviteDialog.findViewById(R.id.ll_twitter).setOnClickListener(this);
        inviteDialog.findViewById(R.id.imgTwitter).setOnClickListener(this);
        inviteDialog.findViewById(R.id.txtTwitter).setOnClickListener(this);
        inviteDialog.findViewById(R.id.ll_email).setOnClickListener(this);
        inviteDialog.findViewById(R.id.imgEmail).setOnClickListener(this);
        inviteDialog.findViewById(R.id.txtEmail).setOnClickListener(this);
        inviteDialog.findViewById(R.id.ll_whatsapp).setOnClickListener(this);
        inviteDialog.findViewById(R.id.imgWhatsapp).setOnClickListener(this);
        inviteDialog.findViewById(R.id.txtWhatsapp).setOnClickListener(this);
        inviteDialog.findViewById(R.id.ll_other).setOnClickListener(this);
        inviteDialog.findViewById(R.id.imgOther).setOnClickListener(this);
        inviteDialog.findViewById(R.id.txtOther).setOnClickListener(this);

        inviteDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.layout.drawer_header:

                break;
            case R.id.ll_facebook:
                inviteFacebook();
                break;
            case R.id.imgFacebook:
                inviteFacebook();
                break;
            case R.id.txtFacebook:
                inviteFacebook();
                break;
            case R.id.ll_twitter:
                inviteTwitter();
                break;
            case R.id.imgTwitter:
                inviteTwitter();
                break;
            case R.id.txtTwitter:
                inviteTwitter();
                break;
            case R.id.ll_email:
                inviteEmail();
                break;
            case R.id.imgEmail:
                inviteEmail();
                break;
            case R.id.txtEmail:
                inviteEmail();
                break;
            case R.id.ll_whatsapp:
                inviteWhatsapp();
                break;
            case R.id.imgWhatsapp:
                inviteWhatsapp();
                break;
            case R.id.txtWhatsapp:
                inviteWhatsapp();
                break;
            case R.id.ll_other:
                inviteOthers();
                break;
            case R.id.imgOther:
                inviteOthers();
                break;
            case R.id.txtOther:
                inviteOthers();
                break;
        }
    }

    private void inviteFacebook(){
        startActivity(getOpenFacebookIntent(getContext()));
        inviteDialog.dismiss();
    }

    private static Intent getOpenFacebookIntent(Context context) {
        try {
//            context.getPackageManager().getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
            context.getPackageManager().getPackageInfo("com.facebook.snmdevice", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/254175194653125")); //Trys to make intent with FB's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/snmdevice/feed")); //catches and opens a url to the desired page
        }
    }

    private void inviteTwitter(){
        startActivity(getOpenTwitterIntent(getContext()));
        inviteDialog.dismiss();
    }

    private static Intent getOpenTwitterIntent(Context context) {
        Intent intent = null;
        try {
            // get the Twitter app if possible
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
//            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=USERID"));
//            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=SNM_GlobalTech"));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } catch (Exception e) {
            // no Twitter app, revert to browser
//            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/USERID_OR_PROFILENAME"));
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/SNM_GlobalTech"));
        }
        return intent;
    }

    private void inviteEmail(){
        sendEmail();
        inviteDialog.dismiss();
    }

    private void sendEmail(){
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
//        emailIntent.setType("text/plain");
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"email@example.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "subject here");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "body text");
//        File root = Environment.getExternalStorageDirectory();
//        String pathToMyAttachedFile = "temp/attachement.xml";
//        File file = new File(root, pathToMyAttachedFile);
//        if (!file.exists() || !file.canRead()) {
//            return;
//        }
//        Uri uri = Uri.fromFile(file);
//        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(emailIntent, "Email with..."));
    }

    private void inviteWhatsapp(){
        PackageManager pm = getContext().getPackageManager();
        try {

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = "YOUR TEXT HERE";

            PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.whatsapp");
            waIntent.setPackage(info.packageName);

            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(getContext(), "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
        }

        inviteDialog.dismiss();
    }

    private void inviteOthers(){

        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getContext().getResources().getString(R.string.app_name));
            String strShareMessage = "\nLet me recommend you this application\n\n";
            strShareMessage = strShareMessage + "https://play.google.com/store/apps/details?id="
                    + getActivity().getPackageName();
            Uri screenshotUri = Uri.parse("android.resource://packagename/drawable/image_name");
            i.setType("image/png");
            i.putExtra(Intent.EXTRA_STREAM, screenshotUri);
            i.putExtra(Intent.EXTRA_TEXT, strShareMessage);
            startActivity(Intent.createChooser(i, "Share via"));
        } catch(Exception e) {
            //e.toString();
        }

        inviteDialog.dismiss();
    }

}
