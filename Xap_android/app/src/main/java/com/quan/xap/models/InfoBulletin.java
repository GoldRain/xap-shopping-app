package com.quan.xap.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by guoquan on 4/3/2017.
 */

public class InfoBulletin implements Serializable{
    private static String InfoBulletin = "InfoBulletin";

    @SerializedName("id")
    private String id = "";
    @SerializedName("title")
    private String title = "";
    @SerializedName("content")
    private String content = "";
    @SerializedName("image")
    private String image = "";
    @SerializedName("timestamps")
    private String timestamps = "";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(String timestamps) {
        this.timestamps = timestamps;
    }
}
