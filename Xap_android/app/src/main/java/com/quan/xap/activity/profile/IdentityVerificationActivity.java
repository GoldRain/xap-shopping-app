package com.quan.xap.activity.profile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.quan.xap.R;
import com.quan.xap.activity.profile.identity.FacebookVerificationActivity;
import com.quan.xap.activity.profile.identity.GoogleVerificationActivity;
import com.quan.xap.activity.profile.identity.PhoneVerificationActivity;
import com.quan.xap.api.ApiClient;
import com.quan.xap.api.ApiClient_01;
import com.quan.xap.models.ResultEmailConform;
import com.quan.xap.models.ResultGetChat;
import com.quan.xap.models.ResultGetEmailBody;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.SessionManager;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class IdentityVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    private SessionManager session;

    ImageView imgCheck_email;
    ImageView imgCheck_facebook;
    ImageView imgCheck_google;

    boolean isEmail = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identity_verification);

        session = new SessionManager(getApplicationContext());

        initialUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setIsVerified();
    }

    private void setIsVerified(){
        if(Common.userProfile!=null) {
            if (Common.isVerified[0]) {
                imgCheck_email.setVisibility(View.VISIBLE);
            } else {
                imgCheck_email.setVisibility(View.INVISIBLE);
            }

            if (Common.isVerified[1] || (Common.userProfile.getFacebook() != null && !Common.userProfile.getFacebook().isEmpty())) {
                imgCheck_facebook.setVisibility(View.VISIBLE);
                Common.isVerified[1] = true;
            } else {
                imgCheck_facebook.setVisibility(View.INVISIBLE);
                Common.isVerified[1] = false;
            }

            if (Common.isVerified[2] || (Common.userProfile.getGoogle() != null && !Common.userProfile.getGoogle().isEmpty())) {
                imgCheck_google.setVisibility(View.VISIBLE);
                Common.isVerified[2] = true;
            } else {
                imgCheck_google.setVisibility(View.INVISIBLE);
                Common.isVerified[2] = false;
            }
        }
    }

    private void initialUI() {
        findViewById(R.id.imageBack).setOnClickListener(this);
        findViewById(R.id.rl_email).setOnClickListener(this);
        findViewById(R.id.rl_phone).setOnClickListener(this);
        findViewById(R.id.rl_facebook).setOnClickListener(this);
        findViewById(R.id.rl_google).setOnClickListener(this);

        ((TextView) findViewById(R.id.txtTitle)).setText(getResources().getString(R.string.identify_verification));

        imgCheck_email = (ImageView)findViewById(R.id.imgCheck_email);
        imgCheck_facebook = (ImageView)findViewById(R.id.imgCheck_facebook);
        imgCheck_google = (ImageView)findViewById(R.id.imgCheck_google);

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(this);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        switch (view.getId()){
            case R.id.imageBack:
                finish();
                break;
            case R.id.rl_email:
                if(!Common.isVerified[0] && !isEmail) {
                    isEmail = true;
                    if (session.getEmail().isEmpty()) {
                        alertDialog.setMessage(getResources().getString(R.string.email_uncheck_message));
                        alertDialog.show();
                    } else {
                        final ProgressDialog progressDialog = new ProgressDialog(this);
                        progressDialog.show();
                        Map<String, Object> params = new HashMap<String, Object>();
                        if (Common.userProfile != null)
                            params.put("user_id", Common.userProfile.getId());
                        ApiClient.getApiClient().verify_email(params, new Callback<ResultGetEmailBody>() {
                            @Override
                            public void success(ResultGetEmailBody resultGetItems, Response response) {
                                if(resultGetItems!=null && resultGetItems.getStat().equals(Common.success)) {
                                    Map<String, Object> params = new HashMap<String, Object>();
                                    params.put("crendentials", "@$?76ctv");
                                    params.put("from", "xap676@gmail.com");
                                    params.put("to", resultGetItems.getData().getTo_email());
                                    params.put("body", resultGetItems.getData().getBody());
                                    params.put("subject", getResources().getString(R.string.verify_email));
                                    params.put("from_name", getResources().getString(R.string.xap_support));
                                    ApiClient_01.getApiClient().wang_email(params, new Callback<ResultEmailConform>() {
                                        @Override
                                        public void success(ResultEmailConform resultEmailConform, Response response) {
                                            progressDialog.dismiss();
                                            if(resultEmailConform.getResult().equals("1")) {
                                                alertDialog.setMessage(getResources().getString(R.string.email_check_message));
                                                alertDialog.show();
                                                Common.isVerified[0] = true;
                                                isEmail = false;
                                                setIsVerified();
                                            }else{
                                                alertDialog.setMessage(getResources().getString(R.string.email_failed_message));
                                                alertDialog.show();
                                                Common.isVerified[0] = false;
                                                isEmail = false;
                                                setIsVerified();
                                            }
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }else{
                                    progressDialog.dismiss();
                                    Common.isVerified[0] = false;
                                    isEmail = false;
                                    setIsVerified();
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.email_failed_message), Toast.LENGTH_LONG).show();
                                }

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
                break;
            case R.id.rl_phone:
                intent = new Intent(IdentityVerificationActivity.this, PhoneVerificationActivity.class);
                startActivity(intent);
                break;
            case R.id.rl_facebook:
                if(!Common.isVerified[1]) {
                    intent = new Intent(IdentityVerificationActivity.this, FacebookVerificationActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.rl_google:
                if(!Common.isVerified[2]) {
                    intent = new Intent(IdentityVerificationActivity.this, GoogleVerificationActivity.class);
                    startActivity(intent);
                }
                break;
        }
    }
}
