package com.quan.xap.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.quan.xap.MainActivity;
import com.quan.xap.MyApplication;
import com.quan.xap.R;
import com.quan.xap.api.ApiClient;
import com.quan.xap.models.InfoItem;
import com.quan.xap.models.ResultGetItems;
import com.quan.xap.models.ResultUpdateUser;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class LogoActivity extends AppCompatActivity {

    private SessionManager session;


    private static final String[] INITIAL_PERMS={
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST=1337;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_logo);
        Common.Category_String = getResources().getStringArray(R.array.categories_string);
        Common.Reasons = getResources().getStringArray(R.array.reasons_string);
        Common.Menu_string = getResources().getStringArray(R.array.menu_string);


        initGps();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED  && ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED  && ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED  && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                getUserInfo();
            } else {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
            }
        }else{
            getUserInfo();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == INITIAL_REQUEST){
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getUserInfo();
            } else {
                // permission denied, boo! Disable the
                Toast.makeText(LogoActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
                LogoActivity.this.finish();
            }
            return;
        }
    }

    private void toMainActivity(){
        Intent intent = new Intent(LogoActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void getUserInfo(){
        session = new SessionManager(this);
        if(session.isLoggedIn()) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", session.getUserID());
            params.put("token", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
            ApiClient.getApiClient().get_user_info(params, new Callback<ResultUpdateUser>() {
                @Override
                public void success(ResultUpdateUser resultGetItems, Response response) {
                    if(resultGetItems.getStat() != null){
                        if(resultGetItems.getStat().equals(Common.success)) {
                            Common.userProfile = resultGetItems.getUserData();
                            if (Common.userProfile == null) {
                            }
                            get_items();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
                    get_items();
                }
            });
        }else{
            get_items();
        }
    }

//    private void setToken(){
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put("user_id", session.getUserID());
//        params.put("token", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
//
//        ApiClient.getApiClient().set_token(params, new Callback<ResultGetChat>(){
//            @Override
//            public void success(ResultGetChat resultGetItems, Response response) {
//                if(resultGetItems.getStat().equals(Common.success)){
//
//                }
//            }
//            @Override
//            public void failure(RetrofitError error) {
//            }
//        });
//    }

    private void get_items(){
        //final ProgressDialog progressDialog = new ProgressDialog(this);
        //progressDialog.show();
        Location location = getLastKnownLocation();
        if(location != null){
            Common.latitude = (float) location.getLatitude();
            Common.longitude = (float)location.getLongitude();
        }else{
            Toast.makeText(LogoActivity.this, "No Gps Signal",Toast.LENGTH_SHORT).show();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        String user_id = new SessionManager(this).getUserID();
        if(user_id.isEmpty()) user_id = "0";
        params.put("user_id", user_id);
        params.put("longitude", String.valueOf(Common.longitude));
        params.put("latitude", String.valueOf(Common.latitude));
        params.put("offset", "-1");

        ApiClient.getApiClient().get_items(params, new Callback<ResultGetItems>() {
            @Override
            public void success(ResultGetItems resultValue, retrofit.client.Response response) {
                //progressDialog.dismiss();

                if(resultValue.getStat().equals(Common.success)) {
                    if(Common.totalItems != null){
                        Common.totalItems.clear();
                    }
                    Common.totalItems = new ArrayList<InfoItem>(resultValue.getUserData());
                    Common.isLoadedItemData = true;
                }else{
                    Common.isLoadedItemData = false;
                }
                toMainActivity();
            }
            @Override
            public void failure(RetrofitError error) {

                Common.isLoadedItemData = false;
            }
        });
    }

    LocationManager locationManager;
    private void initGps() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        for (String provider : locationManager.getProviders(criteria, true)) {
            if (provider.contains("gps")) {
                //locationManager.requestSingleUpdate(provider,this,null);
            }
            return;
        }
        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }

    private Location getLastKnownLocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

}
