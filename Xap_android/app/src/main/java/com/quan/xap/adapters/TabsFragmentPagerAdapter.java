package com.quan.xap.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.quan.xap.R;
import com.quan.xap.fragments.PageFragment;
import com.quan.xap.models.InfoUserProfile;
import com.quan.xap.utility.Common;

import java.util.List;


/**
 * Created by Quan on 1/20/2017.
 */

public class TabsFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[] { "Selling", "Sold", "Faves" };
    private String tabSubCount[] = new String[] { "0", "0", "0" };
    private int intSubCount[] = new int[] { 0, 0, 0 };
    private Context context;


    public TabsFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;

        if (Common.userItems != null) {
            for(int i=0; i<Common.userItems.size();i++){
                if(Integer.valueOf(Common.userItems.get(i).getSold())==1){
                    intSubCount[1] = intSubCount[1] + 1;
                }else if(Integer.valueOf(Common.userItems.get(i).getSold())==0){
                    intSubCount[0] = intSubCount[0] + 1;
                }
            }
        }
        if(Common.totalItems!=null){ //Common.totalItems
            for(int i=0; i<Common.totalItems.size(); i++) {
                List<InfoUserProfile> favUsers = Common.totalItems.get(i).getFavUsers();
                for (InfoUserProfile item : favUsers) {
                    if (Common.userProfile != null && item.getId().equals(Common.userProfile.getId())) {
                        intSubCount[2] = intSubCount[2] + 1;
                    }
                }
            }
        }

        tabTitles[0] = context.getResources().getString(R.string.selling);
        tabTitles[1] = context.getResources().getString(R.string.sold);
        tabTitles[2] = context.getResources().getString(R.string.faves);

        Common.intMaxSize_tab = 0;
        for(int i=0;i<intSubCount.length; i++){
            if(intSubCount[i]>Common.intMaxSize_tab) Common.intMaxSize_tab = intSubCount[i];
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.newInstance(position, "param2");
//        return PageFragment_Recycler.newInstance(position, "param2");
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
//        return tabTitles[position];

        return String.valueOf(intSubCount[position]) + "\n" + tabTitles[position];
    }

//    @Override
//    public void finishUpdate(ViewGroup container) {
//        try{
//            super.finishUpdate(container);
//        } catch (NullPointerException nullPointerException){
//            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
//        }
//    }


}