package com.quan.xap.activity.profile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.quan.xap.R;
import com.quan.xap.api.ApiClient;
import com.quan.xap.models.ResultProfileNotification;
import com.quan.xap.utility.Common;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    Switch switch01;
    Switch switch02;
    Switch switch03;
    Switch switch04;
    Switch switch05;

    String notif_chat_messages = "0";
    String notif_price_change = "0";
    String notif_expired_listing = "0";
    String notif_promotions = "0";
    String notif_tips = "0";
    String fav_categories = "0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initialUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setProfileNotification();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        setProfileNotification();
    }

    private void initialUI() {
        findViewById(R.id.imageBack).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtTitle)).setText(getResources().getString(R.string.notifications));

        switch01 = (Switch) findViewById(R.id.switch01);
        switch02 = (Switch) findViewById(R.id.switch02);
        switch03 = (Switch) findViewById(R.id.switch03);
        switch04 = (Switch) findViewById(R.id.switch04);
        switch05 = (Switch) findViewById(R.id.switch05);

        if(Common.userNotification!=null) {
            if (Common.userNotification.getNotif_chat_messages() != null) {
                if (Common.userNotification.getNotif_chat_messages().equals("1")) {
                    switch01.setChecked(true);
                } else {
                    switch01.setChecked(false);
                }
            }
            if (Common.userNotification.getNotif_price_change() != null) {
                if (Common.userNotification.getNotif_price_change().equals("1")) {
                    switch02.setChecked(true);
                } else {
                    switch02.setChecked(false);
                }
            }
            if (Common.userNotification.getNotif_expired_listing() != null) {
                if (Common.userNotification.getNotif_expired_listing().equals("1")) {
                    switch03.setChecked(true);
                } else {
                    switch03.setChecked(false);
                }
            }
            if (Common.userNotification.getNotif_promotions() != null) {
                if (Common.userNotification.getNotif_promotions().equals("1")) {
                    switch04.setChecked(true);
                } else {
                    switch04.setChecked(false);
                }
            }
            if (Common.userNotification.getNotif_tips() != null) {
                if (Common.userNotification.getNotif_tips().equals("1")) {
                    switch05.setChecked(true);
                } else {
                    switch05.setChecked(false);
                }
            }
        }

        switch01.setOnCheckedChangeListener(this);
        switch02.setOnCheckedChangeListener(this);
        switch03.setOnCheckedChangeListener(this);
        switch04.setOnCheckedChangeListener(this);
        switch05.setOnCheckedChangeListener(this);
    }

    private void setProfileNotification() {
        readSwitchValue();
        Map<String, Object> params = new HashMap<String, Object>();
        if(Common.userProfile!=null) {
            params.put("user_id", Common.userProfile.getId());

            params.put("notif_chat_messages", notif_chat_messages);
            params.put("notif_price_change", notif_price_change);
            params.put("notif_expired_listing", notif_expired_listing);
            params.put("notif_promotions", notif_promotions);
            params.put("notif_tips", notif_tips);
            params.put("fav_categories", Common.userNotification.getFav_categories());
            ApiClient.getApiClient().set_profile_settings(params, new Callback<ResultProfileNotification>() {
                @Override
                public void success(ResultProfileNotification resultUpdateUser, retrofit.client.Response response) {
                    if(resultUpdateUser.getStat().equals(Common.success)) {
                        Common.userNotification = resultUpdateUser.getData();
                    }else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.set_profile_settings_failed)  , Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void readSwitchValue(){
        if(switch01.isChecked()){
            notif_chat_messages = "1";
        }else{
            notif_chat_messages = "0";
        }
        if(switch02.isChecked()){
            notif_price_change = "1";
        }else{
            notif_price_change = "0";
        }
        if(switch03.isChecked()){
            notif_expired_listing = "1";
        }else{
            notif_expired_listing = "0";
        }
        if(switch04.isChecked()){
            notif_promotions = "1";
        }else{
            notif_promotions = "0";
        }
        if(switch05.isChecked()){
            notif_tips = "1";
        }else{
            notif_tips = "0";
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageBack:
                finish();
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()){
            case R.id.switch01:
                if(b){
                    notif_chat_messages = "1";
                }else{
                    notif_chat_messages = "0";
                }
                break;
            case R.id.switch02:
                if(b){
                    notif_price_change = "1";
                }else{
                    notif_price_change = "0";
                }
                break;
            case R.id.switch03:
                if(b){
                    notif_expired_listing = "1";
                }else{
                    notif_expired_listing = "0";
                }
                break;
            case R.id.switch04:
                if(b){
                    notif_promotions = "1";
                }else{
                    notif_promotions = "0";
                }
                break;
            case R.id.switch05:
                if(b){
                    notif_tips = "1";
                }else{
                    notif_tips = "0";
                }
                break;
        }
    }
}
