package com.quan.xap.activity.profile.identity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.quan.xap.R;
import com.quan.xap.api.ApiClient;
import com.quan.xap.models.ResultGetChat;
import com.quan.xap.utility.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FacebookVerificationActivity extends AppCompatActivity implements View.OnClickListener{

    String facebook_email = "";
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_verification);

        initialUI();
    }

    private void initialUI() {
        findViewById(R.id.imageBack).setOnClickListener(this);
        findViewById(R.id.txtButton).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtTitle)).setText("Identity Verification");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageBack:
                finish();
                break;
            case R.id.txtButton:
                if(Common.userProfile!=null && Common.userProfile.getFacebook()!=null && !Common.userProfile.getFacebook().isEmpty()){
                    verify_facebook();
                }else{
                    setFacebook();
                }
                break;
        }
    }

    private void verify_facebook(){
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(this);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        Map<String, Object> params = new HashMap<String, Object>();
        if(Common.userProfile!=null) {
            params.put("user_id", Common.userProfile.getId());
            params.put("facebook", Common.userProfile.getFacebook());

            ApiClient.getApiClient().verify_facebook(params, new Callback<ResultGetChat>(){
                @Override
                public void success(ResultGetChat resultGetItems, Response response) {
                    if(resultGetItems.getStat().equals(Common.success)) {
                        alertDialog.setMessage(getResources().getString(R.string.facebook_check_message));
                        alertDialog.show();
                        Common.isVerified[1] = true;
                        finish();
                    }else{
                        Common.isVerified[1] = false;
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.verify_failed_message), Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
                }
            });
        }else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.verify_failed_message), Toast.LENGTH_LONG).show();
        }
    }

////////////////////// facebook login ///////////////////////////////////////////////////////////

    private void setFacebook(){
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        List<String> permissionNeeds = Arrays.asList("publish_actions");
        //this loginManager helps you eliminate adding a LoginButton to your UI
        LoginManager manager = LoginManager.getInstance();
        manager.logInWithPublishPermissions(this, permissionNeeds);
        manager.registerCallback(callbackManager, new FacebookCallback<LoginResult>()
        {
            @Override
            public void onSuccess(LoginResult loginResult)
            {
                signin_facebook();
            }
            @Override
            public void onCancel()
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.verify_failed_message), Toast.LENGTH_SHORT).show();
                System.out.println("onCancel");
            }
            @Override
            public void onError(FacebookException exception)
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.verify_failed_message), Toast.LENGTH_SHORT).show();
                System.out.println("onError");
            }
        });
    }

    private void signin_facebook(){
        final Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            //Toast.makeText(FacebookLogin.this,"Wait...",Toast.LENGTH_SHORT).show();
            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            try {
                                if(!object.isNull("email")){
                                    facebook_email = object.getString("email");
                                }else if(!object.isNull("name")){
                                    facebook_email = object.getString("name");
                                }else if(!object.isNull("id")){
                                    facebook_email = object.getString("id");
                                }

                                if(Common.userProfile != null) {
                                    Common.userProfile.setFacebook(facebook_email);
                                }
                                verify_facebook();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.verify_failed_message), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, name, email, gender, birthday, friends, albums");
            request.setParameters(parameters);
            request.executeAsync();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//  facebook callback result
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
