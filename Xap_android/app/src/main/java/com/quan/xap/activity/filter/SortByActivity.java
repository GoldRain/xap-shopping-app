package com.quan.xap.activity.filter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.quan.xap.R;
import com.quan.xap.utility.Common;

public class SortByActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort_by);

        initialUI();
    }

    private void initialUI() {

        findViewById(R.id.imgBack).setOnClickListener(this);
        findViewById(R.id.rl_distance).setOnClickListener(this);
        findViewById(R.id.rl_price_high).setOnClickListener(this);
        findViewById(R.id.rl_price_low).setOnClickListener(this);
        findViewById(R.id.rl_most_recently).setOnClickListener(this);





    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                finish();
                break;
            case R.id.rl_distance:
                Common.Sort_By_int =0;
                finish();
                break;
            case R.id.rl_price_high:
                Common.Sort_By_int = 1;
                finish();
                break;
            case R.id.rl_price_low:
                Common.Sort_By_int = 2;
                finish();
                break;
            case R.id.rl_most_recently:
                Common.Sort_By_int = 3;
                finish();
                break;
        }
    }
}
