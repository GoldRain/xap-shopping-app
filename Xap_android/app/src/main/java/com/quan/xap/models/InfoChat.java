package com.quan.xap.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by guoquan on 4/3/2017.
 */

public class InfoChat implements Serializable{
    private static String Chat = "Chat";

    @SerializedName("id")
    private String id = "";
    @SerializedName("from_user_id")
    private String from_user_id = "";
    @SerializedName("to_user_id")
    private String to_user_id = "";
    @SerializedName("item_id")
    private String item_id = "";
    @SerializedName("timestamps")
    private String timestamps;
    @SerializedName("message")
    private String message = "";
    @SerializedName("from_user_name")
    private String from_user_name = "";
    @SerializedName("to_user_name")
    private String to_user_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(String timestamps) {
        this.timestamps = timestamps;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFrom_user_name() {
        return from_user_name;
    }

    public void setFrom_user_name(String from_user_name) {
        this.from_user_name = from_user_name;
    }

    public String getTo_user_name() {
        return to_user_name;
    }

    public void setTo_user_name(String to_user_name) {
        this.to_user_name = to_user_name;
    }
}
