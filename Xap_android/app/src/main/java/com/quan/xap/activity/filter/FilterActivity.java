package com.quan.xap.activity.filter;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.quan.xap.R;
import com.quan.xap.activity.subMainActivity;
import com.quan.xap.constants.Constants;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.SessionManager;

import org.florescu.android.rangeseekbar.RangeSeekBar;

public class FilterActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private SupportMapFragment supportMapFragment;

    private Marker mMarker;

    float float_zoom = 6.0f;
    float float_zoom_max = 10.0f;

    LinearLayout ll_delivers;
    LinearLayout ll_accepts;

    LinearLayout ll_hours;
    LinearLayout ll_days_7;
    LinearLayout ll_days_30;

    ImageView imgCategory;
    TextView txtFilter;

    ImageView imageFilter;
    TextView txtCategory;

    RangeSeekBar sb_budget;
    RangeSeekBar sb_distance;

    TextView txtBudget;
    TextView txtDistance;


    EditText edtSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    private void initialUI() {
        FragmentManager myFragmentManager = getSupportFragmentManager();
        supportMapFragment = (SupportMapFragment) myFragmentManager
                .findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(FilterActivity.this);

        edtSearch = (EditText)findViewById(R.id.edt_search);

        ll_delivers = (LinearLayout)findViewById(R.id.ll_delivers);
        ll_accepts = (LinearLayout)findViewById(R.id.ll_accepts);

        ll_hours = (LinearLayout)findViewById(R.id.ll_hours);
        ll_days_7 = (LinearLayout)findViewById(R.id.ll_days_7);
        ll_days_30 = (LinearLayout)findViewById(R.id.ll_days_30);

        findViewById(R.id.txtReset).setOnClickListener(this);
        findViewById(R.id.txtApply).setOnClickListener(this);
        findViewById(R.id.ll_delivers).setOnClickListener(this);
        findViewById(R.id.ll_accepts).setOnClickListener(this);

        findViewById(R.id.ll_hours).setOnClickListener(this);
        findViewById(R.id.ll_days_7).setOnClickListener(this);
        findViewById(R.id.ll_days_30).setOnClickListener(this);

        findViewById(R.id.rl_category).setOnClickListener(this);
        findViewById(R.id.rl_Sort).setOnClickListener(this);

        imageFilter = (ImageView) findViewById(R.id.imageFilter);
        txtFilter = (TextView) findViewById(R.id.txtFilter);
        imageFilter.setImageDrawable(getResources().getDrawable(Common.Sort_By_Drawable[Common.Sort_By_int]));
        Common.Sort_By_String[0] = getResources().getString(R.string.sort_by_01);
        Common.Sort_By_String[1] = getResources().getString(R.string.sort_by_02);
        Common.Sort_By_String[2] = getResources().getString(R.string.sort_by_03);
        Common.Sort_By_String[3] = getResources().getString(R.string.sort_by_04);
        txtFilter.setText(getResources().getString(R.string.sort_by) + " " + Common.Sort_By_String[Common.Sort_By_int]);

        imgCategory = (ImageView) findViewById(R.id.imgCategory);
        txtCategory = (TextView) findViewById(R.id.txtCategory);
        imgCategory.setImageDrawable(getResources().getDrawable(Common.Category_Drawable[Common.Category_int]));
        txtCategory.setText(Common.Category_String[Common.Category_int]);

        txtBudget = (TextView) findViewById(R.id.txtBudget);
        txtDistance = (TextView) findViewById(R.id.txtDistance);

        sb_budget = (RangeSeekBar) findViewById(R.id.sb_budget);
        sb_distance = (RangeSeekBar) findViewById(R.id.sb_distance);
        sb_budget.setTextAboveThumbsColor(getResources().getColor(R.color.transparent));
        txtBudget.setText(0 + Common.currency_usd + " - " + getResources().getString(R.string.more_than) + " 2000" + Common.currency_usd);
        sb_distance.setTextAboveThumbsColor(getResources().getColor(R.color.transparent));
        sb_budget.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Number minValue, Number maxValue) {
//                https://plus.google.com/+AladinQ/posts/2B1fpKQWFmG
                int minLogValue = (int)((minValue.doubleValue() * minValue.doubleValue()) * Math.log10(minValue.doubleValue()))/10;
                int maxLogValue = (int)((maxValue.doubleValue() * maxValue.doubleValue()) * Math.log10(maxValue.doubleValue()))/10;
                if(maxValue.intValue() == bar.getAbsoluteMaxValue().intValue()){
                    txtBudget.setText(minLogValue + Common.currency_usd + " - " + getResources().getString(R.string.more_than) + maxLogValue + Common.currency_usd);
                }else {
                    txtBudget.setText(minLogValue + Common.currency_usd + " - " + maxLogValue + Common.currency_usd);
                }
                Common.filter_maxprice = maxLogValue;
                Common.filter_minprice = minLogValue;
            }
        });
        txtDistance.setText(getResources().getString(R.string.far) + "(10+" + getResources().getString(R.string.mile) + "s)");
        sb_distance.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Number minValue, Number maxValue) {
                if(maxValue.intValue() == bar.getAbsoluteMaxValue().intValue()){
                    txtDistance.setText(getResources().getString(R.string.far) + " (" + maxValue.intValue() + "+ " + getResources().getString(R.string.mile) + "s)");
                }else if(maxValue.intValue()>=5 ) {
                    txtDistance.setText(getResources().getString(R.string.my_area) + " (" + maxValue.intValue() + getResources().getString(R.string.mile) + "s)");
                }else if(maxValue.intValue()>=2 ) {
                    txtDistance.setText(getResources().getString(R.string.my_city) + " (" + maxValue.intValue() + getResources().getString(R.string.mile) + "s)");
                }else if(maxValue.intValue()<2 ) {
                    txtDistance.setText(getResources().getString(R.string.close) + " (" + maxValue.intValue() + getResources().getString(R.string.mile) + ")");
                }
                float_zoom = float_zoom_max - maxValue.floatValue()/2;
                supportMapFragment.getMapAsync(FilterActivity.this);
                Common.filter_distance = maxValue.floatValue();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        initialUI();
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.txtApply:
                Common.isFilter = true;
                Common.filter_category = -1;
                Common.filter_name = edtSearch.getText().toString();
                intent = new Intent(FilterActivity.this, subMainActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.txtReset:
                Common.filter_distance = -1; //mile
                Common.filter_minprice = 0;
                Common.filter_maxprice = -1;
                Common.filter_date = -1;
                Common.filter_deliver = -1;
                Common.filter_accept = -1;
                Common.filter_category = -1;
                SessionManager session = new SessionManager(this);
                session.setUpdate(true);
                finish();
                break;
            case R.id.ll_delivers:

                if(ll_delivers.getAlpha()==1.0f){
                    ll_delivers.setAlpha(0.3f);
                    Common.filter_deliver = 0;
                }else{
                    ll_delivers.setAlpha(1.0f);
                    Common.filter_deliver = 1;
                }
                break;
            case R.id.ll_accepts:
                if(ll_accepts.getAlpha()==1.0f){
                    ll_accepts.setAlpha(0.3f);
                    Common.filter_accept = 0;
                }else{
                    ll_accepts.setAlpha(1.0f);
                    Common.filter_accept = 1;
                }
                break;
            case R.id.ll_hours:
                if(ll_hours.getAlpha()==1.0f){
                    ll_hours.setAlpha(0.3f);
                }else{
                    Common.filter_date = 1;
                    ll_hours.setAlpha(1.0f);
                    ll_days_7.setAlpha(0.3f);
                    ll_days_30.setAlpha(0.3f);
                }
                break;
            case R.id.ll_days_7:
                if(ll_days_7.getAlpha()==1.0f){
                    ll_days_7.setAlpha(0.3f);
                }else{
                    Common.filter_date = 7;
                    ll_hours.setAlpha(0.3f);
                    ll_days_7.setAlpha(1.0f);
                    ll_days_30.setAlpha(0.3f);
                }
                break;
            case R.id.ll_days_30:
                if(ll_days_30.getAlpha()==1.0f){
                    ll_days_30.setAlpha(0.3f);
                }else{
                    Common.filter_date = 30;
                    ll_hours.setAlpha(0.3f);
                    ll_days_7.setAlpha(0.3f);
                    ll_days_30.setAlpha(1.0f);
                }
                break;
            case R.id.rl_category:
                intent = new Intent(FilterActivity.this, ChooseCategoryActivity.class);
                startActivity(intent);
                break;
            case R.id.rl_Sort:
                intent = new Intent(FilterActivity.this, SortByActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

//        if(Common.userProfile!=null)
//        {
//            Double dbl_lat = 22.0;
//            Double dbl_long = 108.0;
//            if(Common.userProfile.getLatitude()!=null) dbl_lat = Double.valueOf(Common.userProfile.getLatitude());
//            if(Common.userProfile.getLongitude()!=null) dbl_long = Double.valueOf(Common.userProfile.getLatitude());
//            location = new LatLng(dbl_lat, dbl_long);
//        }

        LatLng location = null;
        if(Common.latitude != 0 && Common.longitude != 0){
            location = new LatLng(Common.latitude, Common.longitude);
        }else{
            Location location1 = Constants.getLastKnownLocation(this);
            if(location1 != null){
                location = new LatLng(location1.getLatitude(), location1.getLongitude());
            }
        }
        if(location != null){
            CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(float_zoom).build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            mMarker = mMap.addMarker(new MarkerOptions().position(location));
            mMap.moveCamera(cameraUpdate);
        }


    }

}
