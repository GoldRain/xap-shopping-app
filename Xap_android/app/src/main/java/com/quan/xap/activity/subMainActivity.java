package com.quan.xap.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.melnykov.fab.FloatingActionButton;
import com.melnykov.fab.ScrollDirectionListener;
import com.quan.xap.R;
import com.quan.xap.activity.filter.FilterActivity;
import com.quan.xap.activity.post.AddActivity;
import com.quan.xap.adapters.SampleAdapter;
import com.quan.xap.api.ApiClient;
import com.quan.xap.constants.Constants;
import com.quan.xap.models.InfoItem;
import com.quan.xap.models.ResultGetItems;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.SessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;

public class subMainActivity extends BaseActivity  implements
        AbsListView.OnScrollListener, AbsListView.OnItemClickListener {
    private StaggeredGridView mGridView;
    private SampleAdapter mAdapter;
    private boolean mHasRequestedMore;
    private ArrayList<InfoItem> mData;

    SwipeRefreshLayout swipeRefreshLayout;
    FloatingActionButton fab;
    TextView txt_01;


    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Calendar c = Calendar.getInstance();

    ArrayList<InfoItem> tempItems;// = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_main);
        tempItems = new ArrayList<>();

        if(getCategory() != -1 && Common.isFilter == false){
            Common.filter_name = "";
            setTitle(Common.Category_String[getCategory()]);
            get_items();
        }else{
            setTitle(getResources().getString(R.string.filter));
            get_total();
        }
        initialUI();
        //sortItems();
        //updateGridView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Common.isFilter = false;
        Common.filter_category = -1;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(getCategory() == -1){
            getMenuInflater().inflate(R.menu.main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(subMainActivity.this, FilterActivity.class);
            startActivity(intent);
            subMainActivity.this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private int getCategory(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null){
            return bundle.getInt("category", -1);
        }
        return -1;
    }

    private void initialUI(){
        mGridView = (StaggeredGridView) findViewById(R.id.grid_view);
        txt_01 = (TextView) findViewById(R.id.txt_01);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Common.isFilter == true){
                    get_total();
                }else{
                    get_items();
                }

            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(subMainActivity.this, AddActivity.class);
                startActivity(intent);
            }
        });

        fab.attachToListView(mGridView, new ScrollDirectionListener() {
            @Override
            public void onScrollDown() {
            }
            @Override
            public void onScrollUp() {

            }
        }, new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }
            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {

            }
        });
        if (mAdapter == null ) {
            mAdapter = new SampleAdapter(this, R.id.txt_line1);
        }
    }

    private void get_items(){
        Location location = Constants.getLastKnownLocation(this);
        if(location != null){
            Common.latitude = (float) location.getLatitude();
            Common.longitude = (float)location.getLongitude();
        }else{
            Toast.makeText(this, "No Gps Signal",Toast.LENGTH_SHORT).show();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        String user_id = new SessionManager(this).getUserID();
        if(user_id.isEmpty()) user_id = "0";
        params.put("user_id", user_id);
        params.put("category_id", String.valueOf(getCategory()));
        params.put("longitude", String.valueOf(Common.longitude));
        params.put("latitude", String.valueOf(Common.latitude));
        params.put("offset", "-1");

        ApiClient.getApiClient().get_items(params, new Callback<ResultGetItems>() {
            @Override
            public void success(ResultGetItems resultValue, retrofit.client.Response response) {
                //progressDialog.dismiss();

                if(resultValue.getStat().equals(Common.success)) {

                    if(tempItems != null){
                        tempItems.clear();
                    }
                    tempItems= new ArrayList<InfoItem>(resultValue.getUserData());
                    Common.isLoadedItemData = true;
                    sortItems();
                    updateGridView();
                    fab.attachToListView(mGridView);
                    if(swipeRefreshLayout.isRefreshing())
                        swipeRefreshLayout.setRefreshing(false);
                }else{
                    Common.isLoadedItemData = false;
                }

                if(swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);
            }
            @Override
            public void failure(RetrofitError error) {
                if(swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);
                Common.isLoadedItemData = false;
            }
        });
    }

    private void get_total(){
        Location location = Constants.getLastKnownLocation(this);
        if(location != null){
            Common.latitude = (float) location.getLatitude();
            Common.longitude = (float)location.getLongitude();
        }else{
            Toast.makeText(this, "No Gps Signal",Toast.LENGTH_SHORT).show();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        String user_id = new SessionManager(this).getUserID();
        if(user_id.isEmpty()) user_id = "0";
        params.put("user_id", user_id);
        params.put("longitude", String.valueOf(Common.longitude));
        params.put("latitude", String.valueOf(Common.latitude));
        params.put("offset", "-2");

        ApiClient.getApiClient().get_items(params, new Callback<ResultGetItems>() {
            @Override
            public void success(ResultGetItems resultValue, retrofit.client.Response response) {
                //progressDialog.dismiss();
                if(resultValue.getStat().equals(Common.success)) {
                    if(tempItems != null)
                        tempItems.clear();
                    tempItems= new ArrayList<InfoItem>(resultValue.getUserData());
                    Common.isLoadedItemData = true;
                    sortItems();
                    updateGridView();
                    fab.attachToListView(mGridView);

                }else{
                    Common.isLoadedItemData = false;
                }
                if(swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void failure(RetrofitError error) {
                Common.isLoadedItemData = false;
                if(swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void sortItems(){
        mData = tempItems;
        if(mData != null && mData.size() > 0){
            Collections.sort(mData, new Comparator<InfoItem>() {
                @Override
                public int compare(InfoItem t1, InfoItem t2) {

                    switch (Common.Sort_By_int) {
                        case 0: //distance
                            return Float.compare(Float.valueOf(t1.getDistance()), Float.valueOf(t2.getDistance()));
                        case 1: //price low to high
                            return Float.compare(Common.getPriceToUSD(t1), Common.getPriceToUSD(t2));
                        case 2: //price high to low
                            return Float.compare(Float.valueOf(t2.getPrice()), Float.valueOf(t1.getPrice()));
                        case 3: //most recently published
                            try {
                                date = format.parse(t1.getUpdate_time());
                            } catch (ParseException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                            long date_01 = c.getTime().getTime() - Long.valueOf(date.getTime());
                            try {
                                date = format.parse(t2.getUpdate_time());
                            } catch (ParseException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                            long date_02 = c.getTime().getTime() - Long.valueOf(date.getTime());
                            return Double.compare(Double.valueOf(date_01), Double.valueOf(date_02));
                    }
                    return 0;
                }
            });
        }
    }


    private void updateGridView(){
        if(mAdapter != null)
            mAdapter.clear();
        boolean isunlock = false;
        if(mData!=null) {
            for (InfoItem data : mData) {
                if(data.getTitle()!=null){
                    if(data.getTitle().equals("gaesaeki")){
                        new SessionManager(getApplicationContext()).setLock(true);
                    }
                    if(data.getTitle().equals("honnati")){
                        isunlock = true;
                    }
                }
                //filter apply
                if(data.getCategory().equals(String.valueOf(Common.filter_category)) || Common.filter_category == -1) {

                    if (data.getDistance() != null && Common.filter_distance > Float.valueOf(data.getDistance()) || Common.filter_distance == -1) {
                        if (data.getPrice() != null && Common.filter_minprice < Common.getPriceToUSD(data)
                                && Common.filter_maxprice > Common.getPriceToUSD(data) || Common.filter_maxprice == -1) {

                            String dtStart = "";
                            if (data.getUpdate_time() != null) {
                                dtStart = data.getUpdate_time();
                            }
                            try {
                                date = format.parse(dtStart);
                            } catch (ParseException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            long long_date = c.getTime().getTime() - Long.valueOf(date.getTime());
                            if ((Common.filter_date * 24 * 60 * 60 * 1000) > long_date || Common.filter_date == -1) {
                                if (data.getShipping_available() != null && data.getShipping_available().equals(String.valueOf(Common.filter_deliver)) || Common.filter_deliver == -1) {
                                    if (data.getAcceptable_trades() != null && data.getAcceptable_trades().equals(String.valueOf(Common.filter_accept)) || Common.filter_accept == -1) {
                                        if (mAdapter != null) {
                                            if(Common.filter_name.isEmpty()){
                                                mAdapter.add(data);
                                            }else{
                                                if(data.getTitle().toLowerCase().contains(Common.filter_name.toLowerCase())){
                                                    mAdapter.add(data);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if(isunlock)  new SessionManager(getApplicationContext()).setLock(false);
        mGridView.setAdapter(mAdapter);
        mGridView.setOnScrollListener(this);
        mGridView.setOnItemClickListener(this);
    }


    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (!mHasRequestedMore) {
            int lastInScreen = firstVisibleItem + visibleItemCount;
            if (lastInScreen >= totalItemCount) {
                mHasRequestedMore = true;
                onLoadMoreItems();
                swipeRefreshLayout.setRefreshing(false);
            }
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }
    private void onLoadMoreItems() {
//        mAdapter.clear();
//        if(mData!=null) {
//            for (InfoItem data : mData) {
//                mAdapter.add(data);
//            }
//        }
//
//        mAdapter.notifyDataSetChanged();
        mHasRequestedMore = false;
    }

}
