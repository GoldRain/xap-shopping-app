package com.quan.xap.activity.profile;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.quan.xap.R;
import com.quan.xap.api.ApiClient;
import com.quan.xap.constants.Constants;
import com.quan.xap.dialog.CameraGalleryDialog;
import com.quan.xap.interfaces.CameraGallaryEventListener;
import com.quan.xap.models.ResultUpdateUser;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.SessionManager;
import com.quan.xap.utility.UserPicture;
import com.quan.xap.utility.loadImageHttp.ImageLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.mime.TypedFile;

public class EditProfileActivity extends AppCompatActivity implements
        View.OnClickListener{

    int PERMISSIONS_REQUEST_PERMISSION = 988;

    TextView txtTitle;
    TextView txtChangeEmail;
    ImageView imgLogOut;

    TextView txtMale;
    TextView txtFeMale;

    ImageView imgChangePhoto;
    TypedFile photofile;

    EditText editFirstName;
    EditText editLastName;
    EditText editBirthday;
    EditText editLocation;

    boolean isCameraOfBaseActivity = false;

    private SessionManager session;

    String strGender = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        if(new SessionManager(getApplicationContext()).getLock()) {
//            Toast.makeText(getApplicationContext(), "The app can not open. ask the support please", Toast.LENGTH_LONG).show();
            finish();
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA
                                , Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_PERMISSION);
            } else {

            }
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        session = new SessionManager(getApplicationContext());

        strGender = getResources().getString(R.string.female);

        initialUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setProfileImage();
    }

    private void initialUI() {
        findViewById(R.id.imageBack).setOnClickListener(this);

        findViewById(R.id.txtChangeEmail).setOnClickListener(this);
        findViewById(R.id.txtIdentifyVerification).setOnClickListener(this);
        findViewById(R.id.txtFavoriteCategories).setOnClickListener(this);
        findViewById(R.id.txtNotifications).setOnClickListener(this);

        findViewById(R.id.txtChangePhoto).setOnClickListener(this);
        imgChangePhoto = (ImageView) findViewById(R.id.imgChangePhoto);
//        imgChangePhoto.setDrawingCacheEnabled(true);
        ImageLoader imgLoader = new ImageLoader(this);
        int loader = R.drawable.loader;
        if(Common.userProfile!=null && Common.userProfile.getProfile_image()!=null) {
            if(!Common.userProfile.getProfile_image().isEmpty()) {
                imgLoader.DisplayImage(Common.common_url + Common.userProfile.getProfile_image(), loader, imgChangePhoto);
            }
        }

        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtChangeEmail = (TextView) findViewById(R.id.txtChangeEmail);
        txtChangeEmail.setText(getResources().getString(R.string.change_email) + " : " + session.getEmail());
        txtTitle.setText(getResources().getString(R.string.edit_profile));
        imgLogOut = (ImageView) findViewById(R.id.imgLogOut);
        imgLogOut.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_white_36dp));
        imgLogOut.setVisibility(View.VISIBLE);
        imgLogOut.setOnClickListener(this);

        txtMale = (TextView) findViewById(R.id.txtMale);
        txtFeMale = (TextView) findViewById(R.id.txtFeMale);
        txtMale.setOnClickListener(this);
        txtFeMale.setOnClickListener(this);
        if(Common.userProfile!=null && Common.userProfile.getGender()!=null) {
            strGender = Common.userProfile.getGender();
        }
        if(strGender.equals(getResources().getString(R.string.male))){
            txtMale.performClick();
        }else if(strGender.equals(getResources().getString(R.string.female))){
            txtFeMale.performClick();
        }

        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editBirthday = (EditText) findViewById(R.id.editBirthday);
        editLocation = (EditText) findViewById(R.id.editLocation);

        if(Common.userProfile!=null) {
            editFirstName.setText(Common.userProfile.getFirstname());
            editLastName.setText(Common.userProfile.getLastname());
            editBirthday.setText(Common.userProfile.getBirthday());
            editLocation.setText(Common.userProfile.getAddress());
        }
    }




    private void updateUser(){
        Map<String, Object> params = new HashMap<String, Object>();

        if(photofile == null) {
            File imageFile = Common.savebitmap("photo", ((BitmapDrawable)imgChangePhoto.getDrawable()).getBitmap());
            photofile = new TypedFile("image/*", imageFile);
        }

        params.put("id", session.getUserID());
        params.put("photo", photofile);

        params.put("user_name", session.getUsername());
        params.put("first_name", editFirstName.getText().toString().trim());
        params.put("last_name", editLastName.getText().toString().trim());
        params.put("address", editLocation.getText().toString().trim());

        if(Common.latitude == 0 || Common.longitude == 0){
            Location location = Constants.getLastKnownLocation(this);
            if(location != null){
                Common.latitude = (float)location.getLatitude();
                Common.longitude = (float)location.getLongitude();
            }
        }
        params.put("latitude", String.valueOf(Common.latitude));
        params.put("longitude", String.valueOf(Common.longitude));
        params.put("gender", strGender);
        params.put("birthday", editBirthday.getText().toString().trim());
        params.put("email", session.getEmail());

        ApiClient.getApiClient().update_user(params, new Callback<ResultUpdateUser>() {
            @Override
            public void success(ResultUpdateUser resultUpdateUser, retrofit.client.Response response) {
                if(resultUpdateUser.getStat().equals(Common.success)) {
                    Common.userProfile = resultUpdateUser.getUserData();
                }else{
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.user_update_failed)  , Toast.LENGTH_SHORT).show();
                }
//                Toast.makeText(getApplicationContext(), resultUpdateUser.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.imageBack:
                finish();
                break;
            case R.id.imgLogOut:
                updateUser();
                finish();
                break;
            case R.id.txtMale:
                strGender = getResources().getString(R.string.male);
                txtMale.setBackgroundColor(getResources().getColor(R.color.white));
                txtMale.setTextColor(getResources().getColor(R.color.login_text_color_02));
                txtFeMale.setBackground(null);
                txtFeMale.setTextColor(getResources().getColor(R.color.white));
                break;
            case R.id.txtFeMale:
                strGender = getResources().getString(R.string.female);
                txtMale.setBackground(null);
                txtMale.setTextColor(getResources().getColor(R.color.white));
                txtFeMale.setBackgroundColor(getResources().getColor(R.color.white));
                txtFeMale.setTextColor(getResources().getColor(R.color.login_text_color_02));
                break;
            case R.id.txtChangePhoto:
                isCameraOfBaseActivity = true;
                openDonatorDialog();
                break;
            case R.id.txtChangeEmail:
                setChangeEmailDialog();
                break;
            case R.id.txtIdentifyVerification:
                intent = new Intent(EditProfileActivity.this, IdentityVerificationActivity.class);
                startActivity(intent);
                break;
            case R.id.txtFavoriteCategories:
                intent = new Intent(EditProfileActivity.this, FavoriteCategoriesActivity.class);
                startActivity(intent);
                break;
            case R.id.txtNotifications:
                intent = new Intent(EditProfileActivity.this, NotificationActivity.class);
                startActivity(intent);
                break;

        }
    }

    private void setChangeEmailDialog(){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_edit_profile_change_email);
        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.list_item_sample_boundary_conner_10dp));

        final EditText editEmail = (EditText) dialog.findViewById(R.id.editEmail);
        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        TextView txtOK = (TextView) dialog.findViewById(R.id.txtOK);

        editEmail.setText(session.getEmail());
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        txtOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setEmail(editEmail.getText().toString().trim());
                txtChangeEmail.setText(session.getEmail());
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void setProfileImage(){
        if(Common.outputFileUri_User!=null && !Common.outputFileUri_User.getPath().isEmpty()) {
            try {
                imgChangePhoto.setImageBitmap(new UserPicture(Common.outputFileUri_User, getContentResolver()).getBitmap());
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }else{
        }
    }

    ////////////open camera and gallery ///////////////////////////////
    private void openDonatorDialog() {
        CameraGalleryDialog rDonator = new CameraGalleryDialog(this);
        rDonator.setEventListener(new CameraGallaryEventListener() {
            @Override
            public void selectCamera() {
                testCamera();

            }
            @Override
            public void selectGallary() {
                testGallery();
            }
        });
        rDonator.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = rDonator.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        rDonator.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.list_item_sample_boundary_conner_10dp));
        rDonator.show();
    }


    ////////////
    // Camera //
    ////////////

//    private static final int REQUEST_TAKE_PHOTO = 1;
//
//
//
//    String mCurrentPhotoPath;
//
//    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
//    void startCamera() {
//        try {
//            dispatchTakePictureIntent();
//        } catch (IOException e) {
//        }
//    }
//
//    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
//    void showRationaleForCamera(final PermissionRequest request) {
//        new AlertDialog.Builder(this)
//                .setMessage("Access to External Storage is required")
//                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        request.proceed();
//                    }
//                })
//                .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        request.cancel();
//                    }
//                })
//                .show();
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
//            // Show the thumbnail on ImageView
//            Uri imageUri = Uri.parse(mCurrentPhotoPath);
//            File file = new File(imageUri.getPath());
//            try {
//                InputStream ims = new FileInputStream(file);
//                ivPreview.setImageBitmap(BitmapFactory.decodeStream(ims));
//            } catch (FileNotFoundException e) {
//                return;
//            }
//
//            // ScanFile so it will be appeared on Gallery
//            MediaScannerConnection.scanFile(MainActivity.this,
//                    new String[]{imageUri.getPath()}, null,
//                    new MediaScannerConnection.OnScanCompletedListener() {
//                        public void onScanCompleted(String path, Uri uri) {
//                        }
//                    });
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
//    }
//
//    private File createImageFile() throws IOException {
//        // Create an image file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        String imageFileName = "JPEG_" + timeStamp + "_";
//        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_DCIM), "Camera");
//        File image = File.createTempFile(
//                imageFileName,  /* prefix */
//                ".jpg",         /* suffix */
//                storageDir      /* directory */
//        );
//
//        // Save a file: path for use with ACTION_VIEW intents
//        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
//        return image;
//    }
//
//    private void dispatchTakePictureIntent() throws IOException {
//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        // Ensure that there's a camera activity to handle the intent
//        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//            // Create the File where the photo should go
//            File photoFile = null;
//            try {
//                photoFile = createImageFile();
//            } catch (IOException ex) {
//                // Error occurred while creating the File
//                return;
//            }
//            // Continue only if the File was successfully created
//            if (photoFile != null) {
//                Uri photoURI = Uri.fromFile(createImageFile());
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
//                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
//            }
//        }
//    }






    //////////////////gallery//////////////////////////
    private static final int SELECT_PICTURE = 999;
    private void testGallery(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }
//
    //////////////////////////////camera//////////////////////
    int TAKE_PHOTO_CODE = 888;
    public static int count = 0;

    private void testCamera(){
        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();
        count++;
        String file = dir+count+".jpg";
        Common.newfile = new File(file);
        try {   Common.newfile.createNewFile();   }
        catch (IOException e)
        {                }
        Common.outputFileUri_User = Uri.fromFile(Common.newfile);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Common.outputFileUri_User);
            cameraIntent.addFlags(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && isCameraOfBaseActivity) {
            if (requestCode == TAKE_PHOTO_CODE) {
                Log.d("CameraDemo", "Pic saved");
                if(Common.outputFileUri_User!=null && !Common.outputFileUri_User.getPath().isEmpty()) {
                    try {
                        imgChangePhoto.setVisibility(View.VISIBLE);
                        imgChangePhoto.setImageBitmap(new UserPicture(Common.outputFileUri_User, getContentResolver()).getBitmap());

                        File imageFile = Common.savebitmap("photo", imgChangePhoto.getDrawingCache());
                        photofile = new TypedFile("image/*", imageFile);

                        updateUser();

                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }else{
                }
            }
            if (requestCode == SELECT_PICTURE) {
                Common.outputFileUri_User = data.getData();
                try {
                    imgChangePhoto.setVisibility(View.VISIBLE);
                    imgChangePhoto.setImageBitmap(new UserPicture(Common.outputFileUri_User, getContentResolver()).getBitmap());

                    File imageFile = Common.savebitmap("photo", imgChangePhoto.getDrawingCache());
                    photofile = new TypedFile("image/*", imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("camera activity", "Failed to load image", e);
                }
            }
        }
        isCameraOfBaseActivity = false;
    }

    ///////////////////////////////////

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            finish();
        }else
        {

        }
    }
}
