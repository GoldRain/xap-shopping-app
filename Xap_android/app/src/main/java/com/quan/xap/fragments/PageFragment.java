package com.quan.xap.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.etsy.android.grid.StaggeredGridView;
import com.quan.xap.R;
import com.quan.xap.adapters.SampleAdapter;
import com.quan.xap.models.InfoItem;
import com.quan.xap.models.InfoUserProfile;
import com.quan.xap.utility.Common;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PageFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PageFragment extends Fragment  implements
        AbsListView.OnScrollListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;  //page number
    private String mParam2;

    List<String> titleList = new ArrayList<>();
    private static final String TAG = PageFragment.class.getSimpleName();

    private OnFragmentInteractionListener mListener;

    public PageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PageFragment newInstance(int param1, String param2) { //param1 is page number
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        titleList.add(getResources().getString(R.string.profile_tab_page_text_01));
        titleList.add(getResources().getString(R.string.profile_tab_page_text_02));
//        titleList.add(getResources().getString(R.string.profile_tab_page_text_03));
        titleList.add(getResources().getString(R.string.profile_tab_page_text_04));

        if (mAdapter != null) {
            for(int i=0; i<3; i++) {
                mAdapter.add(new SampleAdapter(getActivity(), R.id.txt_line1));
            }
        }
    }


    TextView txt_page_text;

    StaggeredGridView mGridView;
    private boolean mHasRequestedMore;
    private List<SampleAdapter> mAdapter = new ArrayList<>();
    private ArrayList<InfoItem> mData = new ArrayList<>();

    LinearLayout ll_reviews;

//    SwipeRefreshLayout swipeRefreshLayout;
//    FloatingActionButton fab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_page, container, false);

        initialUI(view);
        return view;
    }


    private void initialUI(View view){
        txt_page_text = (TextView) view.findViewById(R.id.txt_page_text);
        txt_page_text.setText(titleList.get(mParam1));

        mGridView = (StaggeredGridView) view.findViewById(R.id.grid_view);
        ll_reviews = (LinearLayout) view.findViewById(R.id.ll_reviews);

//        if(mParam1==2){
//            mGridView.setVisibility(View.GONE);
//            ll_reviews.setVisibility(View.VISIBLE);
//        }else {
            mGridView.setVisibility(View.VISIBLE);
            ll_reviews.setVisibility(View.GONE);
            updateGridView();
//        }

    }


    private void updateGridView(){
        if (mData!=null) mData.clear();
        if (Common.userItems != null) {
            for(int i=0; i<Common.userItems.size();i++){
                if(Integer.valueOf(Common.userItems.get(i).getSold())==mParam1){
                    mData.add(Common.userItems.get(i));
                }
            }
        }
        if(mParam1==2 && Common.totalItems!=null){ //Common.totalItems
            for(int i=0; i<Common.totalItems.size(); i++) {
                List<InfoUserProfile> favUsers = Common.totalItems.get(i).getFavUsers();
                for (InfoUserProfile item : favUsers) {
                    if (Common.userProfile != null && item.getId().equals(Common.userProfile.getId())) {
                        mData.add(Common.totalItems.get(i));
                    }
                }
            }
        }

        if(mData!=null) {
            mAdapter.get(mParam1).clear();
            for (InfoItem data : mData) {
                mAdapter.get(mParam1).add(data);
            }

            if(mData.size()!=0) {
                setPagerHeight(mData.size());
            }
        }

        mAdapter.get(mParam1).notifyDataSetChanged();
        mGridView.setAdapter(mAdapter.get(mParam1));
        mGridView.setOnScrollListener(this);

    }

//    int max_size = 0;
    private void setPagerHeight(int items_count){
//        if(items_count >= max_size) max_size = items_count;
        if(Common.pager!=null) {
            ViewGroup.LayoutParams layoutParams = Common.pager.getLayoutParams();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            int page_height = (int) ((Common.intMaxSize_tab + 1) / 2 * width) + 200; //* 1.5
            layoutParams.height = page_height; //this is in dp
            Common.pager.setLayoutParams(layoutParams);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateGridView();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int lastInScreen = firstVisibleItem + visibleItemCount;
        if (lastInScreen >= totalItemCount) {
            Log.d(TAG, "onScroll lastInScreen - so load more");
            mHasRequestedMore = true;

//            updateGridView();
        }


    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
