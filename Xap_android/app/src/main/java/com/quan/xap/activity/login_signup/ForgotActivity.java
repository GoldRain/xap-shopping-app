package com.quan.xap.activity.login_signup;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quan.xap.R;
import com.quan.xap.api.ApiClient;
import com.quan.xap.api.ApiClient_01;
import com.quan.xap.models.ResultEmailConform;
import com.quan.xap.models.ResultGetEmailBody;
import com.quan.xap.utility.Common;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ForgotActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgMark;
    RelativeLayout rl_sign_in;
    EditText editEmail;
    TextView txt_sign_account;

    boolean isClickable = false;

    AlphaAnimation alphaAnimation;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            toSignInRegister(Common.SIGN);
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        initialUI();
    }

    private void initialUI(){
        imgMark = (ImageView) findViewById(R.id.imgMark);
        rl_sign_in = (RelativeLayout) findViewById(R.id.rl_sign_in);
        editEmail = (EditText) findViewById(R.id.editEmail);
        txt_sign_account = (TextView) findViewById(R.id.txt_sign_account);

        imgMark.setVisibility(View.INVISIBLE);
        rl_sign_in.setVisibility(View.INVISIBLE);

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setViewsVisibility();
            }
        });

        txt_sign_account.setOnClickListener(this);

        alphaAnimation = new AlphaAnimation(0.1f, 1.0f);
        alphaAnimation.setDuration(Common.Duration);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        rl_sign_in.setVisibility(View.VISIBLE);
                        imgMark.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                rl_sign_in.startAnimation(alphaAnimation);
            }
        }, 1000);

    }

    private void setViewsVisibility(){
        String email = editEmail.getText().toString();
        if(!email.isEmpty()){
            if(Common.isValidEmail(email)) {
                txt_sign_account.setBackground(getResources().getDrawable(
                        R.drawable.sign_register_button_boundary_conner_active));
                isClickable = true;
            }else{
                isClickable = false;
                txt_sign_account.setBackground(getResources().getDrawable(
                        R.drawable.sign_register_button_boundary_conner_inactive));
//                Toast.makeText(this, "The Email Address is not suitable", Toast.LENGTH_LONG).show();
            }
        }else{
            txt_sign_account.setBackground(getResources().getDrawable(
                    R.drawable.sign_register_button_boundary_conner_inactive));
            isClickable = false;
        }
    }

    private void toSignInRegister(String strMark){
        final Bundle bndlanimation_bottom_top =
                ActivityOptions.makeCustomAnimation(getApplicationContext(),
                        R.animator.animator_right_00,R.animator.animator_left_00).toBundle();
        Intent intent = new Intent(ForgotActivity.this, SignRegisterActivity.class);
        intent.putExtra(Common.Mark, strMark);
        startActivity(intent, bndlanimation_bottom_top);
        finish();
    }

    @Override
    public void onClick(View view) {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(this);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        switch (view.getId()){
            case R.id.txt_sign_account:
                if(isClickable){
                    final ProgressDialog progressReportData = new ProgressDialog(this);
                    progressReportData.show();
                    Map<String, Object> params = new HashMap<String, Object>();
                    if (Common.userProfile != null)
                        params.put("email", editEmail.getText().toString().trim());
                    ApiClient.getApiClient().forget_password(params, new Callback<ResultGetEmailBody>() {
                        @Override
                        public void success(ResultGetEmailBody resultGetItems, Response response) {
                            if(resultGetItems!=null && resultGetItems.getStat().equals(Common.success)) {
                                Map<String, Object> params = new HashMap<String, Object>();
                                params.put("crendentials", "@$?76ctv");
                                params.put("from", "xap676@gmail.com");
                                params.put("to", resultGetItems.getData().getTo_email());
                                params.put("body", resultGetItems.getData().getBody());
                                params.put("subject", getResources().getString(R.string.recover_password));
                                params.put("from_name", getResources().getString(R.string.xap_support));
                                ApiClient_01.getApiClient().wang_email(params, new Callback<ResultEmailConform>() {
                                    @Override
                                    public void success(ResultEmailConform resultEmailConform, Response response) {
                                        if(progressReportData != null && progressReportData.isShowing()){
                                            progressReportData.dismiss();
                                        }
                                        if(resultEmailConform!=null) {
                                            if(resultEmailConform.getError()!=null) alertDialog.setTitle(resultEmailConform.getError());
                                            if(resultEmailConform.getDetail()!=null) alertDialog.setMessage(resultEmailConform.getDetail());
                                            alertDialog.show();
                                            if (resultEmailConform.getResult().equals("1")) {
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_email), Toast.LENGTH_LONG).show();
                                                toSignInRegister(Common.SIGN);
                                            }
                                        }else{
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.email_failed_message), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                    @Override
                                    public void failure(RetrofitError error) {
                                        progressReportData.dismiss();
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.email_failed_message), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }else{
                                if(progressReportData != null && progressReportData.isShowing()){
                                    progressReportData.dismiss();
                                }
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.email_failed_message), Toast.LENGTH_LONG).show();
                            }
                        }
                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
                        }
                    });
                }
                break;
        }
    }
}
