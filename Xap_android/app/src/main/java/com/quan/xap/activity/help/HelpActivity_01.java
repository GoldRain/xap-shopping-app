package com.quan.xap.activity.help;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.quan.xap.R;

public class HelpActivity_01 extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_01);

        initialUI();
    }

    private void initialUI(){
        ((TextView)findViewById(R.id.txtTitle)).setText(getResources().getString(R.string.help));
        findViewById(R.id.imageBack).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageBack:
                finish();
                break;
        }
    }
}
