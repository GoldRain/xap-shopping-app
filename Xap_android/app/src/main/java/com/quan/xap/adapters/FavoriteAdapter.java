package com.quan.xap.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.quan.xap.R;
import com.quan.xap.utility.Common;

import java.util.ArrayList;
import java.util.List;

//import com.sinch.android.rtc.messaging.WritableMessage;

public class FavoriteAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private Activity getActivity;
    List<String> categories = new ArrayList<>();

    public FavoriteAdapter(Activity activity, List<String> data) {
        getActivity = activity;
        layoutInflater = activity.getLayoutInflater();
        categories = data;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        ViewHolder holder = null;
//        if (convertView == null) {
            int res = R.layout.layout_favorite_adapter;
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(res, viewGroup, false);
            holder.txtItem = (TextView) convertView.findViewById(R.id.txtItem);
            holder.checkItem = (CheckBox) convertView.findViewById(R.id.checkItem);
//            convertView.setTag(holder);
//            holder.checkItem.setTag(Common.favorite_categories[i]);
//        }else {
//            holder = (ViewHolder)convertView.getTag();
//            holder.checkItem.setTag(Common.favorite_categories[i]);
//        }

        holder.txtItem.setText(categories.get(i));
        final ViewHolder finalHolder = holder;
        holder.txtItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finalHolder.checkItem.setChecked(!finalHolder.checkItem.isChecked());
                }
            });
        if(Common.favorite_categories[i]!=null && !Common.favorite_categories[i].isEmpty()){
            if(Common.favorite_categories[i].equals("0")){
                holder.checkItem.setChecked(false);
            }else{
                holder.checkItem.setChecked(true);
            }
        }
        holder.checkItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    Common.favorite_categories[i] = Common.Category_String[i];
                }else{
                    Common.favorite_categories[i] = "0";
                }
            }
        });

        return convertView;
    }


    private static class ViewHolder {
        private TextView txtItem;
        private CheckBox checkItem;
    }
}

