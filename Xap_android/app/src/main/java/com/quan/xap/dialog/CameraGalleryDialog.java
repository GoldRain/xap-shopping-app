package com.quan.xap.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.quan.xap.R;
import com.quan.xap.interfaces.CameraGallaryEventListener;


/**
 * Created by JJM on 12/2/2016.
 */

public class CameraGalleryDialog extends Dialog implements View.OnClickListener {

    int PERMISSIONS_REQUEST_PERMISSION = 999;

    private Context c;
    private Dialog d;
    private ImageView camera;
    private ImageView gallary;


    public CameraGalleryDialog(Context c) {
        super(c);
        // TODO Auto-generated constructor stub
        this.c = c;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_camera_gallary);
        camera = (ImageView) findViewById(R.id.imgCamera);
        camera.setOnClickListener(this);
        gallary = (ImageView) findViewById(R.id.imgGallary);
        gallary.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgCamera:
                eventListener.selectCamera();
                break;
            case R.id.imgGallary:
                eventListener.selectGallary();
                break;
            default:
                break;
        }
        dismiss();
    }
    private CameraGallaryEventListener eventListener;
    public void setEventListener(CameraGallaryEventListener listener){
        this.eventListener=listener;
    }




}
