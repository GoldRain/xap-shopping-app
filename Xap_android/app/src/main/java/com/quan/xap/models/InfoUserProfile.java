package com.quan.xap.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by guoquan on 4/3/2017.
 */

public class InfoUserProfile implements Serializable{
    private static String USER_PROFILE = "user_profile";

    @SerializedName("id")
    private String id = "";
    @SerializedName("user_name")
    private String user_name = "";
    @SerializedName("first_name")
    private String first_name = "";
    @SerializedName("last_name")
    private String last_name = "";
    @SerializedName("address")
    private String address = "";
    @SerializedName("latitude")
    private String latitude = "0";
    @SerializedName("longitude")
    private String longitude = "0";
    @SerializedName("gender")
    private String gender = "";
    @SerializedName("birthday")
    private String birthday = "";
    @SerializedName("email")
    private String email = "";
    @SerializedName("password")
    private String password = "";
    @SerializedName("profile_image")
    private String profile_image = "";
    @SerializedName("phone_number")
    private String phone_number = "";
    @SerializedName("facebook")
    private String facebook = "";
    @SerializedName("google")
    private String google = "";
    @SerializedName("verif_email")
    private String verif_email = "";
    @SerializedName("verif_phone")
    private String verif_phone = "";
    @SerializedName("device_token")
    private String device_token = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return user_name;
    }

    public void setUsername(String username) {
        this.user_name = username;
    }

    public String getFirstname() {
        return first_name;
    }

    public void setFirstname(String firstname) {
        this.first_name = firstname;
    }

    public String getLastname() {
        return last_name;
    }

    public void setLastname(String lastname) {
        this.last_name = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getGoogle() {
        return google;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public String getVerif_email() {
        return verif_email;
    }

    public void setVerif_email(String verif_email) {
        this.verif_email = verif_email;
    }

    public String getVerif_phone() {
        return verif_phone;
    }

    public void setVerif_phone(String verif_phone) {
        this.verif_phone = verif_phone;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }
}
