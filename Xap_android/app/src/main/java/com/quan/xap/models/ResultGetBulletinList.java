package com.quan.xap.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoquan on 4/12/2017.
 */

public class ResultGetBulletinList implements Serializable{
    @SerializedName("stat")
    private String stat;
    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<InfoBulletin> datalist = new ArrayList<>();

    public String getStat() {
        return stat;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public List<InfoBulletin> getDataList() {
        return datalist;
    }

}
