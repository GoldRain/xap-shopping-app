package com.quan.xap.navigation;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.quan.xap.R;
import com.quan.xap.utility.Common;

import java.util.HashMap;
import java.util.List;


public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, List<String>> expandableListDetail;

    public static int ITEM1 = 0;
    public static int ITEM2 = 1;
    public static int ITEM3 = 2;
    public static int ITEM4 = 2;
    public static int ITEM5 = 3;
    public static int ITEM6 = 3;
    public static int ITEM7 = 3;


    public static int SUBITEM_1 = 0;
    public static int SUBITEM_2 = 1;
    public static int SUBITEM_3 = 2;
    public static int SUBITEM_4 = 3;
    public static int SUBITEM_5 = 4;
    public static int SUBITEM_6 = 5;
    public static int SUBITEM_7 = 6;
    public static int SUBITEM_8 = 7;
    public static int SUBITEM_9 = 8;
    public static int SUBITEM_10 = 9;
    public static int SUBITEM_11 = 10;
    public static int SUBITEM_12 = 11;





    public ExpandableListAdapter(Context context, List<String> expandableListTitle,
                                 HashMap<String, List<String>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;


    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = (String) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }
        TextView expandedListTextView = (TextView) convertView
                .findViewById(R.id.expandedListItem);
        expandedListTextView.setText(expandedListText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.listTitle);
        listTitleTextView.setTypeface(null, Typeface.NORMAL);
        listTitleTextView.setText(listTitle);


        TextView listTitleTextArrowView = (TextView) convertView.findViewById(R.id.listTitleArrow);
        listTitleTextArrowView.setTypeface(null, Typeface.NORMAL);
        listTitleTextArrowView.setTypeface(FontManager.getTypeface(context, FontManager.FONTAWESOME));

        // set icons for menu items
        ImageView listTitleTextIconView = (ImageView) convertView.findViewById(R.id.listTitleIcon);
        listTitleTextIconView.setScaleX(0.7f);
        listTitleTextIconView.setScaleY(0.7f);
        switch (listPosition){
            case 0:
                listTitleTextIconView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_chat_white_36dp));
                Common.listTitleTextArrowView = listTitleTextArrowView;
                Common.listTitleTextArrowView.setVisibility(View.INVISIBLE);
                Common.listTitleTextArrowView.setBackground(context.getResources().getDrawable(R.drawable.item_count_num));
                Common.listTitleTextArrowView.setTextSize(context.getResources().getDimension(R.dimen.badge_text_size_menu));
                Common.listTitleTextArrowView.setTextColor(context.getResources().getColor(R.color.white));
                break;
            case 1:
                listTitleTextIconView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_art_track_white_36dp));
                break;
//            case 2:
//                listTitleTextIconView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_collections_white_36dp));
//                break;
            case 2:
                listTitleTextIconView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_dns_white_36dp));
                break;
//            case 4:
//                listTitleTextIconView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_insert_invitation_white_36dp));
//                break;
            case 3:
                listTitleTextIconView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_assignment_ind_white_36dp));
                break;
            case 4:
                listTitleTextIconView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_help_outline_white_36dp));
                break;
        }

        // set arrow icons for relevant items
        if (listPosition == ITEM3) {
            if (!isExpanded)
                listTitleTextArrowView.setText(context.getResources().getString(R.string.fa_chevron_right));
            else
                listTitleTextArrowView.setText(context.getResources().getString(R.string.fa_chevron_down));
        } else {
            listTitleTextArrowView.setText("");
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}
