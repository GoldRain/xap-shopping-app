package com.quan.xap.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.quan.xap.R;
import com.quan.xap.adapters.BulletinAdapter;
import com.quan.xap.api.ApiClient;
import com.quan.xap.models.InfoBulletin;
import com.quan.xap.models.ResultGetBulletinList;
import com.quan.xap.utility.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class BulletinActivity extends AppCompatActivity implements View.OnClickListener {

    ListView listView;
    TextView txtWehave;
    List<InfoBulletin> bulletinList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulletin);

        initialUI();

        getBulletin();
    }

    private void initialUI(){
        findViewById(R.id.imageBack).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtTitle)).setText(Common.Menu_string[1]);
        listView = (ListView) findViewById(R.id.listView);
        txtWehave = (TextView) findViewById(R.id.txtWehave);
    }




    private void setListView(){
        BulletinAdapter ItemAdapter = new BulletinAdapter(this, bulletinList);
        listView.setAdapter(ItemAdapter);
        ItemAdapter.notifyDataSetChanged();

    }


    private void getBulletin(){
        bulletinList.clear();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", "0");
        ApiClient.getApiClient().get_bulletin(params, new Callback<ResultGetBulletinList>(){
            @Override
            public void success(ResultGetBulletinList resultGetItems, Response response) {
                if(resultGetItems.getStat().equals(Common.success)) {
                    bulletinList = resultGetItems.getDataList();
                    if (bulletinList != null) {
                        if (bulletinList.size() > 0) txtWehave.setVisibility(View.GONE);
                        setListView();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.get_bulletin_failed)  , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageBack:
                finish();
                break;
        }
    }
}
