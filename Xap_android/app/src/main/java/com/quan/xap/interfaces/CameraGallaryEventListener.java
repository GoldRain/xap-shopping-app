package com.quan.xap.interfaces;

/**
 * Created by JJM on 12/8/2016.
 */

public interface CameraGallaryEventListener {
    public void selectCamera();
    public void selectGallary();
}
