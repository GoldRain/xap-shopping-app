package com.quan.xap.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Quan on 9/28/2017.
 */

public class ClassChatList {

    public String item_id = "";
    public String my_user_id = "";
    public String other_user_id = "";
    public int unread_number = 0;
    public List<InfoChat> chatList = new ArrayList<>();
    public InfoItem item = new InfoItem();

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getMy_user_id() {
        return my_user_id;
    }

    public void setMy_user_id(String my_user_id) {
        this.my_user_id = my_user_id;
    }

    public String getTo_user_id() {
        return other_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.other_user_id = to_user_id;
    }

    public List<InfoChat> getChatList() {
        return chatList;
    }

    public void setChatList(List<InfoChat> chatList) {
        this.chatList = chatList;
    }

    public InfoItem getItem() {
        return item;
    }

    public void setItem(InfoItem item) {
        this.item = item;
    }

    public int getUnread_number() {
        return unread_number;
    }

    public void setUnread_number(int unread_number) {
        this.unread_number = unread_number;
    }
}
