package com.quan.xap.adapters;

/**
 * Created by JJM on 12/7/2016.
 */

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.quan.xap.R;
import com.quan.xap.activity.MyProductionActivity;
import com.quan.xap.activity.ProductionActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class SlidingImageAdapter extends PagerAdapter {


    private ArrayList<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;


    public SlidingImageAdapter(Context context, ArrayList<String> IMAGES) {
        this.context = context;
        this.IMAGES=IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        Picasso.with(context).cancelRequest(imageView);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    ImageView imageView;

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {

        View imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);
        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
        //imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        if (!isTablet(context)) {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            if(context instanceof  MyProductionActivity){
                ((MyProductionActivity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            }else if(context instanceof ProductionActivity){
                ((ProductionActivity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            }
            //int width = displaymetrics.widthPixels / 2;
            //final int height = displaymetrics.heightPixels;

            Picasso.with(context)
                    .load(IMAGES.get(position))
                    //.resize((int)dipToPixels(context, 250), (int)dipToPixels(context, 350))
                    .placeholder(
                            context.getResources().getDrawable(R.drawable.item_placeholder))
                    .into(imageView,  new com.squareup.picasso.Callback(){
                        @Override
                        public void onSuccess() {
                            int width = ((BitmapDrawable) imageView.getDrawable()).getBitmap().getWidth();
                            int height = ((BitmapDrawable) imageView.getDrawable()).getBitmap().getHeight();
                            int xWidth = (int)(width * ( dipToPixels(context, 350)/height) );
                            Picasso.with(context)
                                    .load(IMAGES.get(position))
                                    .resize(xWidth, (int)dipToPixels(context, 350))
                                    .into(imageView);
                        }
                        @Override
                        public void onError() {

                        }
                    });

        }
//        imageView.getLayoutParams().height = (int)dipToPixels(context, 350);
//        imageView.getLayoutParams().width  = (int)dipToPixels(context, 200);
//        imageView.requestLayout();
//        ImageLoader imgLoader = new ImageLoader(context);
//        int loader = R.drawable.loader;
//        imgLoader.DisplayImage(IMAGES.get(position), loader, imageView);/////
        view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }



    public static float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }


}