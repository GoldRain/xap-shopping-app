package com.quan.xap.adapters;

import android.app.Activity;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.quan.xap.R;

import java.util.ArrayList;
import java.util.List;

//import com.sinch.android.rtc.messaging.WritableMessage;

public class ChattingAdapter extends BaseAdapter {

    public static final int DIRECTION_INCOMING = 0;
    public static final int DIRECTION_OUTGOING = 1;

    private List<Pair<String, Integer>> messages;
    private LayoutInflater layoutInflater;


    public ChattingAdapter(Activity activity) {
        layoutInflater = activity.getLayoutInflater();
        messages = new ArrayList<Pair<String, Integer>>();
    }

    public void addMessage(String message, int direction) {
        messages.add(new Pair(message, direction));
        notifyDataSetChanged();
    }

    public void deleteMessage(int i) {
        if(messages.size()>1) messages.remove(i);
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int i) {
        return messages.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int i) {
        return messages.get(i).second;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        int direction = getItemViewType(i);
        //show message on left or right, depending on if
        //it's incoming or outgoing
        if (convertView == null) {
            int res = 0;
            if (direction == DIRECTION_INCOMING) {
                res = R.layout.message_right;
            } else if (direction == DIRECTION_OUTGOING) {
                res = R.layout.message_left;
            }
            convertView = layoutInflater.inflate(res, viewGroup, false);
        }

        final String message = messages.get(i).first;

        final TextView messageBodyField=(TextView) convertView.findViewById(R.id.txtMessage);
        messageBodyField.setText(message);


        return convertView;
    }
}

