package com.quan.xap.api;

import android.util.Log;

import com.quan.xap.models.ResultEmailConform;
import com.squareup.okhttp.OkHttpClient;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;

public class ApiClient_01 {

    public static String API_ROOT = "http://82.223.19.247/webservice/include";

    private static ApiInterface apiService;

    public static ApiInterface getApiClient() {
        if (apiService == null) {
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setConnectTimeout(300, TimeUnit.SECONDS);
            okHttpClient.setReadTimeout(300, TimeUnit.SECONDS);

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(API_ROOT)
                    .setClient(new OkClient(okHttpClient))
                    .setLogLevel(RestAdapter.LogLevel.FULL).setLog(new RestAdapter.Log() {
                        @Override
                        public void log(String message) {
                            Log.w("MAP", message);
                        }
                    })
                    .build();
            apiService = restAdapter.create(ApiInterface.class);
        }
        return apiService;
    }

    public interface ApiInterface {
        //----------email conform
        @Multipart
        @POST("/test.php")
        public void wang_email(@PartMap Map<String, Object> options, Callback<ResultEmailConform> cb);


    }
}


