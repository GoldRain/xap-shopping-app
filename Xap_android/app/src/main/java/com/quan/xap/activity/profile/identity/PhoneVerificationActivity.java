package com.quan.xap.activity.profile.identity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.quan.xap.R;

public class PhoneVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editPhone_01;
    EditText editPhone_02;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);

        initialUI();
    }

    private void initialUI() {
        findViewById(R.id.imageBack).setOnClickListener(this);
        findViewById(R.id.txtButton).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtTitle)).setText("Identity Verification");

        editPhone_01=(EditText) findViewById(R.id.editPhone_01);
        editPhone_02=(EditText) findViewById(R.id.editPhone_02);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageBack:
                finish();
                break;
            case R.id.txtButton:
                if(editPhone_01.getText().toString().isEmpty() || editPhone_02.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"The phone number is empty.", Toast.LENGTH_SHORT).show();
                }else{

                }
                break;
        }
    }
}
