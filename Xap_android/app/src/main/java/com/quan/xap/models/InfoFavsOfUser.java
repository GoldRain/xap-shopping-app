package com.quan.xap.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoquan on 4/3/2017.
 */

public class InfoFavsOfUser implements Serializable{
    private static String FavsOfUser = "FavsOfUser";

    @SerializedName("id")
    private String id = "";
    @SerializedName("user_id")
    private String user_id = "";
    @SerializedName("item_id")
    private String item_id = "";
    @SerializedName("timestamps")
    private String timestamps = "";
    @SerializedName("item")
    private List<InfoItem> item = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(String timestamps) {
        this.timestamps = timestamps;
    }

    public List<InfoItem> getItem() {
        return item;
    }

    public void setItem(List<InfoItem> item) {
        this.item = item;
    }
}
