package com.quan.xap.api;

import android.util.Log;

import com.quan.xap.models.ResultGetBulletinList;
import com.quan.xap.models.ResultGetChat;
import com.quan.xap.models.ResultGetChatList;
import com.quan.xap.models.ResultGetEmailBody;
import com.quan.xap.models.ResultGetFavsOfItem;
import com.quan.xap.models.ResultGetFavsOfUser;
import com.quan.xap.models.ResultGetItemInfo;
import com.quan.xap.models.ResultGetItems;
import com.quan.xap.models.ResultProfileNotification;
import com.quan.xap.models.ResultUpdateUser;
import com.squareup.okhttp.OkHttpClient;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;

public class ApiClient {

    public static String API_ROOT = "http://82.223.19.247/xap/api";
//    public static String API_ROOT = "http://192.168.0.198:80/xap/api";

    private static ApiInterface apiService;

    public static ApiInterface getApiClient() { //boolean is3g
        if (apiService == null) {
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setConnectTimeout(50000, TimeUnit.SECONDS);
            okHttpClient.setReadTimeout(50000, TimeUnit.SECONDS);

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(API_ROOT)
                    .setClient(new OkClient(okHttpClient))
                    .setLogLevel(RestAdapter.LogLevel.FULL).setLog(new RestAdapter.Log() {
                        @Override
                        public void log(String message) {
                            Log.w("MAP", message);
                        }
                    })
                    .build();
            apiService = restAdapter.create(ApiInterface.class);
        }
        return apiService;
    }

    public interface ApiInterface {
        //----------login
        @Multipart
        @POST("/user_login.php")
        public void user_login(@PartMap Map<String, Object> options, Callback<ResultUpdateUser> cb);

        //----------login facebook
        @Multipart
        @POST("/signin_facebook.php")
        public void signin_facebook(@PartMap Map<String, Object> options, Callback<ResultUpdateUser> cb);

        //----------login google
        @Multipart
        @POST("/signin_google.php")
        public void signin_google(@PartMap Map<String, Object> options, Callback<ResultUpdateUser> cb);

        //---------signup-------------
        @Multipart
        @POST("/user_signup.php")
        public void user_signup(@PartMap Map<String, Object> options, Callback<ResultUpdateUser> cb);

        //---------update -------------
        @Multipart
        @POST("/update_user.php")
        public void update_user(@PartMap Map<String, Object> options, Callback<ResultUpdateUser> cb);

        //---------forget password -------------
        @Multipart
        @POST("/forget_password.php")
        public void forget_password(@PartMap Map<String, Object> options, Callback<ResultGetEmailBody> cb);

/////////////////////////////////////////////////////////////////////////////////////////////////

        //---------get items -------------
        @Multipart
        @POST("/get_items.php")
        public void get_items(@PartMap Map<String, Object> options, Callback<ResultGetItems> cb);

        //---------get user items -------------
        @Multipart
        @POST("/get_user_items.php")
        public void get_user_items(@PartMap Map<String, Object> options, Callback<ResultGetItems> cb);

        //---------get item info-------------
        @Multipart
        @POST("/get_item_info.php")
        public void get_item_info(@PartMap Map<String, Object> options, Callback<ResultGetItemInfo> cb);

        //---------get user info -------------
        @Multipart
        @POST("/get_user_info.php")
        public void get_user_info(@PartMap Map<String, Object> options, Callback<ResultUpdateUser> cb);

        //---------get favs of user -------------
        @Multipart
        @POST("/get_favs_of_user.php")
        public void get_favs_of_user(@PartMap Map<String, Object> options, Callback<ResultGetFavsOfUser> cb);

        //---------get favs of item -------------
        @Multipart
        @POST("/get_favs_of_item.php")
        public void get_favs_of_item(@PartMap Map<String, Object> options, Callback<ResultGetFavsOfItem> cb);

        //---------get profile settings notification -------------
        @Multipart
        @POST("/get_profile_settings.php")
        public void get_profile_settings(@PartMap Map<String, Object> options, Callback<ResultProfileNotification> cb);


/////////////////////////////////////////////////////////////////////////////////////////////////

        //---------add item -------------
        @Multipart
        @POST("/add_item.php")
        public void add_item(@PartMap Map<String, Object> options, Callback<ResultGetItemInfo> cb);

        //---------update item -------------
        @Multipart
        @POST("/update_item.php")
        public void update_item(@PartMap Map<String, Object> options, Callback<ResultGetItemInfo> cb);

        //---------set seen item -------------
        @Multipart
        @POST("/set_seen_item.php")
        public void set_seen_item(@PartMap Map<String, Object> options, Callback<ResultGetItemInfo> cb);

        //---------set fav -------------
        @Multipart
        @POST("/set_fav.php")
        public void set_fav(@PartMap Map<String, Object> options, Callback<ResultGetItemInfo> cb);

        //---------set profile settings notification-------------
        @Multipart
        @POST("/set_profile_settings.php")
        public void set_profile_settings(@PartMap Map<String, Object> options, Callback<ResultProfileNotification> cb);

        //---------reserve item -------------
        @Multipart
        @POST("/reserve_item.php")
        public void reserve_item(@PartMap Map<String, Object> options, Callback<ResultGetItemInfo> cb);

        //---------sell item -------------
        @Multipart
        @POST("/sell_item.php")
        public void sell_item(@PartMap Map<String, Object> options, Callback<ResultGetItemInfo> cb);

        //---------delete item -------------
        @Multipart
        @POST("/delete_item.php")
        public void delete_item(@PartMap Map<String, Object> options, Callback<ResultGetItemInfo> cb);

        //---------add report -------------
        @Multipart
        @POST("/add_report.php")
        public void add_report(@PartMap Map<String, Object> options, Callback<ResultGetItemInfo> cb);

/////////////////////////////////////////////////////////////////////////////////////////////////

        //---------add chat -------------
        @Multipart
        @POST("/add_chat.php")
        public void add_chat(@PartMap Map<String, Object> options, Callback<ResultGetChat> cb);

        //---------get chat -------------
        @Multipart
        @POST("/get_chat.php")
        public void get_chat(@PartMap Map<String, Object> options, Callback<ResultGetChatList> cb);

        //---------delete chat -------------
        @Multipart
        @POST("/delete_chat.php")
        public void delete_chat(@PartMap Map<String, Object> options, Callback<ResultGetChat> cb);

/////////////////////////////////////////////////////////////////////////////////////////////////////

        //---------verify email -------------
        @Multipart
        @POST("/verify_email.php")
        public void verify_email(@PartMap Map<String, Object> options, Callback<ResultGetEmailBody> cb);

        //---------verify facebook -------------
        @Multipart
        @POST("/verify_facebook.php")
        public void verify_facebook(@PartMap Map<String, Object> options, Callback<ResultGetChat> cb);

        //---------verify google -------------
        @Multipart
        @POST("/verify_google.php")
        public void verify_google(@PartMap Map<String, Object> options, Callback<ResultGetChat> cb);


////////////////////////////////////////////////////////////////////////////////////////////////////

        //---------delete chat -------------
        @Multipart
        @POST("/set_token.php")
        public void set_token(@PartMap Map<String, Object> options, Callback<ResultGetChat> cb);

        //---------get bulletin -------------
        @Multipart
        @POST("/get_bulletin.php")
        public void get_bulletin(@PartMap Map<String, Object> options, Callback<ResultGetBulletinList> cb);

    }
}


