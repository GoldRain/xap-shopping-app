package com.quan.xap.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by guoquan on 4/3/2017.
 */

public class InfoEmailBody implements Serializable{
    private static String EmailBody = "EmailBody";

    @SerializedName("body")
    private String body = "";
    @SerializedName("to_email")
    private String to_email = "";

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTo_email() {
        return to_email;
    }

    public void setTo_email(String to_email) {
        this.to_email = to_email;
    }
}
