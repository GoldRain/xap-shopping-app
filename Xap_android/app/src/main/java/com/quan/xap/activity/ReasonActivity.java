package com.quan.xap.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.quan.xap.R;
import com.quan.xap.api.ApiClient;
import com.quan.xap.models.ResultGetItemInfo;
import com.quan.xap.utility.Common;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ReasonActivity extends AppCompatActivity implements View.OnClickListener {

    ListView listView;
    private String reported_ID = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reason);

        reported_ID = getIntent().getExtras().getString(Common.ITEM_ID);

        initialUI();
    }

    private void initialUI(){
        findViewById(R.id.imageBack).setOnClickListener(this);


        listView = (ListView) findViewById(R.id.listView);
        ArrayAdapter<String> mHistory = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Common.Reasons);
        listView.setAdapter(mHistory);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                addReport(i);
                finish();
            }
        });

    }

    private void addReport(int iType){
        if(Common.userProfile!=null && Common.userProfile.getId()!=null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("reporter", Common.userProfile.getId());
            params.put("reported_id", reported_ID);
            params.put("type", String.valueOf(iType));
            params.put("reason", Common.Reasons[iType]);
            ApiClient.getApiClient().add_report(params, new Callback<ResultGetItemInfo>() {
                @Override
                public void success(ResultGetItemInfo resultGetItems, Response response) {
                    if(resultGetItems.getStat().equals(Common.success)) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.add_report_success), Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.add_report_failed), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageBack:

                finish();
                break;

        }
    }
}
