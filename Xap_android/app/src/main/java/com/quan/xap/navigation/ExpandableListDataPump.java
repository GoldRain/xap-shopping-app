package com.quan.xap.navigation;


import com.quan.xap.utility.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ExpandableListDataPump {
    public static HashMap<String, List<String>> getData() {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<String, List<String>>();

        List<String> menu1 = new ArrayList<String>();
        List<String> menu2 = new ArrayList<String>();
        List<String> menu3 = new ArrayList<String>();
        List<String> menu5 = new ArrayList<String>();
        List<String> menu6 = new ArrayList<String>();
        List<String> menu7 = new ArrayList<String>();

        List<String> list4 = new ArrayList<String>();
        for (int i=0; i<Common.Category_String.length; i++) {
            list4.add(Common.Category_String[i]);
        }
//        list4.add("Electronics");
//        list4.add("Sports & Leisure");
//        list4.add("Home & Garden");
//        list4.add("Games & Consoles");
//        list4.add("Books, Movies & Music");
//        list4.add("Fashion & Accessories");
//        list4.add("Baby & Child");
//        list4.add("Real Estate");
//        list4.add("Appliances");
//        list4.add("Services");
//        list4.add("Other");


        expandableListDetail.put(Common.Menu_string[0], menu1);
        expandableListDetail.put(Common.Menu_string[1], menu2);
//        expandableListDetail.put("Collections", menu3);
        expandableListDetail.put(Common.Menu_string[2], list4);
//        expandableListDetail.put("New in my area", menu5);
        expandableListDetail.put(Common.Menu_string[3], menu6);
        expandableListDetail.put(Common.Menu_string[4], menu7);

        return expandableListDetail;
    }
}
