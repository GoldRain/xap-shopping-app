package com.quan.xap.activity.profile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.quan.xap.R;
import com.quan.xap.adapters.FavoriteAdapter;
import com.quan.xap.utility.Common;

import java.util.ArrayList;
import java.util.List;

public class FavoriteCategoriesActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txtTitle;
    ImageView imgLogOut;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_categories);

        initialUI();
    }

    private void initialUI() {
        findViewById(R.id.imageBack).setOnClickListener(this);

        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText(getResources().getString(R.string.favorite_categories));
        imgLogOut = (ImageView) findViewById(R.id.imgLogOut);
        imgLogOut.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_white_36dp));
        imgLogOut.setVisibility(View.VISIBLE);
        imgLogOut.setOnClickListener(this);


        if(Common.userNotification!=null && Common.userNotification.getFav_categories()!=null && !Common.userNotification.getFav_categories().isEmpty()){
            String[] strFav = Common.userNotification.getFav_categories().split(",");
            if(strFav.length == Common.favorite_categories.length){
                Common.favorite_categories = strFav;
            }
        }

        List<String> categories = new ArrayList<>();
        for(String item : Common.Category_String){
            categories.add(item);
        }
        listView = (ListView) findViewById(R.id.listView);
        FavoriteAdapter adapter = new FavoriteAdapter(this, categories);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageBack:
                finish();
                break;
            case R.id.imgLogOut:
                setNotificationFavCategories();
                finish();
                break;
        }
    }

    private void setNotificationFavCategories() {
        String strtemp = "";
        for(int i=0;i<Common.favorite_categories.length;i++){
            strtemp = strtemp + Common.favorite_categories[i];
            if(i!=Common.favorite_categories.length-1){
                strtemp = strtemp + ",";
            }
        }
        Common.userNotification.setFav_categories(strtemp);
    }


}
