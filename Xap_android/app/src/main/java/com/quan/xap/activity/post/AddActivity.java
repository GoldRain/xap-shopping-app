package com.quan.xap.activity.post;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.quan.xap.R;
import com.quan.xap.activity.login_signup.LoginActivity;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class AddActivity extends AppCompatActivity implements View.OnClickListener {

    SessionManager session;

    EditText editWrite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        initialUI();
    }

    private void initialUI() {
        editWrite = (EditText) findViewById(R.id.editWrite);
        editWrite.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        findViewById(R.id.imageBack).setOnClickListener(this);
        findViewById(R.id.imageCheck).setOnClickListener(this);

        addCategories();
    }

    private void addCategories() {
        final LinearLayout layout_categories = (LinearLayout) findViewById(R.id.layout_categories);

        if (Common.Categories_AddItem != null) {
            List<View> views = new ArrayList<>();  ////////////////////////template
            for (int i = 0; i < Common.Categories_AddItem.length; i++) {
                final View convertView = getLayoutInflater().inflate(R.layout.layout_category_item,
                        (ViewGroup) findViewById(android.R.id.content), false);
                final TextView txtcategory = (TextView) convertView.findViewById(R.id.txtcategory);
                txtcategory.setText(Common.Categories_AddItem[i]);
                txtcategory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editWrite.setText(txtcategory.getText());
                    }
                });
                views.add(convertView);   ///////////template
            }
            addViewToLinearLayout(layout_categories, views, getBaseContext(), true);
        }
    }


    private void addViewToLinearLayout(final LinearLayout ll, final List<View> views, Context mContext, boolean isCategory) {
        Display display = getWindowManager().getDefaultDisplay();
        ll.removeAllViews();
//        ll.setGravity(Gravity.CENTER_HORIZONTAL);
        int maxWidth = display.getWidth() - 250;
        LinearLayout.LayoutParams params;
        LinearLayout newLL = new LinearLayout(mContext);
        newLL.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//        newLL.setGravity(Gravity.LEFT);
        newLL.setGravity(Gravity.CENTER_HORIZONTAL);
        newLL.setOrientation(LinearLayout.HORIZONTAL);
        int widthSoFar = 0;
        for (int i = 0; i < views.size(); i++) {
            final LinearLayout LL = new LinearLayout(mContext);
            LL.setOrientation(LinearLayout.HORIZONTAL);
//            LL.setGravity(Gravity.LEFT); //Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM
            LL.setGravity(Gravity.CENTER_HORIZONTAL); //Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM
            LL.setLayoutParams(new ListView.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            views.get(i).measure(0, 0);
            params = new LinearLayout.LayoutParams(views.get(i).getMeasuredWidth(), LinearLayout.LayoutParams.WRAP_CONTENT);
            //params.setMargins(5, 0, 5, 0);  // YOU CAN USE THIS
            ((ViewGroup) views.get(i)).removeView(views.get(i));
            LL.addView(views.get(i)); // , params)
            LL.measure(0, 0);
///////////////////remove layout when click imageDelete

/////////////////////////////////////////////
            widthSoFar += views.get(i).getMeasuredWidth();// YOU MAY NEED TO ADD THE MARGINS
            if (widthSoFar >= maxWidth) {
                ll.addView(newLL);
                newLL = new LinearLayout(mContext);
                newLL.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.FILL_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                newLL.setOrientation(LinearLayout.HORIZONTAL);
//                newLL.setGravity(Gravity.LEFT);
                newLL.setGravity(Gravity.CENTER_HORIZONTAL);
                params = new LinearLayout.LayoutParams(LL.getMeasuredWidth(), LL.getMeasuredHeight());
                newLL.addView(LL); //, params);
                widthSoFar = LL.getMeasuredWidth();
            } else {
                newLL.addView(LL);
            }
        }
        ll.addView(newLL);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageBack:
                finish();
                break;
            case R.id.imageCheck:
                if(!editWrite.getText().toString().isEmpty()) {
                    Intent intent = new Intent(AddActivity.this, ItemActivity.class);
                    intent.putExtra("Title", editWrite.getText().toString());
                    startActivity(intent);
                    finish();
                }
                break;

        }
    }


}
