package com.quan.xap.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quan.xap.R;
import com.quan.xap.activity.chat.ChatActivity;
import com.quan.xap.api.ApiClient;
import com.quan.xap.interfaces.ConversasionDeleteListener;
import com.quan.xap.interfaces.OpenDrawerEventListener;
import com.quan.xap.models.ClassChatList;
import com.quan.xap.models.ResultGetChat;
import com.quan.xap.utility.Common;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

//import com.sinch.android.rtc.messaging.WritableMessage;

public class ConversationAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private Activity getActivity;
    List<ClassChatList> categories = new ArrayList<>();

    public ConversationAdapter(Activity activity, List<ClassChatList> data) {
        getActivity = activity;
        layoutInflater = activity.getLayoutInflater();
        categories = data;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        ViewHolder holder = null;
        if (convertView == null) {
            int res = R.layout.adapter_conversation;
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(res, viewGroup, false);
            holder.rl_conversation = (RelativeLayout) convertView.findViewById(R.id.rl_conversation);
            holder.imgPhoto = (ImageView) convertView.findViewById(R.id.imgPhoto);
            holder.imgDelete = (ImageView) convertView.findViewById(R.id.imgDelete);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
            holder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            holder.txtContent = (TextView) convertView.findViewById(R.id.txtContent);
            holder.txtDate = (TextView) convertView.findViewById(R.id.txtDate);
            holder.txtBadge = (TextView) convertView.findViewById(R.id.txtBadge);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }

        final ClassChatList item = categories.get(i);

        if(item.getItem()!=null) {
            if (item.getItem().getPic1() != null) {
                if (!item.getItem().getPic1().isEmpty()) {
//                ImageLoader imgLoader = new ImageLoader(getActivity);
//                int loader = R.drawable.loader;
//                imgLoader.DisplayImage(Common.common_url + item.getItem().getPic1(), loader, holder.imgPhoto);
                    Picasso.with(getActivity).load(Common.common_url + item.getItem().getPic1()).into(holder.imgPhoto);
                }
            } else if (item.getItem().getPic2() != null && !item.getItem().getPic2().isEmpty()) {
                Picasso.with(getActivity).load(Common.common_url + item.getItem().getPic2()).into(holder.imgPhoto);
            } else if (item.getItem().getPic3() != null && !item.getItem().getPic3().isEmpty()) {
                Picasso.with(getActivity).load(Common.common_url + item.getItem().getPic3()).into(holder.imgPhoto);
            } else if (item.getItem().getPic4() != null && !item.getItem().getPic4().isEmpty()) {
                Picasso.with(getActivity).load(Common.common_url + item.getItem().getPic4()).into(holder.imgPhoto);
            }

            if(item.getItem().getTitle()!=null){
                holder.txtTitle.setText(item.getItem().getTitle());
            }
            if(item.getItem().getUser()!=null) {
                holder.txtName.setText(item.getItem().getUser().getUsername());
            }
            if(item.getItem().getDescription()!=null) {
                holder.txtContent.setText(item.getItem().getDescription());
            }
            if (item.getItem().getUpdate_time()!=null && !item.getItem().getUpdate_time().isEmpty()) {
                holder.txtDate.setText(item.getItem().getUpdate_time().substring(0, 10));
            }
        }

        holder.rl_conversation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(item.getChatList().size()!=0 && Common.dataChatListValuesList != null && Common.dataChatListValuesList.size()!=0) {
                    if(i < Common.dataChatListValuesList.size()) {
                        Common.dataChatListValuesList.get(i).last_chatting_number
                                = item.getChatList().get(item.getChatList().size() - 1).getId();
                    }
                }

                Intent intent = new Intent(getActivity, ChatActivity.class);
                intent.putExtra(Common.ITEM_ID, item.getItem_id());
//                if(item.getItem().getUser()!=null && Common.userProfile!=null) {
//                    if (item.getItem().getUser().getId().equals(Common.userProfile.getId())) {
//                        intent.putExtra("to_user_id", item.getMy_user_id());
//                    } else {
                        intent.putExtra("to_user_id", item.getTo_user_id());

//                    }
//                }
                getActivity.startActivity(intent);
            }
        });

        final ViewHolder finalHolder = holder;

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity);
                alertDialog.setMessage(getActivity.getResources().getString(R.string.really_delete));
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        delete_chat(createDeleteIds(item), i, finalHolder);
                        dialog.cancel();
//                        finalConvertView.setVisibility(View.GONE);
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();
                    }
                });

                alertDialog.show();
            }
        });

        if(item.getUnread_number()>0){
            holder.txtBadge.setVisibility(View.VISIBLE);
            holder.txtBadge.setText(String.valueOf(item.getUnread_number()));
//            Common.dataChatListValuesList.get(i).last_chatting_number
//                    = categories.get(i).getChatList().get(categories.get(i).getChatList().size()-1).getId();
        }else {
            holder.txtBadge.setVisibility(View.INVISIBLE);
        }

        Log.d("Conversation:", "adapter item : " + String.valueOf(i));

        return convertView;

    }



    private void delete_chat(String ids, final int index, final ViewHolder finalHolder){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("ids", ids);
        ApiClient.getApiClient().delete_chat(params, new Callback<ResultGetChat>(){
            @Override
            public void success(ResultGetChat resultGetItems, Response response) {
                if(resultGetItems.getStat().equals(Common.success)){
                    eventListener.DeleteConversasion(index);
                    notifyDataSetChanged();
//                    finalHolder.rl_conversation.setVisibility(View.GONE);
                }else{
                    Toast.makeText(getActivity.getApplicationContext(), getActivity.getResources().getString(R.string.delete_chat_failed) , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity.getApplicationContext(), getActivity.getResources().getString(R.string.delete_chat_failed) , Toast.LENGTH_LONG).show();
            }
        });
    }

    private String createDeleteIds(ClassChatList item){
        String strIds = "";
        for(int i=0; i<item.getChatList().size(); i++){
            strIds = strIds + "," + item.getChatList().get(i).getId();
        }
        return strIds;
    }

    public static class ViewHolder {
        RelativeLayout rl_conversation;
        public ImageView imgPhoto;
        public ImageView imgDelete;
        public TextView txtTitle;
        public TextView txtName;
        public TextView txtContent;
        public TextView txtDate;
        public TextView txtBadge;
    }

    private ConversasionDeleteListener eventListener;
    public void setEventListener(ConversasionDeleteListener listener){
        this.eventListener=listener;
    }
}

