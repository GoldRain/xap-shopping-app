package com.quan.xap.activity.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.quan.xap.R;
import com.quan.xap.activity.post.AddActivity;
import com.quan.xap.adapters.TabsFragmentPagerAdapter;
import com.quan.xap.api.ApiClient;
import com.quan.xap.models.InfoUserProfile;
import com.quan.xap.models.ResultGetItems;
import com.quan.xap.models.ResultProfileNotification;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.MyDatabaseHelper;
import com.quan.xap.utility.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ProfileActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks
        , GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, OnMapReadyCallback {

    private static final float enable_alpha = 1.0f;
    private static final float unenable_alpha = 0.3f;

    boolean hasStop;

    ImageView imgLogOut;
    ImageView imgSettings;
    ImageView imgShare;
    TextView txtLogOut;

    SessionManager sessionManager;

    private GoogleMap mMap;
    private SupportMapFragment supportMapFragment;
    private Marker mMarker;

    CircleImageView imgProfilePhoto;
    TextView txtUserName;


    ImageView txtArrowView;
    boolean isExpanded = false;
    RelativeLayout rl_verify_show;
    TextView txtVerifyStatus;
    ImageView imgVerifyEmail;
    ImageView imgVerifyPhone;
    ImageView imgVerifyFacebook;
    ImageView imgVerifyGoogle;
    ImageView imgVerifyBirthday;
    ImageView imgVerifySex;
    ImageView imgVerifyLocal;
    ImageView imgVerifyPerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setInitial();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hasStop = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        hasStop = false;
        sessionManager = new SessionManager(this);
        getUserItems();
    }


    private void setInitial(){
        initialUI();

    }


    private void initialUI(){
        ((TextView) findViewById(R.id.txtTitle)).setText(getResources().getString(R.string.my_profile));
        imgLogOut = (ImageView) findViewById(R.id.imgLogOut);
        imgSettings = (ImageView) findViewById(R.id.imgSettings);
        imgShare = (ImageView) findViewById(R.id.imgShare);
        imgLogOut.setVisibility(View.VISIBLE);
        imgSettings.setVisibility(View.VISIBLE);
        imgShare.setVisibility(View.VISIBLE);

        txtLogOut = (TextView) findViewById(R.id.txtLogOut);
        txtLogOut.setVisibility(View.INVISIBLE);

        findViewById(R.id.rl_profile).setOnClickListener(this);
        findViewById(R.id.imageBack).setOnClickListener(this);
        findViewById(R.id.imgLogOut).setOnClickListener(this);
        findViewById(R.id.imgSettings).setOnClickListener(this);
        findViewById(R.id.imgShare).setOnClickListener(this);
        findViewById(R.id.txtLogOut).setOnClickListener(this);
        findViewById(R.id.rl_map_clickable).setOnClickListener(this);
//        findViewById(R.id.imgShare).setOnClickListener(this);

        findViewById(R.id.fab).setOnClickListener(this);

        imgProfilePhoto = (CircleImageView) findViewById(R.id.imgProfilePhoto);
        imgProfilePhoto.setOnClickListener(this);
        if(Common.userProfile!=null && Common.userProfile.getProfile_image()!=null) {
            if (!Common.userProfile.getProfile_image().isEmpty()) {
                Picasso.with(this).load(Common.common_url + Common.userProfile.getProfile_image()).into(imgProfilePhoto);
            }
        }

        txtUserName = (TextView) findViewById(R.id.txtUserName);
        if(Common.userProfile!=null) txtUserName.setText(Common.userProfile.getUsername());

        supportMapFragment=((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        if(!hasStop) supportMapFragment.getMapAsync(this);

        set_verify_show();
        createTabLayout();
    }

    private void getProfileNotification() {
        Map<String, Object> params = new HashMap<String, Object>();
        if(Common.userProfile!=null) {
            params.put("user_id", Common.userProfile.getId());
            ApiClient.getApiClient().get_profile_settings(params, new Callback<ResultProfileNotification>() {
                @Override
                public void success(ResultProfileNotification resultUpdateUser, retrofit.client.Response response) {
                    if(resultUpdateUser.getStat().equals(Common.success)) {
                        Common.userNotification = resultUpdateUser.getData();
                    }else{
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.get_profile_notification_failed), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }





    private void getUserItems(){
        if(Common.userItems!=null) {
            //if (Common.userItems.isEmpty()) {
                if (Common.userProfile != null && Common.userProfile.getId() != null) {
                    Map<String, Object> params = new HashMap<String, Object>();
                    if (Common.userProfile != null) {
                        params.put("user_id", Common.userProfile.getId());
                        final ProgressDialog progressDialog = new ProgressDialog(this);
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.show();
                        ApiClient.getApiClient().get_user_items(params, new Callback<ResultGetItems>() {
                            @Override
                            public void success(ResultGetItems resultGetItems, Response response) {
                                try{
                                    if(progressDialog != null && progressDialog.isShowing())
                                        progressDialog.dismiss();
                                }catch (Exception e){};

                                if (resultGetItems.getStat().equals(Common.success)) {
                                    if(Common.userItems != null){
                                        Common.userItems.clear();
                                    }
                                    Common.userItems = resultGetItems.getUserData();
                                    createTabLayout();
                                } else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.get_item_info_failed), Toast.LENGTH_LONG).show();
                                }
                                //getProfileNotification();
                            }
                            @Override
                            public void failure(RetrofitError error) {
                                if(progressDialog != null &&  progressDialog.isShowing())
                                    progressDialog.dismiss();
                                //Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
                                createTabLayout();
                            }
                        });
                    }
                }
//            } else {
//                createTabLayout();
//            }
        }
    }

    private void createTabLayout(){
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){
            @Override
            public void onTabSelected(TabLayout.Tab tab){
                int position = tab.getPosition();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int position = tab.getPosition();

            }

            
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int position = tab.getPosition();
            }
        });
        Common.pager = (ViewPager) findViewById(R.id.pager);
        TabsFragmentPagerAdapter adapter = new TabsFragmentPagerAdapter(getSupportFragmentManager(), this);
        if(!hasStop && Common.pager!=null && adapter!=null) {
            Common.pager.setAdapter(adapter);
//        setPagerHeight(Common.userItems.size());
            adapter.notifyDataSetChanged();
            tabLayout.setupWithViewPager(Common.pager);
        }

    }

    private void setPagerHeight(int items_count){
        if(Common.pager!=null) {
            ViewGroup.LayoutParams layoutParams = Common.pager.getLayoutParams();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            int page_height = (int) (items_count / 2 * 1.5 * width);
            layoutParams.height = page_height; //this is in dp
            Common.pager.setLayoutParams(layoutParams);
        }
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private void set_verify_show(){
        txtArrowView = (ImageView) findViewById(R.id.txtArrowView);
        txtArrowView.setOnClickListener(this);

        rl_verify_show = (RelativeLayout) findViewById(R.id.rl_verify_show);
        rl_verify_show.setVisibility(View.GONE);

        txtVerifyStatus = (TextView) findViewById(R.id.txtVerifyStatus);
        imgVerifyEmail = (ImageView) findViewById(R.id.imgVerifyEmail);
        imgVerifyPhone = (ImageView) findViewById(R.id.imgVerifyPhone);
        imgVerifyFacebook = (ImageView) findViewById(R.id.imgVerifyFacebook);
        imgVerifyGoogle = (ImageView) findViewById(R.id.imgVerifyGoogle);
        imgVerifyBirthday = (ImageView) findViewById(R.id.imgVerifyBirthday);
        imgVerifySex = (ImageView) findViewById(R.id.imgVerifySex);
        imgVerifyLocal = (ImageView) findViewById(R.id.imgVerifyLocal);
        imgVerifyPerson = (ImageView) findViewById(R.id.imgVerifyPerson);

        if(Common.userProfile!=null) {
            if (Common.userProfile.getVerif_email() != null && !Common.userProfile.getVerif_email().isEmpty()) {
                imgVerifyEmail.setAlpha(enable_alpha);
            } else {
                imgVerifyEmail.setAlpha(unenable_alpha);
            }
            if (Common.userProfile.getVerif_phone() != null && !Common.userProfile.getVerif_phone().isEmpty()) {
                imgVerifyPhone.setAlpha(enable_alpha);
            } else {
                imgVerifyPhone.setAlpha(unenable_alpha);
            }
            if (Common.userProfile.getFacebook() != null && !Common.userProfile.getFacebook().isEmpty()) {
                imgVerifyFacebook.setAlpha(enable_alpha);
            } else {
                imgVerifyFacebook.setAlpha(unenable_alpha);
            }
            if (Common.userProfile.getGoogle() != null && !Common.userProfile.getGoogle().isEmpty()) {
                imgVerifyGoogle.setAlpha(enable_alpha);
            } else {
                imgVerifyGoogle.setAlpha(unenable_alpha);
            }
            if (Common.userProfile.getBirthday() != null && !Common.userProfile.getBirthday().isEmpty()) {
                imgVerifyBirthday.setAlpha(enable_alpha);
            } else {
                imgVerifyBirthday.setAlpha(unenable_alpha);
            }
            if (Common.userProfile.getGender() != null && !Common.userProfile.getGender().isEmpty()) {
                imgVerifySex.setAlpha(enable_alpha);
            } else {
                imgVerifySex.setAlpha(unenable_alpha);
            }
            if (Common.userProfile.getLatitude() != null && Common.userProfile.getLongitude() != null
                    && !Common.userProfile.getLatitude().isEmpty() && !Common.userProfile.getLongitude().isEmpty()) {
                imgVerifyLocal.setAlpha(enable_alpha);
            } else {
                imgVerifyLocal.setAlpha(unenable_alpha);
            }
            if (Common.userProfile.getProfile_image() != null && !Common.userProfile.getProfile_image().isEmpty()) {
                imgVerifyPerson.setAlpha(enable_alpha);
            } else {
                imgVerifyPerson.setAlpha(unenable_alpha);
            }
        }
    }

    private void setUserDetails(){
        if (!isExpanded) { // default false
            txtArrowView.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));
            rl_verify_show.setVisibility(View.GONE);
        }else {
            txtArrowView.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_less_black_24dp));
            rl_verify_show.setVisibility(View.VISIBLE);
        }

    }

    private void googleSignOut() {
        if(Common.mGoogleApiClient==null){
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            Common.mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }

        if(isGoogleConnected) {
            Auth.GoogleSignInApi.signOut(Common.mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_logOut),Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }

    private void facebookSignOut(){
        FacebookSdk.sdkInitialize(getApplicationContext());
        LoginManager.getInstance().logOut();
    }

    private void clearDataChatListValuesList(){
        Common.dataChatListValuesList.clear();
        MyDatabaseHelper mDatabaseHelper = new MyDatabaseHelper(this);
        mDatabaseHelper.setValues();
    }

    @Override
    public void onClick(View view) {
        Intent intent;
//        txtLogOut.setVisibility(View.INVISIBLE);
        switch (view.getId()) {
            case R.id.imageBack:
                finish();
                break;
            case R.id.txtLogOut:
                sessionManager.setLogin(false);
                sessionManager.setUserID("");
                sessionManager.setEmail("");
                sessionManager.setUsername("");
                clearDataChatListValuesList();
                Common.userProfile = new InfoUserProfile();
                if(Common.userItems!=null) Common.userItems.clear();
                Common.setHeaderText(sessionManager, ProfileActivity.this);
                facebookSignOut();
                googleSignOut();
                finish();
                break;
            case R.id.imgLogOut:
                if(txtLogOut.getVisibility()==View.VISIBLE){
                    txtLogOut.setVisibility(View.GONE);
                }else {
                    txtLogOut.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.imgSettings:
                intent = new Intent(ProfileActivity.this, EditProfileActivity.class);
                startActivity(intent);
                break;

            case R.id.rl_map_clickable:
                intent = new Intent(ProfileActivity.this, LocationActivity.class);
                if(Common.userProfile!=null){
                    intent.putExtra("latitude",Float.valueOf(Common.userProfile.getLatitude()));
                    intent.putExtra("longitude",Float.valueOf(Common.userProfile.getLongitude()));
                }
                startActivity(intent);
                break;
            case R.id.txtArrowView:
                isExpanded = !isExpanded;
                setUserDetails();
                break;
            case R.id.imgProfilePhoto:
                break;
            case R.id.imgShare:
                inviteOthers();
                break;
            case R.id.fab:
                intent = new Intent(ProfileActivity.this, AddActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void inviteOthers(){

        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            String strShareMessage = "\nLet me recommend you this application\n\n";
            strShareMessage = strShareMessage + "https://play.google.com/store/apps/details?id="
                    + getPackageName();
            Uri screenshotUri = Uri.parse("android.resource://packagename/drawable/image_name");
            i.setType("image/png");
            i.putExtra(Intent.EXTRA_STREAM, screenshotUri);
            i.putExtra(Intent.EXTRA_TEXT, strShareMessage);
            startActivity(Intent.createChooser(i, "Share via"));
        } catch(Exception e) {
            //e.toString();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng location = new LatLng(0, 0);
        if(Common.userProfile!=null)
        {
            if(Common.userProfile.getLatitude()!=null && Common.userProfile.getLongitude()!=null) {
                location = new LatLng(Double.valueOf(Common.userProfile.getLatitude())
                        , Double.valueOf(Common.userProfile.getLongitude()));
            }
        }
        mMarker = mMap.addMarker(new MarkerOptions().position(location));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 0));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Google connection failed", Toast.LENGTH_LONG).show();
    }

    boolean isGoogleConnected = false;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        isGoogleConnected = true;
    }

    @Override
    public void onConnectionSuspended(int i) {
        Common.mGoogleApiClient.connect();
    }
}
