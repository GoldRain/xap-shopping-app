package com.quan.xap.utility;

import java.util.ArrayList;

public class SampleData {

    public static final int SAMPLE_DATA_ITEM_COUNT = 30;

    public static ArrayList<String> generateSampleData(int iCount) {
        final ArrayList<String> data = new ArrayList<String>(iCount);

//        for (int i = 0; i < SAMPLE_DATA_ITEM_COUNT; i++) {
        for (int i = 0; i < iCount; i++) {
            data.add("SAMPLE #");
        }

        return data;
    }

}
