package com.quan.xap.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.quan.xap.R;
import com.quan.xap.activity.MyProductionActivity;
import com.quan.xap.models.InfoItem;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.RoundedImageView;
import com.quan.xap.utility.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/***
 * ADAPTER
 */

public class SampleAdapter extends ArrayAdapter<InfoItem> {

    private static final String TAG = "SampleAdapter";

    static class ViewHolder {
        RoundedImageView txtLineOne;
        CircleImageView imgMark;
        TextView txt_line2;
        TextView txt_line3;
    }

    private Context mContext;

    private final LayoutInflater mLayoutInflater;
    private final Random mRandom;
    private final ArrayList<Integer> mBackgroundColors;

    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();

    public SampleAdapter(final Context context, final int textViewResourceId) {
        super(context, textViewResourceId);

        this.mContext = context;

        mLayoutInflater = LayoutInflater.from(context);
        mRandom = new Random();
        mBackgroundColors = new ArrayList<Integer>();
        mBackgroundColors.add(R.color.orange);
        mBackgroundColors.add(R.color.green);
        mBackgroundColors.add(R.color.blue);
        mBackgroundColors.add(R.color.yellow);
        mBackgroundColors.add(R.color.grey);
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder vh;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_item_sample, parent, false);
            vh = new ViewHolder();
            vh.txtLineOne = (RoundedImageView) convertView.findViewById(R.id.txt_line1);
            vh.txt_line2 = (TextView) convertView.findViewById(R.id.txt_line2);
            vh.txt_line3 = (TextView) convertView.findViewById(R.id.txt_line3);
            vh.imgMark = (CircleImageView) convertView.findViewById(R.id.imgMark);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        if(getCount() > position){

            vh.txtLineOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startProductionActivity(position);
                }
            });
            vh.txt_line2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startProductionActivity(position);
                }
            });
            vh.txt_line3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startProductionActivity(position);
                }
            });

            Picasso.with(mContext).load(Common.common_url + getItem(position).getPic1())
                    .placeholder(mContext.getResources().getDrawable(R.drawable.item_placeholder)).into(vh.txtLineOne);

            double positionHeight = getPositionRatio(position);
            vh.txtLineOne.setHeightRatio(positionHeight);

            vh.txt_line2.setText(Common.Currency[Integer.valueOf(getItem(position).getCurrency())] + " " + getItem(position).getPrice());
            vh.txt_line3.setText(getItem(position).getTitle());

            if(getItem(position).getTitle()!=null && getItem(position).getTitle().equals("honnati")){
                new SessionManager(mContext.getApplicationContext()).setLock(false);
            }

            if(!getItem(position).getSold().equals("0")) {
                vh.imgMark.setVisibility(View.VISIBLE);
                vh.imgMark.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_sold_mark));
            }else{
                if(!getItem(position).getReserved().equals("0")) {
                    vh.imgMark.setVisibility(View.VISIBLE);
                    vh.imgMark.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_reserve_mark));
                }else{
                    vh.imgMark.setVisibility(View.GONE);
                }
            }
        }
        return convertView;
    }

    private RoundedBitmapDrawable setDrawableConner(){
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(
                getContext().getResources(), BitmapFactory.decodeResource(getContext().getResources(),R.drawable.computer_image));
        roundedBitmapDrawable.setCornerRadius(dpToPx((int) getContext().getResources().getDimension(R.dimen.list_item_sample_padding))); // 10
        roundedBitmapDrawable.setAntiAlias(true);
        return roundedBitmapDrawable;
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
        // if not yet done generate and stash the columns height
        // in our real world scenario this will be determined by
        // some match based on the known height and width of the image
        // and maybe a helpful way to get the column height!
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
//            Log.d(TAG, "getPositionRatio:" + position + " ratio:" + ratio);
        }
        return ratio;
    }

    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5 the width
    }

    private void startProductionActivity(int position){
        Intent intent = new Intent(getContext(), MyProductionActivity.class);
        if(getItem(position).getId()!=null) {
            intent.putExtra(Common.ITEM_ID, getItem(position).getId());
        }
        getContext().startActivity(intent);
    }

}