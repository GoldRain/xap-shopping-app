package com.quan.xap.constants;

import android.app.Activity;
import android.location.Location;
import android.location.LocationManager;

import java.util.List;

import static android.content.Context.LOCATION_SERVICE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Constants used in the application for app purpose
 *
 * @author Octa
 */
public class Constants {

    public static final int LOGIN_TO_TWITTER = 1;

    /**
     * Application namespace
     */
    private static final String NAMESPACE = "ro.octa.twittersample";

    /**
     * Extras constants
     */
    public static final String EXTRA_CALLBACK_URL_KEY = NAMESPACE + ".extra.callbackUrlKey";
    public static final String EXTRA_AUTH_URL_KEY = NAMESPACE + ".extra.authUrlKey";

    public interface ACTION {
        public static String MAIN_ACTION = "com.marothiatechs.customnotification.action.main";
        public static String STARTFOREGROUND_ACTION = "com.marothiatechs.customnotification.action.startforeground";
        public static String STOPFOREGROUND_ACTION = "com.marothiatechs.customnotification.action.stopforeground";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }


    public static Location getLastKnownLocation(Activity activity) {
        LocationManager locationManager = (LocationManager)activity.getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }



}
