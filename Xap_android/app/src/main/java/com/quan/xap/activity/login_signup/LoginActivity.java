package com.quan.xap.activity.login_signup;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.quan.xap.R;
import com.quan.xap.activity.profile.ProfileActivity;
import com.quan.xap.api.ApiClient;
import com.quan.xap.models.ResultUpdateUser;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener {

    ProgressBar progress_google;
    ProgressBar progress_facebook;

    TextView txtFacebook;
    TextView txtGoogle;

    boolean isProgressing = false;

    private CallbackManager callbackManager;

    private static final String TAG = "LoginInActivity";
    private static final int RC_SIGN_IN = 9001;

//    private GoogleApiClient mGoogleApiClient;
    SignInButton signInButton;

    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        printKeyHash(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        FacebookSdk.setIsDebugEnabled(true);

        setContentView(R.layout.activity_login);


        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final String accessToken = loginResult.getAccessToken()
                        .getToken();
                Log.i("accessToken", accessToken);

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject me,
                                                    GraphResponse response) {


                                Map<String, Object> params = new HashMap<String, Object>();

                                params.put("facebook", me.optString("id"));
                                params.put("user_name", me.optString("name"));
                                params.put("first_name", me.optString("first_name"));
                                params.put("last_name", me.optString("last_name"));
                                params.put("email", me.optString("email"));

                                ApiClient.getApiClient().signin_facebook(params, new Callback<ResultUpdateUser>() {
                                    @Override
                                    public void success(ResultUpdateUser resultUpdateUser, retrofit.client.Response response) {
                                        if(resultUpdateUser.getStat().equals(Common.success)) {
                                            Common.userProfile = resultUpdateUser.getUserData();
                                            if (Common.userProfile != null && !Common.userProfile.getId().isEmpty()) {
                                                session.setLogin(true);
                                                session.setUsername(Common.userProfile.getUsername());
                                                session.setUserID(Common.userProfile.getId());
                                                session.setEmail(Common.userProfile.getEmail());
                                                Common.dataChatListValuesList.clear();
                                                Common.setHeaderText(session, LoginActivity.this);
                                                Common.isVerified = new boolean[]{false, true, false};
                                                toProfileActivity();
                                            } else {
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_again), Toast.LENGTH_SHORT).show();
                                            }
                                        }else{
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.facebook_login_failed), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_SHORT).show();
                                    }
                                });



                                LoginManager.getInstance().logOut();
//                                    LogCat.e(name + "  " + token + "  " + object.getString("id"));
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });


        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        session = new SessionManager(getApplicationContext());
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity or profileActivity
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            finish();
        }
        initialUI();
    }

    //E6:3A:36:FA:15:94:6E:F8:DE:38:25:71:A7:74:7F:A0:08:9D:FA:AF original
    //AF:1F:67:BE:D1:2F:B1:50:37:83:98:91:D0:DA:61:E1:CB:5E:0E:05 relea
    private void initialUI() {
        progress_google = (ProgressBar) findViewById(R.id.progress_google);
        progress_facebook = (ProgressBar) findViewById(R.id.progress_facebook);
        setProgressValue(0);

        txtFacebook = (TextView) findViewById(R.id.txtFacebook);
        txtGoogle = (TextView) findViewById(R.id.txtGoogle);

        findViewById(R.id.txtFacebook).setOnClickListener(this);
        findViewById(R.id.imgFacebook).setOnClickListener(this);
        findViewById(R.id.txtGoogle).setOnClickListener(this);
        findViewById(R.id.imgGoogle).setOnClickListener(this);

        findViewById(R.id.txtSignIn).setOnClickListener(this);
        findViewById(R.id.txtRegister).setOnClickListener(this);
    }

    public  String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();
            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);
            Log.e("Package Name=", context.getApplicationContext().getPackageName());
            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));
                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
                Log.e("Key Hash=", key);
                Log.e("Key Hash=", key);
                Log.e("Key Hash=", key);
//                if(key.equals("rx9nvtEvsVA3g5iR0Nph4cteDgU=")){
//                    Toast.makeText(LoginActivity.this, "oksssss", Toast.LENGTH_LONG).show();
//                }else{
//                    Toast.makeText(LoginActivity.this, key , Toast.LENGTH_LONG).show();
//                }
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        }

        catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }


        return key;
    }

    private void signInWithFacebook(){
        if(!isProgressing) {
            progress_facebook.setVisibility(View.VISIBLE);
//            setFacebook_01();
            setFacebook();
            isProgressing = true;
        }
    }

    private void signInWithGoogle(){
        if(!isProgressing) {
            if(isGoogleConnected) {
                progress_google.setVisibility(View.VISIBLE);
                setGoogle();
                isProgressing = true;
            }else{
                Toast.makeText(this,getResources().getString(R.string.during_google_login), Toast.LENGTH_LONG).show();
            }

        }
    }

/////////////////////facebook login


    private void setFacebook(){
        List<String> permissionNeeds = Arrays.asList("publish_actions");
        Collection<String> permissions = Arrays.asList("public_profile", "user_friends", "email");//"public_profile", "user_friends", "email", "user_likes"
        //this loginManager helps you eliminate adding a LoginButton to your UI
        LoginManager manager = LoginManager.getInstance();
        manager.logInWithPublishPermissions(this, permissions);
        manager.registerCallback(callbackManager, new FacebookCallback<LoginResult>()
        {
            @Override
            public void onSuccess(LoginResult loginResult)
            {
                session.setLogin(true);
                Common.isFilter = false;
                progress_facebook.setVisibility(View.GONE);
                progress_google.setVisibility(View.GONE);
                signin_facebook();

            }
            @Override
            public void onCancel()
            {
                session.setLogin(false);
                System.out.println("onCancel");
                isProgressing = false;
                progress_facebook.setVisibility(View.GONE);
                progress_google.setVisibility(View.GONE);
            }
            @Override
            public void onError(FacebookException exception)
            {
                System.out.println("onError");
                isProgressing = false;
                progress_facebook.setVisibility(View.GONE);
                progress_google.setVisibility(View.GONE);
            }
        });
    }

    private void signin_facebook(){
        final Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            //Toast.makeText(FacebookLogin.this,"Wait...",Toast.LENGTH_SHORT).show();
            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            try {
                                Map<String, Object> params = new HashMap<String, Object>();

                                String facebook = "";
                                if(!object.isNull("email")){
                                    facebook = object.getString("email");
                                }else if(!object.isNull("name")){
                                    facebook = object.getString("name");
                                }else if(!object.isNull("id")){
                                    facebook = object.getString("id");
                                }
                                params.put("facebook", facebook);
                                params.put("user_name", profile.getName());
                                params.put("first_name", profile.getFirstName());
                                params.put("last_name", profile.getLastName());

                                ApiClient.getApiClient().signin_facebook(params, new Callback<ResultUpdateUser>() {
                                    @Override
                                    public void success(ResultUpdateUser resultUpdateUser, retrofit.client.Response response) {
                                        if(resultUpdateUser.getStat().equals(Common.success)) {
                                            Common.userProfile = resultUpdateUser.getUserData();
                                            if (Common.userProfile != null && !Common.userProfile.getId().isEmpty()) {
                                                session.setLogin(true);
                                                session.setUsername(Common.userProfile.getUsername());
                                                session.setUserID(Common.userProfile.getId());
                                                session.setEmail(Common.userProfile.getEmail());
                                                Common.dataChatListValuesList.clear();
                                                Common.setHeaderText(session, LoginActivity.this);
                                                Common.isVerified = new boolean[]{false, true, false};
                                                toProfileActivity();
                                            } else {
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_again), Toast.LENGTH_SHORT).show();
                                            }
                                        }else{
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.facebook_login_failed), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                //  e.printStackTrace();
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.facebook_login_failed), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, name, email, gender, birthday, friends, albums");
            request.setParameters(parameters);
            request.executeAsync();
            request.executeAsync();
        }
    }



//////////////////////Google login
    boolean isGoogleConnected = true;
    private void setGoogle(){
        // Configure sign-in to request the user's ID, email address, and basic profile. ID and
        // basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestEmail()
                .build();

            // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
            Common.mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();

            signIn();

    }

    private void signIn(){
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(Common.mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//  facebook callback result
        if(callbackManager!=null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            isGoogleConnected = true;
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        isProgressing = false;
        isGoogleConnected = false;
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.

            GoogleSignInAccount acct = result.getSignInAccount();

            progress_facebook.setVisibility(View.GONE);
            progress_google.setVisibility(View.GONE);

            session.setLogin(true);

            Common.isFilter = false;
            signin_google(acct);

        } else {
            // Signed out, show unauthenticated UI.
            session.setLogin(false);
            Common.setHeaderText(session, this);

            progress_facebook.setVisibility(View.GONE);
            progress_google.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.google_login_failed), Toast.LENGTH_SHORT).show();
        }
    }

    private void signin_google(GoogleSignInAccount acct){
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("google", acct.getId());
        params.put("user_name", acct.getDisplayName());
        params.put("first_name", acct.getFamilyName());
        params.put("last_name", acct.getGivenName());
        params.put("email", acct.getEmail());

        ApiClient.getApiClient().signin_google(params, new Callback<ResultUpdateUser>() {
            @Override
            public void success(ResultUpdateUser resultUpdateUser, retrofit.client.Response response) {
                if(resultUpdateUser.getStat().equals(Common.success)) {
                    Common.userProfile = resultUpdateUser.getUserData();
                    if (Common.userProfile != null && !Common.userProfile.getId().isEmpty()) {
                        session.setLogin(true);
                        session.setUsername(Common.userProfile.getUsername());
                        session.setUserID(Common.userProfile.getId());
                        session.setEmail(Common.userProfile.getEmail());
                        Common.setHeaderText(session, LoginActivity.this);
                        Common.dataChatListValuesList.clear();
                        Common.isVerified = new boolean[]{false, false, true};
                        toProfileActivity();
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_again), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.google_login_failed), Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void failure(RetrofitError error) {
                progress_facebook.setVisibility(View.GONE);
                progress_google.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txtFacebook:
//                signInWithFacebook();
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
                break;
            case R.id.imgFacebook:
                signInWithFacebook();
                break;
            case R.id.txtGoogle:
                signInWithGoogle();
                break;
            case R.id.imgGoogle:
                signInWithGoogle();
                break;
            case R.id.txtSignIn:
                toSignInRegister(Common.SIGN);
                break;
            case R.id.txtRegister:
                toSignInRegister(Common.REGISTER);
                break;
        }
    }

    private void toSignInRegister(String strMark){

        final Bundle bndlanimation_bottom_top =
                ActivityOptions.makeCustomAnimation(getApplicationContext(),
                        R.animator.animation_enter,R.animator.animation_leave).toBundle();
        Intent intent = new Intent(LoginActivity.this, SignRegisterActivity.class);
        intent.putExtra(Common.Mark, strMark);
        startActivity(intent, bndlanimation_bottom_top);
        finish();
    }


    private void setProgressValue(final int progress) {
        progress_google.setProgress(progress);
        progress_facebook.setProgress(progress);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setProgressValue(progress + 10);
            }
        });
        thread.start();
    }

    boolean isToProfile = true;
    private void toProfileActivity(){
        isProgressing = false;
        progress_facebook.setVisibility(View.GONE);
        progress_google.setVisibility(View.GONE);
        if(isToProfile) {
            Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
            startActivity(intent);
            finish();
            isToProfile = false;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }
}
