package com.quan.xap.activity.login_signup;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quan.xap.R;
import com.quan.xap.activity.profile.ProfileActivity;
import com.quan.xap.api.ApiClient;
import com.quan.xap.api.ApiClient_01;
import com.quan.xap.models.ResultEmailConform;
import com.quan.xap.models.ResultUpdateUser;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.SessionManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignRegisterActivity extends AppCompatActivity implements View.OnClickListener{

    String strMark;
    RelativeLayout rl_content;
    RelativeLayout rl_sign_in;
    RelativeLayout rl_register;

    AnimationSet animationSet;
    AlphaAnimation alphaAnimation;
    AlphaAnimation alphaAnimation_innative;
    TranslateAnimation moveAnimation_left_right;
    TranslateAnimation moveAnimation_right_left;
    ScaleAnimation zoomoutAnimation;

    ImageView imgMark_Sign;
    ImageView imgMark_Register;
    int width; int height;

    EditText editEmail;
    EditText editPassword;
    TextView txt_sign_account;
    TextView txtForgotPassword;

    EditText editFirstLastName;
    EditText editEmail_01;
    EditText editPassword_01;
    ImageView imgShow;
    TextView txt_create_account;

    boolean isClickable_sign = false;
    boolean isClickable_register = false;

    boolean isShow = false;

    SessionManager session;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            final Bundle bndlanimation_bottom_top =
                    ActivityOptions.makeCustomAnimation(getApplicationContext(),
                            R.animator.animator_top,R.animator.animator_bottom).toBundle();
            Intent intent = new Intent(SignRegisterActivity.this, LoginActivity.class);
            intent.putExtra(Common.Mark, strMark);
            startActivity(intent, bndlanimation_bottom_top);
            finish();
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_register);

        strMark = getIntent().getStringExtra(Common.Mark);
        session = new SessionManager(getApplicationContext());
        initialUI();

    }

    private void initialUI() {

        getWidth_Height();

        rl_content = (RelativeLayout) findViewById(R.id.rl_content);
        rl_sign_in = (RelativeLayout) findViewById(R.id.rl_sign_in);
        rl_register = (RelativeLayout) findViewById(R.id.rl_register);
        rl_sign_in.setVisibility(View.INVISIBLE);
        rl_register.setVisibility(View.INVISIBLE);

        findViewById(R.id.txtSignIn).setOnClickListener(this);
        findViewById(R.id.txtRegister).setOnClickListener(this);

        imgMark_Sign = (ImageView) findViewById(R.id.imgMark_Sign);
        imgMark_Register = (ImageView) findViewById(R.id.imgMark_Register);
        imgMark_Sign.setVisibility(View.INVISIBLE);
        imgMark_Register.setVisibility(View.INVISIBLE);

        setAnimationSet();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(strMark.equals(Common.SIGN)){
                    setRl_content(Common.SIGN);
                }else if(strMark.equals(Common.REGISTER)){
                    setRl_content(Common.REGISTER);
                }
            }
        }, 1000);
////////////////sign in
        editEmail = (EditText) findViewById(R.id.editEmail);
        editPassword = (EditText) findViewById(R.id.editPassword);
        txt_sign_account = (TextView) findViewById(R.id.txt_sign_account);
        txtForgotPassword = (TextView) findViewById(R.id.txtForgotPassword);
        setSignIn();
////////////////register
        editFirstLastName = (EditText) findViewById(R.id.editFirstLastName);
        editEmail_01 = (EditText) findViewById(R.id.editEmail_01);
        editPassword_01 = (EditText) findViewById(R.id.editPassword_01);
        imgShow = (ImageView) findViewById(R.id.imgShow);
        txt_create_account = (TextView) findViewById(R.id.txt_create_account);
        setRegister();

    }

    private void setSignIn(){
        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setViewsVisibility_SignIn();
            }
        });
        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setViewsVisibility_SignIn();
            }
        });
        txt_sign_account.setOnClickListener(this);
        txtForgotPassword.setOnClickListener(this);
    }

    private void setRegister(){
        editFirstLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setViewsVisibility_Register();
            }
        });
        editEmail_01.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setViewsVisibility_Register();
            }
        });
        editPassword_01.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setViewsVisibility_Register();
            }
        });
        imgShow.setOnClickListener(this);
        txt_create_account.setOnClickListener(this);
    }

    private void setViewsVisibility_SignIn(){
        String email = editEmail.getText().toString();
        if(!email.isEmpty() && !editPassword.getText().toString().isEmpty()){
            if(Common.isValidEmail(email)) {
                txt_sign_account.setBackground(getResources().getDrawable(
                        R.drawable.sign_register_button_boundary_conner_active));
                isClickable_sign = true;
            }else{
                isClickable_sign = false;
                txt_sign_account.setBackground(getResources().getDrawable(
                        R.drawable.sign_register_button_boundary_conner_inactive));
                Toast.makeText(this, getResources().getString(R.string.the_email_address_is_not_suitable), Toast.LENGTH_SHORT).show();
            }
        }else{
            txt_sign_account.setBackground(getResources().getDrawable(
                    R.drawable.sign_register_button_boundary_conner_inactive));
            isClickable_sign = false;
        }
    }

    private void setViewsVisibility_Register(){
        String email = editEmail_01.getText().toString();
        String password = editPassword_01.getText().toString();
        if(!email.isEmpty() && !password.isEmpty() && !editFirstLastName.getText().toString().isEmpty()){
            if(Common.isValidEmail(email) ) {
                if(password.length() >= 8){
                    txt_create_account.setBackground(getResources().getDrawable(
                            R.drawable.sign_register_button_boundary_conner_active));
                    isClickable_register = true;
                }else{
                    //Toast.makeText(this, getResources().getString(R.string.the_length_of_password_is_below_8_characters), Toast.LENGTH_LONG).show();
                }
            }else{
                isClickable_register = false;
                txt_create_account.setBackground(getResources().getDrawable(
                        R.drawable.sign_register_button_boundary_conner_inactive));
                //Toast.makeText(this, getResources().getString(R.string.the_email_address_is_not_suitable), Toast.LENGTH_SHORT).show();
            }
        }else{
            txt_create_account.setBackground(getResources().getDrawable(
                    R.drawable.sign_register_button_boundary_conner_inactive));
            isClickable_register = false;
        }
    }

    private void setRl_content(String strmark){
        if(strmark.equals(Common.SIGN)){
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    rl_sign_in.setVisibility(View.VISIBLE);
                    rl_register.setVisibility(View.INVISIBLE);

                    imgMark_Sign.setVisibility(View.VISIBLE);
                    imgMark_Register.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            rl_sign_in.startAnimation(alphaAnimation);

        }else if(strmark.equals(Common.REGISTER)){
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    rl_register.setVisibility(View.VISIBLE);
                    rl_sign_in.setVisibility(View.INVISIBLE);

                    imgMark_Register.setVisibility(View.VISIBLE);
                    imgMark_Sign.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            rl_register.startAnimation(alphaAnimation);
        }
    }

    private void setAnimationSet(){
        animationSet = new AnimationSet(false);//false means don't share interpolators
        alphaAnimation = new AlphaAnimation(0.1f, 1.0f);
        alphaAnimation_innative = new AlphaAnimation(1.0f, 0.0f);

        moveAnimation_left_right = new TranslateAnimation(Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, width/2,
                Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, 0);
        moveAnimation_right_left = new TranslateAnimation(Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, -width/2,
                Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, 0);
        zoomoutAnimation= new ScaleAnimation(1, 0.2f, 1, 0.2f);

        alphaAnimation.setDuration(Common.Duration);
        alphaAnimation_innative.setDuration(Common.Duration);
        moveAnimation_left_right.setDuration(Common.Duration);
        moveAnimation_right_left.setDuration(Common.Duration);
        zoomoutAnimation.setDuration(Common.Duration);

        moveAnimation_left_right.setFillAfter(false);
        moveAnimation_right_left.setFillAfter(false);

        moveAnimation_left_right.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imgMark_Register.setVisibility(View.VISIBLE);
                imgMark_Sign.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        moveAnimation_right_left.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imgMark_Sign.setVisibility(View.VISIBLE);
                imgMark_Register.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

//        animationSet.addAnimation(zoomoutAnimation);
        animationSet.addAnimation(moveAnimation_left_right);
//        animationSet.addAnimation(alphaAnimation);

        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animationSet.setDuration(Common.Duration);
        animationSet.setFillAfter(false);
    }

    ProgressDialog progressReportData;
    ProgressDialog progresssignupData;

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(progressReportData!=null) progressReportData.dismiss();
        if(progresssignupData!=null) progresssignupData.dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txtSignIn:
                if(!strMark.equals(Common.SIGN)) {
                    strMark = Common.SIGN;
                    setRl_content(Common.SIGN);
                    rl_register.startAnimation(alphaAnimation_innative);
                    imgMark_Register.startAnimation(moveAnimation_right_left);
                }
                break;
            case R.id.txtRegister:
                if(!strMark.equals(Common.REGISTER)) {
                    strMark = Common.REGISTER;
                    setRl_content(Common.REGISTER);
                    imgMark_Sign.startAnimation(moveAnimation_left_right);
                }
                break;
            case R.id.txt_sign_account:
                if(isClickable_sign){
                    progressReportData = new ProgressDialog(this);
                    progressReportData.show();
                    user_login();
                }
                break;
            case R.id.txtForgotPassword:
                final Bundle bndlanimation_bottom_top =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(),
                                R.animator.animator_left,R.animator.animator_right).toBundle();
                Intent intent = new Intent(SignRegisterActivity.this, ForgotActivity.class);
                intent.putExtra(Common.Mark, strMark);
                startActivity(intent, bndlanimation_bottom_top);
                finish();
                break;
            case R.id.txt_create_account:
                String password = editPassword_01.getText().toString();
                String email = editEmail_01.getText().toString();
                if(Common.isValidEmail(email)){
                    if(password.length() >= 8){
                        if(isClickable_register){
                            progresssignupData = new ProgressDialog(this);
                            progresssignupData.show();
                            user_signup();
                        }
                    }else{
                        Toast.makeText(this, getResources().getString(R.string.the_length_of_password_is_below_8_characters), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(this, getResources().getString(R.string.the_email_address_is_not_suitable), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.imgShow:
                isShow = !isShow;
                setImageShow();
                break;
        }
    }

    private void setImageShow(){
        if(isShow){
            imgShow.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_black_24dp));
            editPassword_01.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }else{
            imgShow.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_off_black_24dp));
            editPassword_01.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }

    private void getWidth_Height(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        width = width - (int)getResources().getDimension(R.dimen.sign_register_relative_layout_margin_left_right) * 2;
    }

    private void user_login(){
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("email", editEmail.getText().toString());
        params.put("password", editPassword.getText().toString());

        ApiClient.getApiClient().user_login(params, new Callback<ResultUpdateUser>() {
            @Override
            public void success(ResultUpdateUser resultUpdateUser, retrofit.client.Response response) {
                progressReportData.dismiss();
                if(resultUpdateUser.getStat().equals(Common.success)) {
                    Common.userProfile = resultUpdateUser.getUserData();
                    Common.dataChatListValuesList.clear();

                    if (Common.userProfile != null && !Common.userProfile.getId().isEmpty()) {
                        ///////////////template code
                        session.setLogin(true);
                        session.setUpdate(true);
                        Common.isFilter = false;
                        session.setUserID(resultUpdateUser.getUserData().getId());
                        session.setUsername(resultUpdateUser.getUserData().getUsername());
                        session.setEmail(resultUpdateUser.getUserData().getEmail());
                        Common.setHeaderText(session, SignRegisterActivity.this);
                        Common.isVerified = new boolean[]{false, false, false};
                        if (session.isLoggedIn()) {
                            // User is already logged in. Take him to main activity or profileActivity
                            Intent intent1 = new Intent(SignRegisterActivity.this, ProfileActivity.class);
                            startActivity(intent1);
                            finish();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_again), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.user_update_failed)  , Toast.LENGTH_SHORT).show();
                }
                /////////////////////////
            }

            @Override
            public void failure(RetrofitError error) {
                progressReportData.dismiss();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void user_signup(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("username", editFirstLastName.getText().toString());
        params.put("email", editEmail_01.getText().toString());
        params.put("password", editPassword_01.getText().toString());

        Location location = getLastKnownLocation();

        if(location != null){
            params.put("lat", String.valueOf(location.getLatitude()));
            params.put("lng", String.valueOf(location.getLongitude()));
        }

        ApiClient.getApiClient().user_signup(params, new Callback<ResultUpdateUser>() {
            @Override
            public void success(ResultUpdateUser resultUpdateUser, retrofit.client.Response response) {
//                Toast.makeText(getApplicationContext(), resultUpdateUser.getMessage(), Toast.LENGTH_SHORT).show();
                progresssignupData.dismiss();
                if(resultUpdateUser.getStat().equals(Common.success)) {
                    ///////////////template code
                    findViewById(R.id.txtSignIn).performClick();
                    //Common.userProfile = resultGetItems.getUserData();


                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("crendentials", "@$?76ctv");
                    params.put("from", "xap676@gmail.com");
                    params.put("to",  editEmail_01.getText().toString());
                    params.put("body", "<a href='http://82.223.19.247/xap/api/signup_confirm.php?token=" + resultUpdateUser.getUserData().getId() + "'>Please click this link for confirm</a> ");
                    params.put("subject", "Welcome to sign up");
                    params.put("from_name", getResources().getString(R.string.xap_support));

                    ApiClient_01.getApiClient().wang_email(params, new Callback<ResultEmailConform>() {
                        @Override
                        public void success(ResultEmailConform resultEmailConform, Response response) {
                            if(resultEmailConform!=null) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.confirm_mail),Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.email_failed_message), Toast.LENGTH_LONG).show();
                            }
                        }
                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.email_failed_message), Toast.LENGTH_LONG).show();
                        }
                    });
                }else{
                    if(resultUpdateUser.getCode().equals("111")){
                        Toast.makeText(getApplicationContext(), resultUpdateUser.getMessage()  , Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.user_update_failed)  , Toast.LENGTH_SHORT).show();
                    }

                }
                /////////////////////////
            }
            @Override
            public void failure(RetrofitError error) {
                progresssignupData.dismiss();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_SHORT).show();
            }
        });
    }

    LocationManager locationManager;
    private Location getLastKnownLocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }
}

