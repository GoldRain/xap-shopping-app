package com.quan.xap.activity.chat;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.quan.xap.R;
import com.quan.xap.activity.ProductionActivity;
import com.quan.xap.activity.ReasonActivity;
import com.quan.xap.activity.login_signup.LoginActivity;
import com.quan.xap.adapters.ChattingAdapter;
import com.quan.xap.api.ApiClient;
import com.quan.xap.models.InfoChat;
import com.quan.xap.models.InfoItem;
import com.quan.xap.models.ResultGetChat;
import com.quan.xap.models.ResultGetChatList;
import com.quan.xap.models.ResultGetItemInfo;
import com.quan.xap.models.ResultUpdateUser;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.MyDatabaseHelper;
import com.quan.xap.utility.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    List<InfoChat> chatList = new ArrayList<>();

    SessionManager session;
    String item_ID = "0";
    String to_user_ID = "0";
    InfoItem item_info = new InfoItem();

    String strLastDate="";

    ImageView imageSend;

    TextView txtTitle;
    TextView txtPrice;
    TextView txtUserName;
    CircleImageView imgPhoto;

    LinearLayout ll_LogOut;
    TextView txtLogOut_01;
    TextView txtLogOut_02;
    TextView txtLogOut_03;

    ListView messagesList;
    ChattingAdapter chattingAdapter;

    boolean isItemDeleted = false;

    int intervalTime = 5000; // 5 seconds
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if(!isAddChat) {
                getChat(strLastDate);
            }
            handler.postDelayed(runnable, intervalTime);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        if(new SessionManager(getApplicationContext()).getLock()) {
//            Toast.makeText(getApplicationContext(), "The app can not open. ask the support please", Toast.LENGTH_LONG).show();
            finish();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if(getIntent().getExtras().getString(Common.ITEM_ID)!=null) {
            if(!getIntent().getExtras().getString(Common.ITEM_ID).isEmpty()) {
                item_ID = getIntent().getExtras().getString(Common.ITEM_ID);
            }
        }
        to_user_ID = getIntent().getExtras().getString("to_user_id");

//        item_info = (InfoItem) getIntent().getSerializableExtra("item_info");

        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        initialUI();

//        getChat(strLastDate);

    }

    @Override
    protected void onResume() {
        super.onResume();
        strTempLast = strLastDate;
        handler.postDelayed(runnable, 100);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    private void initialUI() {
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtPrice = (TextView) findViewById(R.id.txtPrice);
        txtUserName = (TextView) findViewById(R.id.txtUserName);

        imgPhoto = (CircleImageView) findViewById(R.id.imgPhoto);

        findViewById(R.id.imageBack).setOnClickListener(this);
        findViewById(R.id.imgLogOut).setOnClickListener(this);
        findViewById(R.id.ll_text).setOnClickListener(this);
        findViewById(R.id.imgShare).setOnClickListener(this);

        ll_LogOut = (LinearLayout) findViewById(R.id.ll_LogOut);
        txtLogOut_01 = (TextView) findViewById(R.id.txtLogOut_01);
        txtLogOut_02 = (TextView) findViewById(R.id.txtLogOut_02);
        txtLogOut_03 = (TextView) findViewById(R.id.txtLogOut_03);
        ll_LogOut.setOnClickListener(this);
        txtLogOut_01.setOnClickListener(this);
        txtLogOut_02.setOnClickListener(this);
        txtLogOut_03.setOnClickListener(this);

        getItemInfo();
////////// set OnClick Send
        sendMessage();
    }

    private void setlastdateTodataChatListValuesList(){
        if(Common.userProfile!=null && chatList.size()!=0 && Common.dataChatListValuesList!=null){
            for(int i=0;i<Common.dataChatListValuesList.size(); i++) {
                if(Common.dataChatListValuesList.get(i).my_user_id.equals(Common.userProfile.getId()) &&
                        Common.dataChatListValuesList.get(i).other_user_id.equals(to_user_ID)){
                    Common.dataChatListValuesList.get(i).last_chatting_number
                            = chatList.get(chatList.size() - 1).getId();
                }
            }
        }
        MyDatabaseHelper mDatabaseHelper = new MyDatabaseHelper(this);
        mDatabaseHelper.setValues();
    }

    private void getUserInfo(){

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", to_user_ID);

            ApiClient.getApiClient().get_user_info(params, new Callback<ResultUpdateUser>() {
                @Override
                public void success(ResultUpdateUser resultGetItems, Response response) {
                    if(resultGetItems.getStat().equals(Common.success)) {

                        if(resultGetItems != null && resultGetItems.getUserData() != null){
                            txtUserName.setText(resultGetItems.getUserData().getUsername());
                            if (resultGetItems.getUserData().getProfile_image()!= null
                                    && !resultGetItems.getUserData().getProfile_image().isEmpty()) {
                                Picasso.with(ChatActivity.this).load(Common.common_url + resultGetItems.getUserData().getProfile_image()).into(imgPhoto);
                            }
                        }

                    }else{
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.get_user_info_failed), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
                }
            });

    }

    private void setItem_info(){
        if(item_info!=null && item_info.getTitle()!=null &&
                item_info.getCurrency()!=null && item_info.getPrice()!=null
                && item_info.getId().equals(item_ID)) {
            txtTitle.setText(item_info.getTitle());
            if(!item_info.getCurrency().isEmpty()) {
                txtPrice.setText(Common.Currency[Integer.valueOf(item_info.getCurrency())] + " " + item_info.getPrice());
            }

        }else{
            isItemDeleted = true;
            txtTitle.setText(getResources().getString(R.string.the_production_already_is_deleted));
        }
        getUserInfo();
    }

    private void getItemInfo(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", item_ID);
        ApiClient.getApiClient().get_item_info(params, new Callback<ResultGetItemInfo>(){
            @Override
            public void success(ResultGetItemInfo resultGetItems, Response response) {
                if(resultGetItems.getStat().equals(Common.success)) {
                    item_info = resultGetItems.getUserData();
                    setItem_info();
                }else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.get_item_info_failed), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addChat(String messageBody){
        Map<String, Object> params = new HashMap<String, Object>();
        if(Common.userProfile!=null && Common.userProfile.getId()!=null && !Common.userProfile.getId().isEmpty()
                && item_info.getUser()!=null && item_info.getUser().getId()!=null  && !item_info.getUser().getId().isEmpty())  {
            params.put("from_user_id", Common.userProfile.getId());
            params.put("to_user_id", to_user_ID); //item_info.getUser().getId());
            params.put("item_id", item_ID); //item_ID);
            params.put("message", messageBody);
            ApiClient.getApiClient().add_chat(params, new Callback<ResultGetChat>(){
                @Override
                public void success(ResultGetChat resultGetItems, Response response) {
                    if(resultGetItems.getStat().equals(Common.success)) {
                        InfoChat chatData = resultGetItems.getData();
                        if (chatData != null) {
                            strLastDate = chatData.getTimestamps();
                            String messageBody = chatData.getMessage();
                            if (!messageBody.isEmpty()) {
                                chattingAdapter.addMessage(messageBody, ChattingAdapter.DIRECTION_INCOMING);
                                messagesList.setSelection(chattingAdapter.getCount() - 1);
                            }
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.chatting_message_failed)  , Toast.LENGTH_LONG).show();
                    }
                    isAddChat = false;
                }

                @Override
                public void failure(RetrofitError error) {
                    isAddChat = false;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    String strTempLast;
    private void getChat(String last_date){
        imageSend.setEnabled(false);
        Map<String, Object> params = new HashMap<String, Object>();

//        if(item_info.getUser()!=null) params.put("user_id", item_info.getUser().getId()); //to_user_id
        if(Common.userProfile!=null && Common.userProfile.getId()!=null && !Common.userProfile.getId().isEmpty()) {
            params.put("user_id", Common.userProfile.getId());
            params.put("last_date", last_date);

            ApiClient.getApiClient().get_chat(params, new Callback<ResultGetChatList>(){
                @Override
                public void success(ResultGetChatList resultGetItems, Response response) {
                    if(resultGetItems.getStat().equals(Common.success)) {
                        chatList = resultGetItems.getDataList();
                        setlastdateTodataChatListValuesList();

                        for (InfoChat item : resultGetItems.getDataList()) {
                            if (Common.userProfile != null) {
                                //                        if (item.getFrom_user_id().equals(Common.userProfile.getId())
                                //                                || item.getTo_user_id().equals(Common.userProfile.getId())) {
                                if (item_ID.equals(item.getItem_id())) {
                                    if (item.getFrom_user_id().equals(Common.userProfile.getId())
                                            && item.getTo_user_id().equals(to_user_ID)) {
                                        if (!isAddChat) {
                                            strLastDate = item.getTimestamps();
                                            chattingAdapter.addMessage(item.getMessage(), ChattingAdapter.DIRECTION_INCOMING);
                                            messagesList.setSelection(chattingAdapter.getCount() - 1);
                                        }
                                    }
                                    if (item.getTo_user_id().equals(Common.userProfile.getId())
                                            && item.getFrom_user_id().equals(to_user_ID)) {
                                        strLastDate = item.getTimestamps();
                                        chattingAdapter.addMessage(item.getMessage(), ChattingAdapter.DIRECTION_OUTGOING);
                                        messagesList.setSelection(chattingAdapter.getCount() - 1);
                                    }
                                }
                                //                        }
                            }
                        }

                    }else{
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.chatting_message_failed)  , Toast.LENGTH_LONG).show();
                    }
                    imageSend.setEnabled(true);
                }

                @Override
                public void failure(RetrofitError error) {
                    imageSend.setEnabled(true);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    boolean isAddChat = false;

    private void sendMessage(){
        messagesList = (ListView) findViewById(R.id.messageList);
        chattingAdapter = new ChattingAdapter(this);
        messagesList.setAdapter(chattingAdapter);
//        for(int i=0;i<8;i++){
//            chattingAdapter.addMessage("", ChattingAdapter.DIRECTION_INCOMING);
//        }
        final EditText editMessage=(EditText) findViewById(R.id.editMessage);
        imageSend=(ImageView) findViewById(R.id.imageSend);
        imageSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!editMessage.getText().toString().trim().isEmpty()) {
                    isAddChat = true;
                    addChat(editMessage.getText().toString().trim());
                    editMessage.setText("");
                }
            }
        });
    }

    private void startProductionActivity(){
        if(isItemDeleted){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.the_production_already_is_deleted), Toast.LENGTH_LONG).show();
        }else {
            if(item_info.getId()!=null && item_info.getId().equals(item_ID)) {
                Intent intent = new Intent(this, ProductionActivity.class);
                intent.putExtra(Common.ITEM_ID, item_ID);
                startActivity(intent);
                finish();
            }
        }
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageBack:
                finish();
                break;
            case R.id.ll_text:
                startProductionActivity();
                break;
            case R.id.imgShare:
                startProductionActivity();
                break;
            case R.id.imgLogOut:
                if(ll_LogOut.getVisibility() == View.VISIBLE){
                    ll_LogOut.setVisibility(View.GONE);
                }else {
                    ll_LogOut.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.txtLogOut_01:
                ll_LogOut.setVisibility(View.GONE);
                if(isItemDeleted){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.the_production_already_is_deleted), Toast.LENGTH_LONG).show();
                }else {
                    if(item_info.getId()!=null && item_info.getId().equals(item_ID) && item_info.getUser_id()!=null) {
                        Intent intent = new Intent(ChatActivity.this, ReasonActivity.class);
                        intent.putExtra(Common.ITEM_ID, item_info.getUser_id());
                        startActivity(intent);
                    }
                }
//                finish();
                break;
            case R.id.txtLogOut_02:
                ll_LogOut.setVisibility(View.GONE);
//                finish();
                break;
            case R.id.txtLogOut_03:
                ll_LogOut.setVisibility(View.GONE);
//                finish();
                break;
        }
    }
}
