package com.quan.xap.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by guoquan on 4/12/2017.
 */

public class ResultGetChat implements Serializable{
    @SerializedName("stat")
    private String stat;
    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private InfoChat data = new InfoChat();

    public String getStat() {
        return stat;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public InfoChat getData() {
        return data;
    }

}
