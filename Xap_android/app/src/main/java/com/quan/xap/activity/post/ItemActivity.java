package com.quan.xap.activity.post;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.quan.xap.BuildConfig;
import com.quan.xap.R;
import com.quan.xap.api.ApiClient;
import com.quan.xap.dialog.CameraGalleryDialog;
import com.quan.xap.interfaces.CameraGallaryEventListener;
import com.quan.xap.models.InfoItem;
import com.quan.xap.models.ResultGetItemInfo;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.UserPicture;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class ItemActivity extends AppCompatActivity implements View.OnClickListener {

    int PERMISSIONS_REQUEST_PERMISSION = 988;

    InputMethodManager imm;

    List<ImageView> imgPhotos = new ArrayList<>();
    ImageView imgChangePhoto;
    TypedFile photofile;

    int cameraNumber = 0;

    LinearLayout ll_AcceptableTrades;
    LinearLayout ll_FirmPrice;
    LinearLayout ll_ShippingAvailable;

    EditText editTitle;
    EditText editDescription;
    EditText editPrice;
    EditText editCategory;

    Spinner spinnerCurrency;
    Button btnRotation1, btnRotation2, btnRotation3, btnRotation4;


    String strTitle = "";
    InfoItem item_info;
    boolean[] isDrawableChanges = new boolean[]{false,false,false,false};

    NumberPicker picker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        strTitle = getIntent().getExtras().getString("Title");
        item_info = (InfoItem) getIntent().getSerializableExtra(Common.ITEM_ID);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA
                                , Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_PERMISSION);
            } else {

            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        initialUI();
        setProfileImage();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(progressDialog!=null) progressDialog.dismiss();
    }

    private void initialUI() {
        findViewById(R.id.imageBack).setOnClickListener(this);

        findViewById(R.id.txtListItem).setOnClickListener(this);

        ll_AcceptableTrades = (LinearLayout) findViewById(R.id.ll_AcceptableTrades);
        ll_FirmPrice = (LinearLayout) findViewById(R.id.ll_FirmPrice);
        ll_ShippingAvailable = (LinearLayout) findViewById(R.id.ll_ShippingAvailable);
        ll_AcceptableTrades.setOnClickListener(this);
        ll_FirmPrice.setOnClickListener(this);
        ll_ShippingAvailable.setOnClickListener(this);

        editTitle = (EditText) findViewById(R.id.editTitle);
        editDescription = (EditText) findViewById(R.id.editDescription);
        editPrice = (EditText) findViewById(R.id.editPrice);
        editCategory = (EditText) findViewById(R.id.editCategory);

        if(strTitle!=null) {
            editTitle.setText(strTitle);
        }
        editCategory.setInputType(InputType.TYPE_NULL);
        editCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(picker.getVisibility()!=View.VISIBLE) {
//                    editCategory.setText(Common.Category_String[picker.getValue()]);
                    picker.setVisibility(View.VISIBLE);
                }else{
                    picker.setVisibility(View.GONE);
                }
            }
        });

        spinnerCurrency = (Spinner) findViewById(R.id.spinnerCurrency);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, Common.Currency);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCurrency.setAdapter(dataAdapter);

        picker = (NumberPicker) findViewById(R.id.picker);
        picker.setMaxValue(Common.Category_String.length-1);
        picker.setMinValue(0);
        picker.setVisibility(View.GONE);
        picker.setDisplayedValues(Common.Category_String);
        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                editCategory.setText(Common.Category_String[newVal]);
            }
        });
        picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.setVisibility(View.GONE);
            }
        });

        imgChangePhoto = new ImageView(this);
        imgPhotos.clear();
        Common.outputFileUri_List = new Uri[]{Uri.parse(""),Uri.parse(""),Uri.parse(""),Uri.parse("")};
        imgPhotos.add((ImageView) findViewById(R.id.imgPhoto_01));
        imgPhotos.add((ImageView) findViewById(R.id.imgPhoto_02));
        imgPhotos.add((ImageView) findViewById(R.id.imgPhoto_03));
        imgPhotos.add((ImageView) findViewById(R.id.imgPhoto_04));

        for(int i=0; i<imgPhotos.size(); i++){
            final int ii = i;
            imgPhotos.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cameraNumber = ii;
//                    imgChangePhoto = imgPhotos.get(cameraNumber);
                    openDonatorDialog();
                }
            });
        }

//        imm.showSoftInput(editCategory, InputMethodManager.RESULT_SHOWN);

        if(item_info!= null){
            loadItemInfo();
        }

        btnRotation1 = (Button)findViewById(R.id.btn_rotation1);
        btnRotation2 = (Button)findViewById(R.id.btn_rotation2);
        btnRotation3 = (Button)findViewById(R.id.btn_rotation3);
        btnRotation4 = (Button)findViewById(R.id.btn_rotation4);
        btnRotation1.setOnClickListener(this);
        btnRotation2.setOnClickListener(this);
        btnRotation3.setOnClickListener(this);
        btnRotation4.setOnClickListener(this);
    }

    List<String> ImagesArray = new ArrayList<>();
    private void loadItemInfo(){
        loadImgPhotos();

        editTitle.setText(item_info.getTitle());
        editDescription.setText(item_info.getDescription());
        editPrice.setText(item_info.getPrice());
        try{
            spinnerCurrency.setSelection(Integer.valueOf(item_info.getCurrency()));
        }catch (Exception e){};

        try{
            if(item_info.getCategory().matches("[0-9]+")) {
                editCategory.setText(Common.Category_String[Integer.valueOf(item_info.getCategory())]);
            }
        }catch (Exception e){};


        if(item_info.getAcceptable_trades().equals("0")){
            ll_AcceptableTrades.setAlpha(0.3f);
        }else{
            ll_AcceptableTrades.setAlpha(1.0f);
        }
        if(item_info.getFirm_price().equals("0")){
            ll_FirmPrice.setAlpha(0.3f);
        }else{
            ll_FirmPrice.setAlpha(1.0f);
        }
        if(item_info.getShipping_available().equals("0")){
            ll_ShippingAvailable.setAlpha(0.3f);
        }else{
            ll_ShippingAvailable.setAlpha(1.0f);
        }

    }

    private void loadImgPhotos(){
        if(item_info!= null){
            if(item_info!=null) {
                ImagesArray.clear();
                if(!item_info.getPic1().isEmpty())
                    ImagesArray.add(Common.common_url + item_info.getPic1());
                if(!item_info.getPic2().isEmpty())
                    ImagesArray.add(Common.common_url + item_info.getPic2());
                if(!item_info.getPic3().isEmpty())
                    ImagesArray.add(Common.common_url + item_info.getPic3());
                if(!item_info.getPic4().isEmpty())
                    ImagesArray.add(Common.common_url + item_info.getPic4());
            }
        }
        for(int i=0;i<ImagesArray.size();i++){
            Picasso.with(this).load(ImagesArray.get(i)).into(imgPhotos.get(i));
            isDrawableChanges[i] = true;
//            ImageLoader imgLoader = new ImageLoader(this);
//            int loader = R.drawable.loader;
//            imgLoader.DisplayImage(ImagesArray.get(i), loader, imgPhotos.get(i));/////
        }
    }

    private void updateitem(){
        Map<String, Object> params = new HashMap<String, Object>();

        List<TypedFile> imageTemps = new ArrayList<>();
        imageTemps.clear();
        for(int i=0; i<4; i++) {
            if (saveImageTypeFile(i) != null) imageTemps.add(saveImageTypeFile(i));
        }
        if(imageTemps.size()==0){
            Toast.makeText(this, getResources().getString(R.string.select_picture_please), Toast.LENGTH_LONG).show();
            if(progressDialog!=null) progressDialog.dismiss();
            return;
        }
        for(int i=0; i<imageTemps.size(); i++){
            params.put("pic" + String.valueOf(i+1), imageTemps.get(i));
        }

        if(item_info!=null) {
            params.put("item_id", item_info.getId());
        }
        params.put("title", editTitle.getText().toString());
        params.put("category", String.valueOf(picker.getValue()));
        params.put("description", editDescription.getText().toString());
        params.put("price", editPrice.getText().toString());
        params.put("currency", String.valueOf(spinnerCurrency.getSelectedItemPosition()));

        params.put("shipping_available", String.valueOf(getAlphaOfTerms(ll_ShippingAvailable)));
        params.put("firm_price", String.valueOf(getAlphaOfTerms(ll_FirmPrice)));
        params.put("acceptable_trades", String.valueOf(getAlphaOfTerms(ll_AcceptableTrades)));

        params.put("latitude", String.valueOf(Common.latitude));
        params.put("longitude", String.valueOf(Common.longitude));

        ApiClient.getApiClient().update_item(params, new Callback<ResultGetItemInfo>(){
            @Override
            public void success(ResultGetItemInfo resultGetItems, Response response) {
                if(progressDialog!=null) progressDialog.dismiss();
                if(resultGetItems.getStat().equals(Common.success)) {
                    if(Common.userItems!=null) Common.userItems.clear();
                    Common.outputFileUri_List = new Uri[]{Uri.parse(""), Uri.parse(""), Uri.parse(""), Uri.parse("")};
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.get_item_info_failed), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if(progressDialog!=null) progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
            }
        });
    }

    private TypedFile saveImageTypeFile(int index){
        if(!isDrawableChanges[index]){
            return null;
        }
        if(imgPhotos!= null && imgPhotos.get(index)!=null) {
            if(imgPhotos.get(index).getDrawable()!=null) {
                return new TypedFile("image/*", Common.savebitmap("photo" + String.valueOf(index), ((BitmapDrawable) imgPhotos.get(index).getDrawable()).getBitmap()));
            }
        }
        return null;
    }

    private void additem(){
        Map<String, Object> params = new HashMap<String, Object>();

        List<TypedFile> imageTemps = new ArrayList<>();
        imageTemps.clear();
        for(int i=0; i<4; i++) {
            if (saveImageTypeFile(i) != null) imageTemps.add(saveImageTypeFile(i));
        }
        if(imageTemps.size()==0){
            Toast.makeText(this, "Select picture, please", Toast.LENGTH_LONG).show();
            if(progressDialog!=null) progressDialog.dismiss();
            return;
        }
        for(int i=0; i<imageTemps.size(); i++){
            params.put("pic" + String.valueOf(i+1), imageTemps.get(i));
        }

        if(Common.userProfile!=null && Common.userProfile.getId() != null && !Common.userProfile.getId().isEmpty()) {
            params.put("user_id", Common.userProfile.getId());

            params.put("title", editTitle.getText().toString());
            params.put("category", String.valueOf(picker.getValue()));
            params.put("description", editDescription.getText().toString());
            params.put("price", editPrice.getText().toString());
            params.put("currency", String.valueOf(spinnerCurrency.getSelectedItemPosition()));

            params.put("shipping_available", String.valueOf(getAlphaOfTerms(ll_ShippingAvailable)));
            params.put("firm_price", String.valueOf(getAlphaOfTerms(ll_FirmPrice)));
            params.put("acceptable_trades", String.valueOf(getAlphaOfTerms(ll_AcceptableTrades)));

            params.put("latitude", String.valueOf(Common.latitude));
            params.put("longitude", String.valueOf(Common.longitude));

            ApiClient.getApiClient().add_item(params, new Callback<ResultGetItemInfo>(){
                @Override
                public void success(ResultGetItemInfo resultGetItems, Response response) {
                    if(progressDialog!=null) progressDialog.dismiss();
                    if(resultGetItems.getStat().equals(Common.success)) {
                        if(Common.userItems!=null) Common.userItems.clear();
                        Common.outputFileUri_List = new Uri[]{Uri.parse(""), Uri.parse(""), Uri.parse(""), Uri.parse("")};
                        Toast.makeText(getApplicationContext(), "Success"  , Toast.LENGTH_LONG).show();
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(), "Failed"  , Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    if(progressDialog!=null) progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_LONG).show();
                }
            });
        }else{
            if(progressDialog!=null) progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_LONG).show();
        }
    }

    ProgressDialog progressDialog;

    @Override
    public void onClick(View view) {
        picker.setVisibility(View.GONE);
        switch (view.getId()){
            case R.id.txtListItem:
                progressDialog = new ProgressDialog(this);
                progressDialog.show();
                progressDialog.setCanceledOnTouchOutside(false);
                if(item_info==null) {
                    additem();
                }else{
                    updateitem();
                }
                break;
            case R.id.imageBack:
                finish();
                break;
            case R.id.ll_AcceptableTrades:
                setAlphaOfTerms(ll_AcceptableTrades);
                break;
            case R.id.ll_FirmPrice:
                setAlphaOfTerms(ll_FirmPrice);
                break;
            case R.id.ll_ShippingAvailable:
                setAlphaOfTerms(ll_ShippingAvailable);
                break;

            case R.id.btn_rotation1:
                if(imgPhotos.size() >= 1 && imgPhotos.get(0) != null){
                    saveRotateImage(0, imgPhotos.get(0));
                }

                break;
            case R.id.btn_rotation2:
                if(imgPhotos.size() >= 2 && imgPhotos.get(1) != null){
                    saveRotateImage(1, imgPhotos.get(1));
                }

                break;
            case R.id.btn_rotation3:
                if(imgPhotos.size() >= 3 && imgPhotos.get(2) != null){
                    saveRotateImage(2, imgPhotos.get(2));
                }

                break;
            case R.id.btn_rotation4:
                if(imgPhotos.size() >=4 && imgPhotos.get(3) != null){
                    saveRotateImage(3, imgPhotos.get(3));
                }

                break;
        }
    }

    private void setAlphaOfTerms(LinearLayout ll_temp){
        if(ll_temp.getAlpha()==0.3f){
            ll_temp.setAlpha(1.0f);
        }else{
            ll_temp.setAlpha(0.3f);
        }
    }

    private int getAlphaOfTerms(LinearLayout ll_temp){
        if(ll_temp.getAlpha()==1.0f){
            return 1;
        }else{
            return 0;
        }
    }


    private void setProfileImage(){
        for(int i=0; i<4; i++) {
            if (Common.outputFileUri_List[i] != null && !Common.outputFileUri_List[i].getPath().isEmpty()) {
                try {
                    imgPhotos.get(i).setImageBitmap(new UserPicture(Common.outputFileUri_List[i]
                            , getContentResolver()).getBitmap());

                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            } else {
            }
        }
    }

    ////////////open camera and gallery ///////////////////////////////
    private void openDonatorDialog() {
        CameraGalleryDialog rDonator = new CameraGalleryDialog(this);
        rDonator.setEventListener(new CameraGallaryEventListener() {
            @Override
            public void selectCamera() {
                testCamera();
            }
            @Override
            public void selectGallary() {
                testGallery();
            }
        });
        rDonator.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = rDonator.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        rDonator.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.list_item_sample_boundary_conner_10dp));
        rDonator.show();
    }

    //////////////////gallery//////////////////////////
    private static final int SELECT_PICTURE = 999;
    private void testGallery(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    //////////////////////////////camera//////////////////////
    int TAKE_PHOTO_CODE = 888;
    public static int count = 0;

    private void testCamera(){
        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();
        count++;
        String file = dir+count+".jpg";
        Common.newfile = new File(file);
        try {   Common.newfile.createNewFile();   }
        catch (IOException e)
        {                }
        if(Common.newfile!=null) {
//            Common.photoTempUri = Uri.fromFile(Common.newfile);
            Common.photoTempUri = FileProvider.getUriForFile(ItemActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    Common.newfile);
        }
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Common.photoTempUri);
            cameraIntent.addFlags(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK ) {
            if (requestCode == TAKE_PHOTO_CODE) {
                Log.d("CameraDemo", "Pic saved");
                if(Common.photoTempUri!=null && !Common.photoTempUri.getPath().isEmpty()) {
                    try {
//                        imgChangePhoto.setVisibility(View.VISIBLE);
                        if(imgPhotos.size()!=0) {

                            imgPhotos.get(cameraNumber).setImageBitmap( new UserPicture(Common.photoTempUri, getContentResolver()).getBitmap() );
                            Common.outputFileUri_List[cameraNumber] = Common.photoTempUri;

                            File imageFile = Common.savebitmap("photo" + String.valueOf(cameraNumber), imgPhotos.get(cameraNumber).getDrawingCache());
                            photofile = new TypedFile("image/*", imageFile);

                            isDrawableChanges[cameraNumber] = true;
                        }
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }else{
                }
            }
            if (requestCode == SELECT_PICTURE) {
                Common.photoTempUri = data.getData();
                try {
//                    imgChangePhoto.setVisibility(View.VISIBLE);
                    if(imgPhotos.size()!=0) {
                        imgPhotos.get(cameraNumber).setImageBitmap(new UserPicture(Common.photoTempUri, getContentResolver()).getBitmap());
                        Common.outputFileUri_List[cameraNumber] = Common.photoTempUri;

                        File imageFile = Common.savebitmap("photo" + String.valueOf(cameraNumber), imgPhotos.get(cameraNumber).getDrawingCache());
                        photofile = new TypedFile("image/*", imageFile);

                        isDrawableChanges[cameraNumber] = true;
                    }
                } catch (IOException e) {
                    Log.e("camera activity", "Failed to load image", e);
                    e.printStackTrace();
                }
            }
        }
    }

    ///////////////////////////////////

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            finish();
        }else
        {

        }
    }


    private void saveRotateImage(int position, ImageView imageView){
        Bitmap bmp = null;
        try {
            bmp = ((BitmapDrawable) imgPhotos.get(position).getDrawable()).getBitmap();
            //bmp = new UserPicture( Common.outputFileUri_List[position], getContentResolver()).getBitmap();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/photo");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-"+ n +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            Bitmap finalBitmap = rotate(bmp, 90);
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();

            imageView.setImageBitmap(finalBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Common.outputFileUri_List[position] =Uri.parse(file.toString());
    }

    public static Bitmap rotate(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(),source.getHeight(), matrix, false);
    }

}
