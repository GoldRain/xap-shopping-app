package com.quan.xap.activity.help;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.quan.xap.R;

public class HelpActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        initialUI();
    }

    private void initialUI(){
        ((TextView)findViewById(R.id.txtTitle)).setText(getResources().getString(R.string.help));
        findViewById(R.id.imageBack).setOnClickListener(this);
        findViewById(R.id.txtHelp_01).setOnClickListener(this);
        findViewById(R.id.txtHelp_02).setOnClickListener(this);
        findViewById(R.id.txtHelp_03).setOnClickListener(this);
        findViewById(R.id.txtHelp_04).setOnClickListener(this);
        findViewById(R.id.txtHelp_05).setOnClickListener(this);
        findViewById(R.id.txtHelp_06).setOnClickListener(this);
        findViewById(R.id.txtHelp_07).setOnClickListener(this);
        findViewById(R.id.txtHelp_08).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.imageBack:
                finish();
                break;
            case R.id.txtHelp_01:
                intent = new Intent(HelpActivity.this, TermsConditionsActivity.class);
                startActivity(intent);
                break;
            case R.id.txtHelp_02:
                intent = new Intent(HelpActivity.this, PrivacyPolicyActivity.class);
                startActivity(intent);
                break;
            case R.id.txtHelp_03:
                intent = new Intent(HelpActivity.this, SafetyGuidelinesActivity.class);
                startActivity(intent);
                break;
            case R.id.txtHelp_04:
                intent = new Intent(HelpActivity.this, XapRulesActivity.class);
                startActivity(intent);
                break;
            case R.id.txtHelp_05:
//                intent = new Intent(HelpActivity.this, LikeUsOnFacebookActivity.class);
//                startActivity(intent);
                break;
            case R.id.txtHelp_06:
//                intent = new Intent(HelpActivity.this, XapOnTwitterActivity.class);
//                startActivity(intent);
                break;
            case R.id.txtHelp_07:
                intent = new Intent(HelpActivity.this, FaqActivity.class);
                startActivity(intent);
                break;
            case R.id.txtHelp_08:
                intent = new Intent(HelpActivity.this, ContactUsActivity.class);
                startActivity(intent);
                break;
        }
    }
}
