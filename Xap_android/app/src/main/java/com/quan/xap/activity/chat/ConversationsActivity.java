package com.quan.xap.activity.chat;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.quan.xap.MyChatService;
import com.quan.xap.R;
import com.quan.xap.activity.login_signup.LoginActivity;
import com.quan.xap.adapters.ChattingAdapter;
import com.quan.xap.adapters.ConversationAdapter;
import com.quan.xap.api.ApiClient;
import com.quan.xap.constants.Constants;
import com.quan.xap.interfaces.ConversasionDeleteListener;
import com.quan.xap.models.ClassChatList;
import com.quan.xap.models.InfoChat;
import com.quan.xap.models.InfoItem;
import com.quan.xap.models.ResultGetChatList;
import com.quan.xap.models.ResultGetItemInfo;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.DataChatListValues;
import com.quan.xap.utility.MyDatabaseHelper;
import com.quan.xap.utility.SessionManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ConversationsActivity extends AppCompatActivity implements View.OnClickListener {


    List<ClassChatList> chatListSort = new ArrayList<>();

    SessionManager session;
    ChattingAdapter chattingAdapter;
    String strLastDate = "0";

    ListView listView;

    int intintervalTime = 5000;
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
//            Toast.makeText(getApplicationContext(), "getChat", Toast.LENGTH_LONG).show();
            getChat(strLastDate);
            handler.postDelayed(runnable, intintervalTime);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversations);

        if(new SessionManager(getApplicationContext()).getLock()) {
//            Toast.makeText(getApplicationContext(), "The app can not open. ask the support please", Toast.LENGTH_LONG).show();
            finish();
        }

        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }else{
            initialUI();
            getDataChatListValuesList();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(callSetListView, new IntentFilter("setListView"));

//        setListView();

        startService_EnableNoti();

//        getChat(strLastDate);
//        handler.postDelayed(runnable, 2000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        setDataChatListValuesList();
        unregisterReceiver(callSetListView);
        handler.removeCallbacks(runnable);
    }

    private void initialUI() {
        findViewById(R.id.imageBack).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtTitle)).setText(getResources().getString(R.string.conversations));
        listView = (ListView) findViewById(R.id.listView);
    }


    private void setListView(){
        ConversationAdapter ItemAdapter = new ConversationAdapter(this, Common.chatListSort);
        ItemAdapter.setEventListener(new ConversasionDeleteListener() {
            @Override
            public void DeleteConversasion(int i) {
                if(i<Common.chatListSort.size()) {
                    Common.chatListSort.remove(i);
                }
                if(i<Common.dataChatListValuesList.size()){
                    Common.dataChatListValuesList.remove(i);
                }
            }
        });
        listView.setAdapter(ItemAdapter);
        ItemAdapter.notifyDataSetChanged();
        Log.d("Conversation:", "listView set adapter success");
    }

    //////////////////notification ///////////////////////
    private void startService_EnableNoti(){
        stopService_DisableNoti();
        Intent serviceIntent = new Intent(this, MyChatService.class);
        serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
        startService(serviceIntent);
    }

    private void stopService_DisableNoti(){
        Intent serviceIntent = new Intent(this, MyChatService.class);
        serviceIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService( serviceIntent );
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.cancel(11);
    }

    private final BroadcastReceiver callSetListView = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setListView();
        }
    };


    boolean iListView = true;
    private void getChat(final String last_date){

        Map<String, Object> params = new HashMap<String, Object>();
        if(Common.userProfile!=null && Common.userProfile.getId()!=null && !Common.userProfile.getId().isEmpty()) {
            params.put("user_id", Common.userProfile.getId()); //to_user_id
            params.put("last_date", "");

            ApiClient.getApiClient().get_chat(params, new Callback<ResultGetChatList>() {
                @Override
                public void success(ResultGetChatList resultGetItems, Response response) {
                    if(resultGetItems.getStat().equals(Common.success)) {
                        String currentdate = "";

                        if (resultGetItems.getDataList().size() != 0) {
                            currentdate = resultGetItems.getDataList().get(resultGetItems.getDataList().size() - 1).getTimestamps();
                        }
                        if (currentdate.compareTo(last_date) > 0) {
                            strLastDate = currentdate;
                            chatListSort.clear();
                            List<InfoChat> chatListFrom = new ArrayList<InfoChat>();
                            List<InfoChat> chatListTo = new ArrayList<InfoChat>();
                            for (InfoChat item : resultGetItems.getDataList()) {
                                if (Common.userProfile != null) {
                                    if (item.getFrom_user_id().equals(Common.userProfile.getId())) {
//                                || item.getTo_user_id().equals(Common.userProfile.getId())) {
                                        chatListFrom.add(item);
                                    } else if (item.getTo_user_id().equals(Common.userProfile.getId())) {
                                        chatListTo.add(item);
                                    }
                                }
                            }

                            setChatListFrom(chatListFrom);
                            setChatListTo(chatListTo);

                            getItemInfo_SortById();

//                            if(iListView) {
//                                refreshDataChatListValuesList();
//                                iListView = false;
//                            }

                            applyUnreadNumberToListView();

                            iListView = false;
                        } else {
                            getItemInfo_SortById();
//                            if(!iListView) {
//                                setListView();
//                                iListView = true;
//                            }
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.chatting_message_failed), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
                }
            });
        }
    }


    private void setChatListFrom(List<InfoChat> mData){
        if(mData==null || mData.size()==0){
            return;
        }

        Collections.sort(mData, new Comparator<InfoChat>() {
            @Override
            public int compare(InfoChat t1, InfoChat t2) {
                return Float.compare(Float.valueOf(t1.getItem_id()), Float.valueOf(t2.getItem_id()));
            }
        });

        String temp_item_id = mData.get(0).getItem_id();
        String temp_to_user_id = mData.get(0).getTo_user_id();

        ClassChatList temp_class = new ClassChatList();

        for(int i=0; i<mData.size(); i++){
            if(temp_item_id.equals(mData.get(i).getItem_id())){
                if(temp_to_user_id.equals(mData.get(i).getTo_user_id())){ // && Common.userProfile.getId().equals(mData.get(i).getFrom_user_id()))
//                    || (temp_to_user_id.equals(mData.get(i).getFrom_user_id()) && Common.userProfile.getId().equals(mData.get(i).getTo_user_id()))){
                    temp_class.my_user_id = Common.userProfile.getId();
                    temp_class.item_id = mData.get(i).getItem_id();
                    temp_class.other_user_id = mData.get(i).getTo_user_id();
                    temp_class.chatList.add(mData.get(i));
                }else{
//                    temp_class.item_id = mData.get(i).getItem_id();
                    if(temp_class.getChatList().size()!=0) {
                        chatListSort.add(temp_class);
                    }
                    temp_class = new ClassChatList();
                    temp_to_user_id = mData.get(i).getTo_user_id();
                    i--;
                }
            }else{
                if(temp_class.getChatList().size()!=0) {
                    chatListSort.add(temp_class);
                }
                temp_class = new ClassChatList();
                temp_item_id = mData.get(i).getItem_id();
                temp_to_user_id = mData.get(i).getTo_user_id();
                i--;
            }
        }
        if(temp_class.getChatList().size()!=0) {
            chatListSort.add(temp_class);
        }
    }

    private void setChatListTo(List<InfoChat> mData){
        if(mData==null || mData.size()==0){
            return;
        }
        Collections.sort(mData, new Comparator<InfoChat>() {
            @Override
            public int compare(InfoChat t1, InfoChat t2) {
                return Float.compare(Float.valueOf(t1.getItem_id()), Float.valueOf(t2.getItem_id()));
            }
        });

        String temp_item_id = mData.get(0).getItem_id();
        String temp_from_user_id = mData.get(0).getFrom_user_id();

        ClassChatList temp_class = new ClassChatList();

        boolean isEquals = false;
        boolean isFirst = true;

        for(int i=0; i<mData.size(); i++){
                for(int k=0; k<chatListSort.size(); k++){
                    ClassChatList item = chatListSort.get(k);
                    if(mData.get(i).getItem_id().equals(item.item_id)
                            && mData.get(i).getFrom_user_id().equals(item.other_user_id)
                            && mData.get(i).getTo_user_id().equals(Common.userProfile.getId()) ){

                        chatListSort.get(k).chatList.add(mData.get(i));
                        isEquals = true;
                    }
                }

                if(!isEquals){
                    if(isFirst){
                        temp_item_id = mData.get(i).getItem_id();
                        temp_from_user_id = mData.get(i).getFrom_user_id();
                    }
                    if(temp_item_id.equals(mData.get(i).getItem_id())){
                        if(temp_from_user_id.equals(mData.get(i).getFrom_user_id())){
                            temp_class.my_user_id = Common.userProfile.getId();
                            temp_class.item_id = mData.get(i).getItem_id();
                            temp_class.other_user_id = mData.get(i).getFrom_user_id();
                            temp_class.chatList.add(mData.get(i));
                        }else{
                            if(!temp_class.getItem_id().isEmpty()) {
                                chatListSort.add(temp_class);
                                temp_class = new ClassChatList();
                                temp_from_user_id = mData.get(i).getTo_user_id();
                                i--;
                            }
                        }
                    }else{
                        if(!temp_class.getItem_id().isEmpty()) {
                            chatListSort.add(temp_class);
                            temp_class = new ClassChatList();
                            temp_item_id = mData.get(i).getItem_id();
                            i--;
                        }
                    }
                    isFirst = false;
                }
                isEquals = false;
        }
        if(!isFirst && !temp_class.getItem_id().isEmpty()) chatListSort.add(temp_class);

        getItemInfo_SortById();

    }

    private void getItemInfo_SortById(){
        for(int k=0; k<chatListSort.size(); k++) {
            final int kk =k;
//            getItemInfo(kk);
            getItemInfo_00(kk);
            Collections.sort(chatListSort.get(k).chatList, new Comparator<InfoChat>() {
                @Override
                public int compare(InfoChat t1, InfoChat t2) {
                    return Float.compare(Float.valueOf(t1.getId()), Float.valueOf(t2.getId()));
//                    return Long.compare(Long.valueOf(t1.getTimestamps()), Long.valueOf(t2.getTimestamps()));
                }
            });
        }
    }

    private void applyUnreadNumberToListView(){
        int itotal_unread = 0;
        for(int k=0;k<chatListSort.size();k++){
            int iUnreadK = getUnreadNumber(k);
            chatListSort.get(k).setUnread_number(iUnreadK);
            itotal_unread = itotal_unread + iUnreadK;
        }
        if(itotal_unread!=0) {
//            Common.listTitleTextArrowView.setVisibility(View.VISIBLE);
            Common.listTitleTextArrowView.setText(String.valueOf(itotal_unread));
        }else{
            Common.listTitleTextArrowView.setVisibility(View.INVISIBLE);
        }
        Log.d("Conversation:", "get unread number success");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setListView();
            }
        }, 100);
    }

    private int getUnreadNumber(int index){
        if(Common.dataChatListValuesList.size()==0){
            refreshDataChatListValuesList();
        }

        int iUnread = 0;
        ClassChatList classChatList = chatListSort.get(index);

        boolean isStartCount = false;
        for(DataChatListValues dataChatListValue : Common.dataChatListValuesList) {
//            isStartCount = false;
            if (dataChatListValue.item_id.equals(classChatList.getItem_id())
                    && dataChatListValue.my_user_id.equals(classChatList.getMy_user_id())
                    && dataChatListValue.other_user_id.equals(classChatList.getTo_user_id())) {
                for (int i = 0; i < classChatList.getChatList().size(); i++) {
                    if(isStartCount){
                        if(classChatList.getChatList().get(i).getFrom_user_id().equals(dataChatListValue.other_user_id)){
                            iUnread++;
                        }
                    }
                    if(dataChatListValue.last_chatting_number.equals(classChatList.getChatList().get(i).getId())){
                        isStartCount = true;
                    }
                }
                return iUnread;
            }
        }

        return iUnread;
    }


    private void setDataChatListValuesList(){
//        refreshDataChatListValuesList();
        MyDatabaseHelper mDatabaseHelper = new MyDatabaseHelper(this);
        mDatabaseHelper.setValues();
    }

    private void refreshDataChatListValuesList(){
        Common.dataChatListValuesList.clear();
        for(int k=0; k<chatListSort.size(); k++){
            ClassChatList chattemp = chatListSort.get(k);
            String getid = "0";
            if(chattemp.getChatList().size()>0) {
                getid = chattemp.getChatList().get(chattemp.getChatList().size() - 1).getId();
            }else {

            }
            DataChatListValues datatemp = new DataChatListValues(chattemp.getItem_id(), chattemp.getMy_user_id()
                    , chattemp.getTo_user_id(), getid);
            Common.dataChatListValuesList.add(datatemp);

        }
    }

    private void getDataChatListValuesList(){
        MyDatabaseHelper mDatabaseHelper = new MyDatabaseHelper(this);
        mDatabaseHelper.getValues();
    }

    private void getItemInfo_00(int listIndex){
        if (chatListSort.size() > listIndex) {
            String list_item_id = chatListSort.get(listIndex).getItem_id();
            if(Common.totalItems!=null && !Common.totalItems.isEmpty()) {
                for (InfoItem item : Common.totalItems) {
                    if (item.getId().equals(list_item_id)) {
                        chatListSort.get(listIndex).setItem(item);
                    }
                }
            }
            if(Common.userItems!=null && !Common.userItems.isEmpty()) {
                for (InfoItem item : Common.userItems){
                    if (item.getId().equals(list_item_id)) {
                        chatListSort.get(listIndex).setItem(item);
                    }
                }
            }
        }

    }


    private void getItemInfo(int listIndex){
        final int index = listIndex;
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("id", chatListSort.get(listIndex).getItem_id());
        ApiClient.getApiClient().get_item_info(params, new Callback<ResultGetItemInfo>(){
            @Override
            public void success(ResultGetItemInfo resultGetItems, Response response) {
                if(resultGetItems.getStat().equals(Common.success)) {
                    if(resultGetItems.getUserData().getId()!=null
                            && resultGetItems.getUserData().getId().equals(chatListSort.get(index).getItem_id())) {
                        if (chatListSort.size() > index) {
                            chatListSort.get(index).setItem(resultGetItems.getUserData());
                        }
                    }else{
//                        chatListSort.remove(index);
                        Toast.makeText(getApplicationContext(), String.valueOf(index + 1) + " - " + getResources().getString(R.string.the_production_already_is_deleted), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.get_item_info_failed)  , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageBack:
                finish();
                break;
        }
    }
}
