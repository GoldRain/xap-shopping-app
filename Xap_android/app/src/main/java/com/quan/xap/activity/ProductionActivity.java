package com.quan.xap.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.quan.xap.R;
import com.quan.xap.activity.chat.ChatActivity;
import com.quan.xap.activity.post.ItemActivity;
import com.quan.xap.adapters.SlidingImageAdapter;
import com.quan.xap.api.ApiClient;
import com.quan.xap.indicator.CirclePageIndicator;
import com.quan.xap.models.InfoItem;
import com.quan.xap.models.InfoUserProfile;
import com.quan.xap.models.ResultGetFavsOfItem;
import com.quan.xap.models.ResultGetItemInfo;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ProductionActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    TextView txtLogOut;
    boolean isFavorite = false;
    ImageView imgFavorite;
    ImageView imgShare;



    TextView txtChat;

    TextView txtProductionPrice;
    TextView txtProductionTitle;
    TextView txtProductionDate;
    TextView txtSeenNumber;
    TextView txtFavoriteNumber;
    TextView txtProductionDetail;
    TextView txtUserName;
    TextView txtAddress;
    CircleImageView imgProfilePhoto;

    TextView txtShowReserve;
    ImageView imgShowReserve;
    RelativeLayout rl_ShowReserve;
    TextView txtSetReserve;
    TextView txtSetSold;

    ImageView imgAcceptableTrades;
    ImageView imgFirmPrice;
    ImageView imgShippingAvailable;

    private String item_ID = "0";
    private InfoItem item_info;

    private boolean isMyItems = false;
    private boolean isUnReserved = false;

    Bundle bundle;
    SupportMapFragment supportMapFragment;
    ImageView imgBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production);
        if(new SessionManager(getApplicationContext()).getLock()) {
            finish();
        }
        item_ID = getIntent().getExtras().getString(Common.ITEM_ID);
        if(Common.userItems!=null) {
            for (InfoItem item : Common.userItems) {
                if (item.getId().equals(item_ID)) {
                    isMyItems = true;
                }
            }
        }
        bundle = savedInstanceState;
        initialUI();
        getItemInfo();
        imgBack = (ImageView)findViewById(R.id.img_back);
        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentMap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isTimerCanceled = false;



    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(Update);
        isTimerCanceled = true;
        if(swipeTimer!=null) {
            swipeTimer.cancel();
            swipeTimer = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(Update);
        isTimerCanceled = true;
        if(swipeTimer!=null) {
            swipeTimer.cancel();
            swipeTimer = null;
        }
    }

    private void setSeenItem(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("item_id", item_ID);
        ApiClient.getApiClient().set_seen_item(params, new Callback<ResultGetItemInfo>(){
            @Override
            public void success(ResultGetItemInfo resultGetItems, Response response) {
                if(resultGetItems.getStat().equals(Common.success)){

                    getFavsOfItem();

                }else{
                    Toast.makeText(getApplicationContext(), resultGetItems.getMessage()  , Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getFavsOfItem(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("item_id", item_ID);
        ApiClient.getApiClient().get_favs_of_item(params, new Callback<ResultGetFavsOfItem>(){
            @Override
            public void success(ResultGetFavsOfItem resultGetItems, Response response) {
                if(resultGetItems.getStat().equals(Common.success)) {
                    txtFavoriteNumber = (TextView) findViewById(R.id.txtFavoriteNumber);
                    if (txtFavoriteNumber != null) {
                        txtFavoriteNumber.setText(String.valueOf(resultGetItems.getData().size()));
                    }

                }else{
                    Toast.makeText(getApplicationContext(), resultGetItems.getMessage(), Toast.LENGTH_SHORT).show();
                }
                //initMap(bundle);
            }
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getItemInfo(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", item_ID);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.show();

        ApiClient.getApiClient().get_item_info(params, new Callback<ResultGetItemInfo>(){
            @Override
            public void success(ResultGetItemInfo resultGetItems, Response response) {
                progressDialog.dismiss();
                if(resultGetItems.getStat().equals(Common.success)) {
                    item_info = resultGetItems.getUserData();

                    initImageSlider();
                    updateItemInfo();

                    if (item_info.getReserved().equals("0")) {
                        isUnReserved = false;
                    } else {
                        isUnReserved = true;
                    }
                    if (isMyItems) {
                        showReserve(item_info.getReserved().equals("0"));
                        showSold(item_info.getSold().equals("0"));
                    }
                }else{
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.get_item_info_failed), Toast.LENGTH_LONG).show();
                }

                supportMapFragment.getMapAsync(ProductionActivity.this);
                setSeenItem();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initialUI() {
        isTimerCanceled = false;

        txtChat = (TextView) findViewById(R.id.txtChat);
        txtChat.setOnClickListener(this);

        txtLogOut = (TextView) findViewById(R.id.txtLogOut);
        findViewById(R.id.txtLogOut).setOnClickListener(this);
        txtLogOut.setVisibility(View.GONE);

        findViewById(R.id.imageBack).setOnClickListener(this);
        findViewById(R.id.imgLogOut).setOnClickListener(this);
        findViewById(R.id.imgShare).setOnClickListener(this);
        imgShare = (ImageView)findViewById(R.id.imgShare);

        findViewById(R.id.imgFacebook).setOnClickListener(this);
        findViewById(R.id.imgWhatsapp).setOnClickListener(this);
        findViewById(R.id.imgEmail).setOnClickListener(this);
        findViewById(R.id.imgTwitter).setOnClickListener(this);

        imgFavorite = (ImageView) findViewById(R.id.imgFavorite);
        findViewById(R.id.imgFavorite).setOnClickListener(this);

        txtProductionPrice = (TextView) findViewById(R.id.txtProductionPrice);
        txtSeenNumber = (TextView) findViewById(R.id.txtSeenNumber);
        txtFavoriteNumber = (TextView) findViewById(R.id.txtFavoriteNumber);
        txtProductionDetail = (TextView) findViewById(R.id.txtProductionDetail);
        txtProductionTitle = (TextView) findViewById(R.id.txtProductionTitle);
        txtProductionDate = (TextView) findViewById(R.id.txtProductionDate);
        txtUserName = (TextView) findViewById(R.id.txtUserName);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        imgProfilePhoto = (CircleImageView) findViewById(R.id.imgProfilePhoto);

        imgAcceptableTrades = (ImageView) findViewById(R.id.imgAcceptableTrades);
        imgFirmPrice = (ImageView) findViewById(R.id.imgFirmPrice);
        imgShippingAvailable = (ImageView) findViewById(R.id.imgShippingAvailable);

        txtShowReserve = (TextView) findViewById(R.id.txtShowReserve);
        imgShowReserve = (ImageView) findViewById(R.id.imgShowReserve);
        rl_ShowReserve = (RelativeLayout) findViewById(R.id.rl_ShowReserve);
        txtSetReserve = (TextView) findViewById(R.id.txtSetReserve);
        txtSetSold = (TextView) findViewById(R.id.txtSetSold);
        findViewById(R.id.txtSetReserve).setOnClickListener(this);
        findViewById(R.id.txtSetSold).setOnClickListener(this);

        setIsMyItems();
    }


    private void setIsMyItems(){
        if(isMyItems){

            rl_ShowReserve.setVisibility(View.VISIBLE);
            txtShowReserve.setVisibility(View.VISIBLE);
            txtSetReserve.setVisibility(View.VISIBLE);
            txtSetSold.setVisibility(View.VISIBLE);

            txtLogOut.setText(getResources().getString(R.string.delete));
            txtChat.setVisibility(View.GONE);
            imgFavorite.setVisibility(View.GONE);

            imgShare.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_white_36dp));

        }else{
            rl_ShowReserve.setVisibility(View.GONE);
            txtShowReserve.setVisibility(View.GONE);
            txtSetReserve.setVisibility(View.GONE);
            txtSetSold.setVisibility(View.GONE);
            txtLogOut.setText(getResources().getString(R.string.report_this_listing));
            txtChat.setVisibility(View.VISIBLE);
            imgFavorite.setVisibility(View.VISIBLE);
        }
    }


    private void setReserve(){
        Map<String, Object> params = new HashMap<String, Object>();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.show();
        params.put("item_id", item_ID);
        if(isUnReserved) {
            params.put("is_reserved", "0");
        }else{
            params.put("is_reserved", "1");
        }
        ApiClient.getApiClient().reserve_item(params, new Callback<ResultGetItemInfo>(){
            @Override
            public void success(ResultGetItemInfo resultGetItems, Response response) {
                if(resultGetItems.getStat().equals(Common.success)){
                    showReserve(isUnReserved);
                }else {
                    Toast.makeText(getApplicationContext(), resultGetItems.getMessage()  , Toast.LENGTH_LONG).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_LONG).show();

            }
        });
    }

    private void showReserve(boolean isUnReserved){
        if(isMyItems) {
            if (isUnReserved) {
                txtSetReserve.setBackground(getResources().getDrawable(R.drawable.button_sold_conner));
                txtSetReserve.setTextColor(getResources().getColor(R.color.white));
                txtSetReserve.setText(getResources().getString(R.string.reserve));
                txtShowReserve.setText(getResources().getString(R.string.unreserve)+"d");
                txtShowReserve.setVisibility(View.GONE);
                rl_ShowReserve.setVisibility(View.GONE);
            } else {
                txtSetReserve.setBackground(getResources().getDrawable(R.drawable.button_unreserve_conner));
                txtSetReserve.setTextColor(getResources().getColor(R.color.facebook_button));
                txtSetReserve.setText(getResources().getString(R.string.unreserve));
                txtShowReserve.setText(getResources().getString(R.string.reserve)+"d");
                txtShowReserve.setVisibility(View.VISIBLE);
                txtShowReserve.setTextColor(getResources().getColor(R.color.facebook_button));
                rl_ShowReserve.setVisibility(View.VISIBLE);
            }
//            txtShowReserve.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_reserve_mark),null,null,null);
            imgShowReserve.setImageDrawable(getResources().getDrawable(R.drawable.ic_reserve_mark));
        }
    }

    private void setSold(final boolean isUnSold){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("item_id", item_ID);

        ApiClient.getApiClient().sell_item(params, new Callback<ResultGetItemInfo>(){
            @Override
            public void success(ResultGetItemInfo resultGetItems, Response response) {
                if(resultGetItems.getStat().equals(Common.success)) {
                    showSold(isUnSold);
                }else{
                    Toast.makeText(getApplicationContext(), resultGetItems.getMessage()  , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_LONG).show();
            }
        });
    }

    private void deleteItem(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("item_id", item_ID);
        ApiClient.getApiClient().delete_item(params, new Callback<ResultGetItemInfo>(){
            @Override
            public void success(ResultGetItemInfo resultGetItems, Response response) {
                if(resultGetItems.getStat().equals(Common.success)) {
                    if(Common.userItems!=null) Common.userItems.clear();
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.delete_item_success) + "\r\n", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.delete_item_failed)  , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showSold(boolean isUnSold){
        if(isMyItems) {
            if (isUnSold) {
                txtSetSold.setBackground(getResources().getDrawable(R.drawable.button_sold_conner));
                txtSetSold.setTextColor(getResources().getColor(R.color.white));
            } else {
                txtSetSold.setBackground(getResources().getDrawable(R.drawable.button_unreserve_conner));
                txtSetSold.setTextColor(getResources().getColor(R.color.facebook_button));
                txtSetSold.setEnabled(false);
                txtSetReserve.setEnabled(false);
                txtSetReserve.setBackground(getResources().getDrawable(R.drawable.button_unreserve_conner));
                txtSetReserve.setTextColor(getResources().getColor(R.color.facebook_button));
                txtShowReserve.setText("Sold");
                txtShowReserve.setTextColor(getResources().getColor(R.color.red));
//                txtShowReserve.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_sold_mark),null,null,null);
                imgShowReserve.setImageDrawable(getResources().getDrawable(R.drawable.ic_sold_mark));
                txtShowReserve.setVisibility(View.VISIBLE);
                rl_ShowReserve.setVisibility(View.VISIBLE);
            }
        }
    }

    private void updateItemInfo(){
        if(item_info!=null && !item_info.getTitle().isEmpty() && !item_info.getTitle().equals("")){
            txtProductionPrice.setText(Common.Currency[Integer.valueOf(item_info.getCurrency())] + " " + item_info.getPrice());
            txtSeenNumber.setText(item_info.getSeens());

            txtProductionTitle.setText(item_info.getTitle());
            txtProductionDetail.setText(item_info.getDescription());
            txtProductionDate.setText(item_info.getUpdate_time());

            if(item_info.getUser() != null){
                txtUserName.setText(item_info.getUser().getUsername());
                txtAddress.setText(item_info.getUser().getAddress());
                if(!item_info.getUser().getProfile_image().isEmpty()) {
                    Picasso.with(this).load(Common.common_url + item_info.getUser().getProfile_image()).into(imgProfilePhoto);
                }
            }else{
                Toast.makeText(ProductionActivity.this, "There is no user that post this item",Toast.LENGTH_LONG).show();
            }


            if(item_info.getAcceptable_trades().equals("0")){
                imgAcceptableTrades.setAlpha(0.3f);
            }else{
                imgAcceptableTrades.setAlpha(1.0f);
            }
            if(item_info.getFirm_price().equals("0")){
                imgFirmPrice.setAlpha(0.3f);
            }else{
                imgFirmPrice.setAlpha(1.0f);
            }
            if(item_info.getShipping_available().equals("0")){
                imgShippingAvailable.setAlpha(0.3f);
            }else{
                imgShippingAvailable.setAlpha(1.0f);
            }

            if(item_info.getFavs()!=null) {
                for (int i = 0; i < item_info.getFavs().size(); i++) {
                    if (Common.userProfile!=null && item_info.getFavs().get(i).getUser_id().equals(Common.userProfile.getId())) {
                        isFavorite = false;
                        imgFavorite.performClick();
                    }
                }
            }
        }
    }

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
//    private static final Integer[] IMAGES= {R.drawable.computer_image,R.drawable.computer_image,R.drawable.computer_image};
    private ArrayList<String> ImagesArray = new ArrayList<String>();
    Handler handler = new Handler();
    Runnable Update= new Runnable() {
        public void run() {
            currentPage++;
            if (currentPage == NUM_PAGES) {
                currentPage = 0;
            }
            mPager.setCurrentItem(currentPage, true);
        }
    };

    Timer swipeTimer = new Timer();
    boolean isTimerCanceled = false;

    private void initImageSlider() {
//        for(int i=0;i<IMAGES.length;i++)
//            ImagesArray.add(IMAGES[i]);

        if(item_info!=null) {
            if(!item_info.getPic1().isEmpty())
                ImagesArray.add(Common.common_url + item_info.getPic1());
            if(!item_info.getPic2().isEmpty())
                ImagesArray.add(Common.common_url + item_info.getPic2());
            if(!item_info.getPic3().isEmpty())
                ImagesArray.add(Common.common_url + item_info.getPic3());
            if(!item_info.getPic4().isEmpty())
                ImagesArray.add(Common.common_url + item_info.getPic4());
        }

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SlidingImageAdapter(this, ImagesArray));
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;
/////////////////Set circle indicator radius
        indicator.setRadius(3 * density);

//        indicator.setFillColor();

        NUM_PAGES =ImagesArray.size();

/////////////// Auto start of viewpager

        handler.removeCallbacks(Update);



        if(!isTimerCanceled) {
            if(swipeTimer!=null) {
                swipeTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(Update);
                    }
                }, 3000, 3000);
            }
        }


//////////////// Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {
            }
        });
    }



    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.imageBack:
                finish();
                break;
            case R.id.txtLogOut:
                if(isMyItems){
                    //delete
                    deleteItem();
                }else{
                    //report
                    Intent intent = new Intent(ProductionActivity.this, ReasonActivity.class);
                    intent.putExtra(Common.ITEM_ID, item_info.getUser_id());
                    startActivity(intent);
                }
                txtLogOut.setVisibility(View.GONE);
                finish();
                break;
            case R.id.imgLogOut:
                if(txtLogOut.getVisibility()==View.VISIBLE){
                    txtLogOut.setVisibility(View.GONE);
                }else {
                    txtLogOut.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.imgShare:
                if(isMyItems){
                    toItemActivity();
                }else {
                    inviteOthers();
                }
                break;
            case R.id.imgFavorite:
                isFavorite = !isFavorite;
                if(isFavorite){
                    imgFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_border_white_36dp_red));
                }else{
                    imgFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_border_white_36dp));
                }
                setFav(isFavorite);
                break;
            case R.id.txtChat:
                Intent intent = new Intent(ProductionActivity.this, ChatActivity.class);
                if(item_ID!=null && item_info!=null) {
                    intent.putExtra(Common.ITEM_ID, item_ID);
                    intent.putExtra("to_user_id", item_info.getUser_id()); //item_info.getUser().getId());
//                    intent.putExtra("item_info", item_info);
                    startActivity(intent);
                }
                finish();
                break;
            case R.id.imgFacebook:
                inviteFacebook();
                break;
            case R.id.imgWhatsapp:
                inviteWhatsapp();
                break;
            case R.id.imgEmail:
                inviteEmail();
                break;
            case R.id.imgTwitter:
                inviteTwitter();
                break;
            case R.id.txtSetReserve:
                isUnReserved = !isUnReserved;
                setReserve();
                break;
            case R.id.txtSetSold:
                setSold(false); //isUnsold = false
                break;
        }
    }

    private void toItemActivity() {
        if(item_info!=null) {
            Intent intent = new Intent(ProductionActivity.this, ItemActivity.class);
            intent.putExtra(Common.ITEM_ID, item_info);
            startActivity(intent);
        }
    }


    private void setFav(final boolean isFav){
        Map<String, Object> params = new HashMap<String, Object>();
        if(Common.userProfile!=null) {
            params.put("user_id", Common.userProfile.getId());
        }
        params.put("item_id", item_ID);
        if(isFav) {
            params.put("fav", "1");
        }else{
            params.put("fav", "0");
        }
        ApiClient.getApiClient().set_fav(params, new Callback<ResultGetItemInfo>(){
            @Override
            public void success(ResultGetItemInfo resultGetItems, Response response) {
                if(resultGetItems.getStat().equals(Common.success)){
                    if(Common.totalItems!=null){
                        for(int i=0;i<Common.totalItems.size();i++){
                            InfoItem item0 = Common.totalItems.get(i);
                            if(item0.getId().equals(item_ID)){
                                List<InfoUserProfile> favUsers = Common.totalItems.get(i).getFavUsers();
                                boolean isequals = true;
                                    for(InfoUserProfile item1 : favUsers) {
                                        if(item1.getId().equals(Common.userProfile.getId())) {
                                            if(!isFav){
                                                favUsers.remove(item1);
                                            }
                                            isequals = false;
                                        }
                                    }
                                    if(isFav && isequals){
                                        favUsers.add(Common.userProfile);
                                    }
                                Common.totalItems.get(i).setFavUsers(favUsers);
                            }
                        }
                    }
                }else{
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.get_item_info_failed)  , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connect_failed)  , Toast.LENGTH_LONG).show();
            }
        });
    }


    private void inviteFacebook(){
        startActivity(getOpenFacebookIntent(this));
    }

    private static Intent getOpenFacebookIntent(Context context) {
        try {
//            context.getPackageManager().getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
            context.getPackageManager().getPackageInfo("com.facebook.snmdevice", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/254175194653125")); //Trys to make intent with FB's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/snmdevice/feed")); //catches and opens a url to the desired page
        }
    }

    private void inviteTwitter(){
        startActivity(getOpenTwitterIntent(this));
    }

    private static Intent getOpenTwitterIntent(Context context) {
        Intent intent = null;
        try {
            // get the Twitter app if possible
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
//            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=USERID"));
//            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=SNM_GlobalTech"));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } catch (Exception e) {
            // no Twitter app, revert to browser
//            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/USERID_OR_PROFILENAME"));
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/SNM_GlobalTech"));
        }
        return intent;
    }

    private void inviteEmail(){
        sendEmail();
    }

    private void sendEmail(){
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
//        emailIntent.setType("text/plain");
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"email@example.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "subject here");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "body text");
//        File root = Environment.getExternalStorageDirectory();
//        String pathToMyAttachedFile = "temp/attachement.xml";
//        File file = new File(root, pathToMyAttachedFile);
//        if (!file.exists() || !file.canRead()) {
//            return;
//        }
//        Uri uri = Uri.fromFile(file);
//        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(emailIntent, "Email with..."));
    }

    private void inviteWhatsapp(){
        PackageManager pm = getPackageManager();
        try {

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = "YOUR TEXT HERE";

            PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.whatsapp");
            waIntent.setPackage(info.packageName);

            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(getApplicationContext(), "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
        }

    }

    private void inviteOthers(){

        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            String strShareMessage = "\nLet me recommend you this application\n\n";
            strShareMessage = strShareMessage + "https://play.google.com/store/apps/details?id="
                    + getPackageName();
            Uri screenshotUri = Uri.parse("android.resource://packagename/drawable/image_name");
            i.setType("image/png");
            i.putExtra(Intent.EXTRA_STREAM, screenshotUri);
            i.putExtra(Intent.EXTRA_TEXT, strShareMessage);
            startActivity(Intent.createChooser(i, "Share via"));
        } catch(Exception e) {
            //e.toString();
        }

    }


    //MapView mapView;
    GoogleMap map;
    Marker productMarker;
    LatLng productLocation;


    private void setUpMap() {
        //map.setInfoWindowAdapter(new CustomInfoWindowAdapter(1));
        productLocation = new LatLng(Common.latitude, Common.longitude);
        if(item_info!=null)
        {
            productLocation = new LatLng(Double.valueOf(item_info.getLatitude()), Double.valueOf(item_info.getLongitude()));
        }
        productMarker = map.addMarker(new MarkerOptions().position(productLocation));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(productLocation, 14));
        MapsInitializer.initialize(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map  = googleMap;
        setUpMap();
    }
}
