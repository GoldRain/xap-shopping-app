package com.quan.xap.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by jon on 11/4/2016.
 */

public class MyDatabaseHelper extends SQLiteOpenHelper {

    public static final String DBNAME= "mydatabase.db";
    public static final String DBLOCATION= Environment.getDataDirectory()+"/data/com.quan.mywallapop/databases/";
    public Context mContext;
    public SQLiteDatabase mDatabase;

    private static final String TABLE_NAME="chatlist";

    public MyDatabaseHelper(Context context){
        super(context, DBNAME, null, 1);
        this.mContext=context;
        //////////database copy
        File database=context.getDatabasePath(DBNAME);

        if(database.exists()==false){
            getReadableDatabase();
            if(copyDatabase(context)){
//                Toast.makeText(context, "Copy database success", Toast.LENGTH_SHORT).show();
            }else{
//                Toast.makeText(context, "Copy data error", Toast.LENGTH_SHORT).show();
                return;
            }
        }else{
//            context.deleteDatabase(DBNAME);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void openDatabase(){
        String dbPath=mContext.getDatabasePath(DBNAME).getPath();
        if(mDatabase!=null && mDatabase.isOpen()){
            return;
        }
        mDatabase= SQLiteDatabase.openOrCreateDatabase(dbPath,null,null);
    }

    public void closeDatabase(){
        if(mDatabase!=null) {
            mDatabase.close();
        }
    }

    public void getValues(){
//        index = Common.countryIndex;
        Common.dataChatListValuesList.clear();
        openDatabase();
        try{

                Cursor cursor = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null);
                int i = 0;
                cursor.moveToFirst();
                while(!cursor.isAfterLast()){
                    DataChatListValues td = new DataChatListValues(cursor.getString((cursor.getColumnIndex("item_id")))
                            , cursor.getString((cursor.getColumnIndex("my_user_id")))
                            , cursor.getString((cursor.getColumnIndex("other_user_id")))
                            , cursor.getString((cursor.getColumnIndex("last_chatting_number")))
                    );

                    Common.dataChatListValuesList.add(td);

                    i = i + 1;
                    cursor.moveToNext();
                }
                cursor.close();
            closeDatabase();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void setValues(){
//        Common.countryIndex = index;
        openDatabase();
///////////////// upgrade

            // Drop older table if existed
            mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            String SQL_CREATE_ENTRIES =
                    "CREATE TABLE `" + TABLE_NAME + "` (\n" +
                            "\t`ID`\tINTEGER,\n" +
                            "\t`item_id`\tTEXT,\n" +
                            "\t`my_user_id`\tTEXT,\n" +
                            "\t`other_user_id`\tTEXT,\n" +
                            "\t`last_chatting_number`\tTEXT,\n" +
                            "\tPRIMARY KEY(`ID`)\n" +
                            ");";
            mDatabase.execSQL(SQL_CREATE_ENTRIES);
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            for (int i = 0; i < Common.dataChatListValuesList.size(); i++) {
                values.clear();
                values.put("ID", i);
                values.put("item_id", Common.dataChatListValuesList.get(i).item_id);
                values.put("my_user_id", Common.dataChatListValuesList.get(i).my_user_id);
                values.put("other_user_id", Common.dataChatListValuesList.get(i).other_user_id);
                values.put("last_chatting_number", Common.dataChatListValuesList.get(i).last_chatting_number);
                db.insert(TABLE_NAME, null, values);
            }

//////////////close db
        closeDatabase();
    }

    public boolean copyDatabase(Context context){
        try{
            InputStream inputStream = context.getAssets().open("db/" + MyDatabaseHelper.DBNAME);
            String outFileName = MyDatabaseHelper.DBLOCATION + MyDatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buff= new byte[1024];
            int length=0;
            while ((length=inputStream.read(buff))>0){
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            Log.v("MainActivity", "DB copied");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

}
