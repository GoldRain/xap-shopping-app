package com.quan.xap.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoquan on 4/3/2017.
 */

public class InfoItem implements Serializable{
    private static String ITEM = "item";

    @SerializedName("id")
    private String id = "";
    @SerializedName("user_id")
    private String user_id = "";
    @SerializedName("title")
    private String title = "";
    @SerializedName("category")
    private String category = "";
    @SerializedName("pic1")
    private String pic1 = "";
    @SerializedName("pic2")
    private String pic2 = "";
    @SerializedName("pic3")
    private String pic3 = "";
    @SerializedName("pic4")
    private String pic4 = "";
    @SerializedName("description")
    private String description = "";
    @SerializedName("price")
    private String price = "";
    @SerializedName("currency")
    private String currency = "";
    @SerializedName("terms")
    private String terms = "";
    @SerializedName("shipping_available")
    private String shipping_available = "";
    @SerializedName("firm_price")
    private String firm_price = "";
    @SerializedName("acceptable_trades")
    private String acceptable_trades = "";
    @SerializedName("seens")
    private String seens = "";
    @SerializedName("update_time")
    private String update_time = "";
    @SerializedName("sold")
    private String sold = "";
    @SerializedName("longitude")
    private String longitude = "0";
    @SerializedName("latitude")
    private String latitude = "0";
    @SerializedName("reserved")
    private String reserved = "";
    @SerializedName("distance")
    private String distance = "";
    @SerializedName("favUsers")
    private List<InfoUserProfile> favUsers = new ArrayList<>();
    @SerializedName("fav")
    private List<InfoFaves> favs = new ArrayList<>();
    @SerializedName("user")
    private InfoUserProfile user = new InfoUserProfile();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPic1() {
        return pic1;
    }

    public void setPic1(String pic1) {
        this.pic1 = pic1;
    }

    public String getPic2() {
        return pic2;
    }

    public void setPic2(String pic2) {
        this.pic2 = pic2;
    }

    public String getPic3() {
        return pic3;
    }

    public void setPic3(String pic3) {
        this.pic3 = pic3;
    }

    public String getPic4() {
        return pic4;
    }

    public void setPic4(String pic4) {
        this.pic4 = pic4;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getShipping_available() {
        return shipping_available;
    }

    public void setShipping_available(String shipping_available) {
        this.shipping_available = shipping_available;
    }

    public String getFirm_price() {
        return firm_price;
    }

    public void setFirm_price(String firm_price) {
        this.firm_price = firm_price;
    }

    public String getAcceptable_trades() {
        return acceptable_trades;
    }

    public void setAcceptable_trades(String acceptable_trades) {
        this.acceptable_trades = acceptable_trades;
    }

    public String getSeens() {
        return seens;
    }

    public void setSeens(String seens) {
        this.seens = seens;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getSold() {
        return sold;
    }

    public void setSold(String sold) {
        this.sold = sold;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getReserved() {
        return reserved;
    }

    public void setReserved(String reserved) {
        this.reserved = reserved;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public List<InfoUserProfile> getFavUsers() {
        return favUsers;
    }

    public void setFavUsers(List<InfoUserProfile> favUsers) {
        this.favUsers = favUsers;
    }

    public InfoUserProfile getUser() {
        return user;
    }

    public void setUser(InfoUserProfile user) {
        this.user = user;
    }

    public List<InfoFaves> getFavs() {
        return favs;
    }

    public void setFavs(List<InfoFaves> favs) {
        this.favs = favs;
    }
}
