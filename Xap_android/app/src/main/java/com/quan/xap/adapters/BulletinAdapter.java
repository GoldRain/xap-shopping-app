package com.quan.xap.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.quan.xap.R;
import com.quan.xap.models.InfoBulletin;
import com.quan.xap.utility.Common;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class BulletinAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private Activity getActivity;
    private List<InfoBulletin> categories = new ArrayList<>();

    public BulletinAdapter(Activity activity, List<InfoBulletin> data) {
        getActivity = activity;
        layoutInflater = activity.getLayoutInflater();
        categories = data;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        ViewHolder holder = null;
        if (convertView == null) {
            int res = R.layout.adapter_bulletin;
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(res, viewGroup, false);
            holder.imgPhoto = (ImageView) convertView.findViewById(R.id.imgPhoto);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
            holder.txtContent = (TextView) convertView.findViewById(R.id.txtContent);
            holder.txtDate = (TextView) convertView.findViewById(R.id.txtDate);

            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }

        final InfoBulletin item = categories.get(i);

        if(item!=null) {
            if (item.getImage() != null) {
                if (!item.getImage().isEmpty()) {
//                ImageLoader imgLoader = new ImageLoader(getActivity);
//                int loader = R.drawable.loader;
//                imgLoader.DisplayImage(Common.common_url + item.getImage(), loader, holder.imgPhoto);
                    Picasso.with(getActivity).load(Common.common_url + item.getImage()).into(holder.imgPhoto);
                }
            }

            if(item.getTitle()!=null) holder.txtTitle.setText(item.getTitle());
            if(item.getContent()!=null) holder.txtContent.setText(item.getContent());
            if(item.getTimestamps()!=null) holder.txtDate.setText(item.getTimestamps());
        }

        return convertView;
    }





    public static class ViewHolder {
        public ImageView imgPhoto;
        public TextView txtTitle;
        public TextView txtContent;
        public TextView txtDate;
    }
}

