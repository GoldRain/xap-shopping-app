package com.quan.xap.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by guoquan on 4/12/2017.
 */

public class ResultEmailConform implements Serializable{
    @SerializedName("result")
    private String result;
    @SerializedName("error")
    private String error;
    @SerializedName("detail")
    private String detail;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
