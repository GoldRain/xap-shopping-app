package com.quan.xap;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.quan.xap.activity.LogoActivity;
import com.quan.xap.activity.filter.FilterActivity;
import com.quan.xap.fragments.StaggeredGridFragment;
import com.quan.xap.interfaces.OpenDrawerEventListener;
import com.quan.xap.models.InfoFaves;
import com.quan.xap.models.InfoUserProfile;
import com.quan.xap.navigation.NavigationDrawerFragment;
import com.quan.xap.utility.Common;
import com.quan.xap.utility.GPSTracker;
import com.quan.xap.utility.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemClickListener
         {

    static int PERMISSIONS_REQUEST_PERMISSION = 1001;
    static int PERM_REQUEST_CODE_DRAW_OVERLAYS = 1003;

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private DrawerLayout mDrawerLayout;

    private SessionManager session;
    private boolean isDrawOverlay = true;
    ActionBar ab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_other);
        // ---------------------------------------------- new part ---------------------------------------------
        //getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView toolbarTitle = (TextView)toolbar.findViewById(R.id.toolbar_title);
        ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu_white_36dp);
        ab.setDisplayHomeAsUpEnabled(true);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        // ----------------------------------------------  end    ----------------------------------------------


        isDrawOverlay = permissionToDrawOverlays();
        if(isDrawOverlay) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (
                        ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED
                        ) {
                    // Should we show an explanation?
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION
//                                , Manifest.permission.ACCESS_COARSE_LOCATION
                            },
                            PERMISSIONS_REQUEST_PERMISSION);
                } else {

                }
            }

            // Set up the drawer.

            mNavigationDrawerFragment = (NavigationDrawerFragment)
                    getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
            mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

            session = new SessionManager(this);

            Common.isLoadedItemData = false;
            //initialUI();
        }
    }


    public boolean permissionToDrawOverlays() {
//        if (android.os.Build.VERSION.SDK_INT >= 23) {   //Android M Or Over
//            if (!Settings.canDrawOverlays(this)) {
//                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
//                startActivityForResult(intent, PERM_REQUEST_CODE_DRAW_OVERLAYS);
//                return false;
//            }
//        }
        return true;
    }

    private void initialUI(){

        getLong_Lat();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startFragment();
            }
        }, 100);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Common.isLoadedItemData = false;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(isDrawOverlay) {
            if (session.isUpdate() || !Common.isLoadedItemData) {
                session.setUpdate(false);
                initialUI();
            }
        }else{
            initialUI();
        }
    }




////////////////////////////////////////
    private static final String TAG = MainActivity.class.getSimpleName();



    private List<InfoUserProfile> getListInfoUserProfile(JSONArray jsonArray){
        List<InfoUserProfile> infoUserProfiles = new ArrayList<>();

        for(int k=0; k<jsonArray.length();k++){
            try {
                JSONObject jObject = jsonArray.getJSONObject(k);
                InfoUserProfile mData01 = getInfoUserProfile(jObject);
                infoUserProfiles.add(mData01);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return infoUserProfiles;
    }
    private List<InfoFaves> getListInfoFaves(JSONArray jsonArray){
        List<InfoFaves> infoFaves = new ArrayList<>();

        for(int k=0; k<jsonArray.length();k++){
            try {
                JSONObject jObject = jsonArray.getJSONObject(k);
                InfoFaves mData01 = new InfoFaves();
                mData01.setId(jObject.getString("id"));
                mData01.setId(jObject.getString("user_id"));
                mData01.setId(jObject.getString("item_id"));
                mData01.setId(jObject.getString("timestamps"));
                infoFaves.add(mData01);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return infoFaves;
    }
    private InfoUserProfile getInfoUserProfile(JSONObject jsonObject){
        InfoUserProfile infoUserProfile = new InfoUserProfile();
        try {
            infoUserProfile.setId(jsonObject.getString("id"));
            infoUserProfile.setId(jsonObject.getString("user_name"));
            infoUserProfile.setId(jsonObject.getString("first_name"));
            infoUserProfile.setId(jsonObject.getString("last_name"));
            infoUserProfile.setId(jsonObject.getString("address"));
            infoUserProfile.setId(jsonObject.getString("latitude"));
            infoUserProfile.setId(jsonObject.getString("longitude"));
            infoUserProfile.setId(jsonObject.getString("gender"));
            infoUserProfile.setId(jsonObject.getString("birthday"));
            infoUserProfile.setId(jsonObject.getString("email"));
            infoUserProfile.setId(jsonObject.getString("password"));
            infoUserProfile.setId(jsonObject.getString("profile_image"));
            infoUserProfile.setId(jsonObject.getString("phone_number"));
            infoUserProfile.setId(jsonObject.getString("facebook"));
            infoUserProfile.setId(jsonObject.getString("google"));
            infoUserProfile.setId(jsonObject.getString("verif_email"));
            infoUserProfile.setId(jsonObject.getString("verif_phone"));
            infoUserProfile.setId(jsonObject.getString("device_token"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return infoUserProfile;
    }
///////////////////////////////////////////

    private void getLong_Lat(){

        initGps();

        GPSTracker gps = new GPSTracker(this);
        if(gps.canGetLocation()){
//            Common.latitude = (float) gps.getLatitude();
//            Common.longitude = (float) gps.getLongitude();
        }else{       // can't get location
                     // GPS or Network is not enabled
                     // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

     LocationManager locationManager;
     private void initGps() {
         locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
         Criteria criteria = new Criteria();
         criteria.setAccuracy(Criteria.ACCURACY_FINE);
         for (String provider : locationManager.getProviders(criteria, true)) {
             if (provider.contains("gps")) {
                 //locationManager.requestSingleUpdate(provider,this,null);
             }
             return;
         }
         startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
     }


     private void startFragment(){
//        StaggeredGridFragment demoFragment = StaggeredGridFragment.newInstance(this, "mParam2");
         StaggeredGridFragment demoFragment = StaggeredGridFragment.newInstance(this, "mParam2");

        demoFragment.setEventListener(new OpenDrawerEventListener() {
            @Override
            public void openDrawerByMenu() {
                openDrawerByMenu_00();
            }
        });
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, demoFragment);
//        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void openDrawerByMenu_00(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Common.isFilter = true;
            Intent intent = new Intent(this, FilterActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PERM_REQUEST_CODE_DRAW_OVERLAYS) {
            if (android.os.Build.VERSION.SDK_INT >= 23) {   //Android M Or Over
                if (!Settings.canDrawOverlays(this)) {
                    Intent intent = new Intent(MainActivity.this, LogoActivity.class);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    // ADD UI FOR USER TO KNOW THAT UI for SYSTEM_ALERT_WINDOW permission was not granted earlier...
                }
            }
        }
    }

             @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                finish();
            } else {

            }
        }

    }
}
