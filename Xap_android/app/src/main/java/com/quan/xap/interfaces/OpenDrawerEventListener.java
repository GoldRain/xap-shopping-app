package com.quan.xap.interfaces;

/**
 * Created by Quan on 5/25/2017.
 */

public interface OpenDrawerEventListener {
    public void openDrawerByMenu();
}
