package com.quan.xap.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.quan.xap.R;


/**
 * Created by Administrator on 3/23/2017.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener{



    TextView txtTitle;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            case R.id.img_back:
//                super.finish();
//                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setTitle(String title){

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setTitle(title);

        txtTitle = (TextView)findViewById(R.id.txt_title);
        txtTitle.setText(title);
    }

    public void showClose(String title){
        TextView txtClose = (TextView)findViewById(R.id.txt_close);
        txtClose.setVisibility(View.VISIBLE);
        txtClose.setOnClickListener(this);
        txtClose.setText(title);

    }
//
//    SweetAlertDialog sweetAlertDialog;
//    public void showProgressDialog(){
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                sweetAlertDialog = new SweetAlertDialog(BaseActivity.this, SweetAlertDialog.PROGRESS_TYPE);
//                sweetAlertDialog.setTitleText(getResources().getString(R.string.connect_server));
//                sweetAlertDialog.setContentText(getResources().getString(R.string.wait));
//                sweetAlertDialog.show();
//            }
//        });
//    }
//
//    public void hideProgressDialog(){
//        if (null != sweetAlertDialog && sweetAlertDialog.isShowing()) {
//            sweetAlertDialog.dismiss();
//        }
//    }


}
